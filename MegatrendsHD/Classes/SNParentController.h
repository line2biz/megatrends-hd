//
//  SNParentController.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 25.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNParentController : UIViewController

- (void)setTrendsMenuHidden:(BOOL)hidden animated:(BOOL)animated;
- (IBAction)didTapButtonTrends:(id)sender;
- (IBAction)didTapButtonAnalysis:(id)sender;
- (IBAction)didTapButtonZPunkt:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *badgeButton;

@end
