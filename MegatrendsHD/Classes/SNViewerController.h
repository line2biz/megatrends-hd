//
//  SNViewerController.h
//  MegatrendsHD
//
//  Created by ios on 5/14/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMAnalyse;

@interface SNViewerController : UIViewController

@property (strong, nonatomic) DMAnalyse *currentAnalyse;

@end
