//
//  SNNavigationController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 27.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNNavigationController.h"
#import "SNNavigationBar.h"
#import "SNNavigationControllerProtocol.h"

@interface SNNavigationController () <UINavigationControllerDelegate>
{
    __weak IBOutlet UIView *_containerView;
    
    UINavigationController *_navigationController;
}

@end

@implementation SNNavigationController


+ (SNNavigationController *)navigationControllerWithRootController:(UIViewController *)viewController
{
    
    NSBundle *bundle = [[SNLocalizationManager sharedManager] bundle];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:bundle];
    SNNavigationController *navController = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    navController->_navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    navController->_navigationController.navigationBarHidden = YES;
    navController->_navigationController.delegate = navController;
    return navController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //    Show New Controller
    [self addChildViewController:_navigationController];
    _navigationController.view.frame = _containerView.bounds;
    _navigationController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [_containerView addSubview:_navigationController.view];
    [_navigationController didMoveToParentViewController:self];
    [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self configureView];
        
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureView
{
    [self navigationController:_navigationController didShowViewController:[_navigationController.viewControllers lastObject] animated:NO];
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Property Accessor
- (UIViewController *)rootViewController
{
    if ([_navigationController.viewControllers count]) {
        return _navigationController.viewControllers[0];
    }
    return nil;
}

- (NSArray *)viewControllers
{
    return _navigationController.viewControllers;
}

#pragma mark - Controller Methods
- (void)popToRootViewControllerAnimated:(BOOL)animated
{
    [_navigationController popToRootViewControllerAnimated:animated];
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonBack:(id)sender
{
    [_navigationController popViewControllerAnimated:YES];
}

#pragma mark - NavigationController Delegate
- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    self.navigationBar.backButton.hidden = [navigationController.viewControllers count] == 1;
    
    if ([viewController conformsToProtocol:@protocol(SNNavigationControllerProtocol)]) {
        _navigationBar.titleLabel.attributedText = [viewController performSelector:@selector(titleForNavigationBar)];
        
    } else {
        //NSLog(@"Unresolved error %@ doesn't impmement protocol %@", NSStringFromClass([viewController class]), NSStringFromProtocol(@protocol(SNNavigationControllerProtocol)));
	    abort();
    }
    
    [self.navigationBar layoutSubviews];

}

@end
