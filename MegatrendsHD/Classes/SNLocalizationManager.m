//
//  SNLocalizationManager.m
//  MegaTrends
//
//  Created by Александр Жовтый on 19.04.13.
//  Copyright (c) 2013 Line2Biz. All rights reserved.
//

#import "SNLocalizationManager.h"

NSString *const SNLocalizationManagerDidChangeLanguage = @"SNLocalizationManagerDidChangeLanguage";

@implementation SNLocalizationManager

static NSBundle *__bundle = nil;

#pragma mark - Initilization
- (id)init
{
    self = [super init];
    if (self) __bundle = [NSBundle mainBundle];
    return self;
}

+ (SNLocalizationManager *)sharedManager
{
    static dispatch_once_t once;
    static SNLocalizationManager * sharedInstance;
    dispatch_once(&once, ^{ sharedInstance = [[self alloc] init]; });
    return sharedInstance;
}

#pragma mark - Localization methods
- (void)setLanguage:(NSString*)langIdentifier
{
	NSString *path = [[NSBundle mainBundle] pathForResource:langIdentifier ofType:@"lproj" ];
	
	if (path == nil) //in case the language does not exists
		[self resetLocalization];
	else
		__bundle = [NSBundle bundleWithPath:path];
    
    NSArray *array = nil;
    if ([langIdentifier isEqualToString:@"en"]) {
        array = @[@"en", @"de"];
    } else {
        array = @[@"de", @"en"];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SNLocalizationManagerDidChangeLanguage object:self];
    
}

- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment
{
	return [__bundle localizedStringForKey:key value:comment table:nil];
}

- (NSString*)getLanguage
{
	NSArray* languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
	NSString *preferredLang = [languages objectAtIndex:0];
    
    if ([preferredLang isEqualToString:@"en"]) {
        return preferredLang;
    }
    else if ([preferredLang isEqualToString:@"de"])
    {
        return preferredLang;
    }
    
	return @"en";
}

// Resets the localization system, so it uses the OS default language.
- (void)resetLocalization
{
	__bundle = [NSBundle mainBundle];
}


- (NSBundle *)bundle
{
    return __bundle;
}

@end
