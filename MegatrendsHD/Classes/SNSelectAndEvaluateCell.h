//
//  SNSelectAndEvaluateCell.h
//  MegatrendsHD
//
//  Created by ios on 5/14/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNSelectAndEvaluateCell : UITableViewCell
@property (strong, nonatomic) DMTrend *trend;
@property (strong, nonatomic) DMDiagram *diagram;

@end
