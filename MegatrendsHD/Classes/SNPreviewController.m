//
//  SNPreviewController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 10.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNPreviewController.h"
#import "SNChartPreviewController.h"
#import "DMManager.h"

@interface SNPreviewController ()
{
    UIImageView *_imageView;
//    id _observer;
}
@end

@implementation SNPreviewController


- (void)configureView
{
    //    Trend Chart
    NSString *imageName = [NSString stringWithFormat:@"%02d.%dsvg@2x", self.keyFactor.trend.tid + 1, self.keyFactor.kid + 1];
    
    NSBundle *bundle = [[SNLocalizationManager sharedManager] bundle];
    NSString *pictPath = [bundle pathForResource:imageName ofType:@"png"];
    _imageView.image = [UIImage imageWithContentsOfFile:pictPath];
    [_imageView setContentMode:UIViewContentModeScaleAspectFit];
    [_imageView setNeedsDisplay];
    _imageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [_imageView addGestureRecognizer:tap];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    _imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _imageView.backgroundColor = [UIColor blackColor];
    
    
    
    [self.view addSubview:_imageView];
    
    
//    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
//        [self configureView];
//    }];
//    
    
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Outlet Methods
- (IBAction)handleTap:(UITapGestureRecognizer *)sender
{
    [(SNChartPreviewController *)self.parentViewController.parentViewController dismissChartController];

}


@end
