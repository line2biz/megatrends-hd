//
//  SNWorkCell.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 15.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNWorkCell.h"
#import "SNAppearance.h"

@interface SNWorkCell()
{
    __weak IBOutlet UIView *_dottedView;
}
@end

@implementation SNWorkCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    
    _dottedView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Dotted-Line"]];
    //_numberLabel.hidden = YES;
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    for (UIView *view in self.subviews) {
        view.backgroundColor = [UIColor clearColor];
    }
    
}

@end
