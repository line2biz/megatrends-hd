//
//  DMKeyDesc+Category.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 20.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMKeyDesc.h"

FOUNDATION_EXPORT NSString *const DMKeyDescOrderKey;
FOUNDATION_EXPORT BOOL      const DMKeyDescAscendingValue;

@class DMDiagram, DMKeyFactor;

@interface DMKeyDesc (Category)

+ (DMKeyDesc *)keyDescForDiagram:(DMDiagram *)diagram keyFactor:(DMKeyFactor *)keyFactor;

+ (NSArray *)sortDescriptorsArray;

@end
