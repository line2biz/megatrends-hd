//
//  SNAnalyseCell.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 13.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNAnalyseCell.h"
#import "DMManager.h"
#import "SNAppearance.h"
#import "SNAlertController.h"

@interface SNAnalyseCell()
{
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UIButton *_showPDFViewerButton;
}
@end

@implementation SNAnalyseCell


- (void)setAnalyse:(DMAnalyse *)analyse
{
    _analyse = analyse;
    
    if ([_analyse.title length]) {
        _titleLabel.text = _analyse.title;
        _titleLabel.textColor = [SNAppearance colorVinous];
    } else {
        _titleLabel.text = LMLocalizedString(@"Titel der Analyse", nil);
        _titleLabel.textColor = [UIColor grayColor];
    }
    
    _showPDFViewerButton.enabled = _analyse.generated;
    if (_showPDFViewerButton.enabled)
        [_showPDFViewerButton setImage:[UIImage imageNamed:@"Icon-PDF-OK"] forState:UIControlStateNormal];
    else
        [_showPDFViewerButton setImage:[UIImage imageNamed:@"Icon-PDF"] forState:UIControlStateNormal];
    
    
}

- (IBAction)didTapButtonDelete:(id)sender
{
    __weak DMAnalyse *weakAnalyze = self.analyse;
    
    
    NSString *sYES = [LMLocalizedString(@"ja", nil) uppercaseString];
    NSString *sNO = [LMLocalizedString(@"nein", nil) uppercaseString];
    
    
    NSString *title = [LMLocalizedString(@"Achtung!", nil) uppercaseString];
    
    [SNAlertController showAlertWithTitle:title  message:LMLocalizedString(@"Löschen?", nil) okButtonTitle:sYES cancelButtonTitle:sNO completionHandler:^(BOOL completed) {
        if (completed) {
            if (weakAnalyze) {
                NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
                [context deleteObject:weakAnalyze];
                [context save:nil];
                
            }
        }
    }];
}

- (IBAction)didTapButtonShow:(id)sender
{
    [self.delegate analyseCell:self didShowAnalyse:self.analyse];
}


@end
