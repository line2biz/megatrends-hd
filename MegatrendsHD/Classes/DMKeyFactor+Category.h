//
//  DMKeyFactor+Category.h
//  MegatrendsHD
//
//  Created by ios on 5/7/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMKeyFactor.h"

FOUNDATION_EXPORT NSString *const DMKeyFactorOrderKey;
FOUNDATION_EXPORT BOOL      const DMKeyFactorAscendingValue;

@class DMTrend;

@interface DMKeyFactor (Category)

+ (DMKeyFactor *)keyFactorByID:(int16_t)kid forTrend:(DMTrend *)trend;

- (NSString *)title;
- (NSString *)teaser;

- (NSString *)chartSource;
- (NSString *)chartDesc;

@end
