//
//  SNNavigationControllerProtocol.h
//  MegatrendsHD
//
//  Created by ios on 5/10/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SNNavigationControllerProtocol <NSObject>
@required
- (NSAttributedString *)titleForNavigationBar;
@end
