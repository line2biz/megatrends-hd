//
//  DMManager.m
//  MSDCatalog
//
//  Created by Александр Жовтый on 09.02.13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "DMManager.h"

//#define DATA_URL_STRING_VALUE @"http://imageco.ru/upgrade-app/sample.json"
#define DATA_URL_STRING_VALUE @"http://imageco.ru/upgrade-app/issues.json"


NSString *const DMManagerModelNameValue = @"MegatrendsHD";

@implementation DMManager
{
    BOOL _isLoading;
}

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;




#pragma mark - Initialization
+ (DMManager *)sharedManager
{
    static dispatch_once_t once;
    static DMManager *__inctance;
    dispatch_once(&once, ^ { __inctance = [[DMManager alloc] init]; });
    return __inctance;
}

+ (NSManagedObjectContext *)managedObjectContext
{
    return [[DMManager sharedManager] managedObjectContext];
}

- (NSURL *)storeURL
{
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    return [url URLByAppendingPathComponent:[DMManagerModelNameValue stringByAppendingString:@".sqlite"]];
}

#pragma mark - Core Data stack
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:DMManagerModelNameValue withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [self storeURL];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (void)saveContext
{
    NSError *error = nil;
    
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            [[NSFileManager defaultManager] removeItemAtURL:[self storeURL] error:nil];
            //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Server functions
- (void)serializeData:(NSArray *)array
{
    NSManagedObjectContext *nestedContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    nestedContext.parentContext = [[DMManager sharedManager] managedObjectContext];
    
    [nestedContext performBlock:^{
        NSString *lang = [[SNLocalizationManager sharedManager] getLanguage];
        
        [array enumerateObjectsUsingBlock:^(NSDictionary *dictionary, NSUInteger idx, BOOL *stop) {
            DMTrend *trend = [DMTrend trendById:idx];
            if (trend == nil) {
                trend = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMTrend class]) inManagedObjectContext:nestedContext];
                trend.tid = idx;
            } else {
                trend = (DMTrend *)[nestedContext objectWithID:trend.objectID];
            }
            
            NSString *titleKey = [@"title_" stringByAppendingString:lang];
            [trend setValue:[dictionary valueForKey:@"Name"] forKey:titleKey];
            
            NSString *teaserKey = [@"teaser_" stringByAppendingString:lang];
            [trend setValue:[dictionary valueForKey:@"Teaser"] forKey:teaserKey];
            
            NSString *descKey = [@"desc_" stringByAppendingString:lang];
            [trend setValue:[dictionary valueForKey:@"Description"] forKey:descKey];
            
            NSString *chartSourceKey = [@"chartSource_" stringByAppendingString:lang];
            [trend setValue:[dictionary valueForKey:@"ChartSource"] forKey:chartSourceKey];
            
            
            NSArray *arrayKeys = [dictionary valueForKey:@"Keys"];
            [arrayKeys enumerateObjectsUsingBlock:^(NSDictionary *keyDictionary, NSUInteger idxKey, BOOL *stopKey) {
                
                DMKeyFactor *keyFactor = [DMKeyFactor keyFactorByID:idxKey forTrend:trend];
                
                if (keyFactor == nil) {
                    keyFactor = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMKeyFactor class]) inManagedObjectContext:nestedContext];
                    keyFactor.kid = idxKey;
                    keyFactor.trend = trend;
                    
                } else {
                    keyFactor = (DMKeyFactor *)[nestedContext objectWithID:keyFactor.objectID];
                }
                
                NSString *titleKey = [@"title_" stringByAppendingString:lang];
                [keyFactor setValue:[keyDictionary valueForKey:@"Title"] forKey:titleKey];
                
                NSString *teaserKey = [@"teaser_" stringByAppendingString:lang];
                [keyFactor setValue:[keyDictionary valueForKey:@"Teaser"] forKey:teaserKey];
                
                NSString *chartSourceKey = [@"chartSource_" stringByAppendingString:lang];
                [keyFactor setValue:[keyDictionary valueForKey:@"ChartSource"] forKey:chartSourceKey];
                
                NSString *chartDescKey = [@"chartDesc_" stringByAppendingString:lang];
                [keyFactor setValue:[keyDictionary valueForKey:@"Desc"] forKey:chartDescKey];
                
            }];
            
        }];
        
        NSError *error = nil;
        [nestedContext save:&error];
        if (error) {
            //NSLog(@"unhandle error parsing data %@", [error localizedDescription]);
            
        } else {
            NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
            [context performBlock:^{
                NSError *error = nil;
                [context save:&error];
                if (error) {
                    //NSLog(@"unhandle error save main context during parsin data: %@", [error localizedDescription]);
                }
                
            }];
        }
        
        
    }];
}


@end


















