//
//  SNAppDelegate.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 24.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXPORT NSString *const SNAppNoticationDomainURIValue;
FOUNDATION_EXPORT NSString *const SNAppDeviceTokenKey;
FOUNDATION_EXPORT NSString *const SNAppNotificationTypeKey;

@class SNParentController;

@interface SNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) BOOL showNews;


+ (SNAppDelegate *)sharedDelegate;
+ (SNParentController *)parentController;

- (void)setBudgeNumber:(NSUInteger)number;

@end
