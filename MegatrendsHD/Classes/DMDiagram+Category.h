//
//  DMDiagram+Category.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 15.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMDiagram.h"

FOUNDATION_EXPORT NSString  *const DMDiagramSortOrderKey;
FOUNDATION_EXPORT BOOL       const DMDiagramSortOrderAscendingValue;
FOUNDATION_EXPORT NSInteger const DMDiagramPositionMinValue;
FOUNDATION_EXPORT NSInteger const DMDiagramPositionMaxValue;


@interface DMDiagram (Category)

- (void)checkCorner;
- (void)updatePositionValues;

+ (NSArray *)sortDescriptorsArray;


@end
