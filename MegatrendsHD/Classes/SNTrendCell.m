//
//  SNTrendCell.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 27.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNTrendCell.h"
#import "SNAppearance.h"

@interface SNTrendCell()
{
    __weak IBOutlet UILabel *_numberLabel;
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UIImageView *_dottedImageView;
}


@end


@implementation SNTrendCell

- (void)awakeFromNib
{
    [super awakeFromNib];
//    _numberLabel.font = [SNAppearance appFontWithSize:_numberLabel.font.pointSize];
//    _titleLabel.font = [SNAppearance appFontWithSize:_titleLabel.font.pointSize];
    _dottedImageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Dotted-Line"]];
//    self.contentView.backgroundColor = [UIColor clearColor];
//    self.backgroundColor = [UIColor redColor];
}

- (void)setTrend:(DMTrend *)trend
{
    _trend = trend;
    _numberLabel.text = [NSString stringWithFormat:@"%02d:", _trend.tid + 1];
    _titleLabel.text = [_trend title];
    
}


@end
