//
//  SNAppDelegate.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 24.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNAppDelegate.h"
#import "SNParentController.h"


NSString *const SNAppNoticationDomainURIValue = @"http://www.z-punkt.de/api/set_token.php?device=ipad&token=";
NSString *const SNAppNotificationTypeKey = @"SNAppNotificationTypeKey";
NSString *const SNAppDeviceTokenKey = @"SNAppDeviceTokenKey";


@implementation SNAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSBundle *bundle = [[SNLocalizationManager sharedManager] bundle];
    NSString* plistPath = [bundle pathForResource:@"Data" ofType:@"plist"];
    [[DMManager sharedManager] serializeData:[NSArray arrayWithContentsOfFile:plistPath]];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        NSBundle *bundle = [[SNLocalizationManager sharedManager] bundle];
        NSString* plistPath = [bundle pathForResource:@"Data" ofType:@"plist"];
        [[DMManager sharedManager] serializeData:[NSArray arrayWithContentsOfFile:plistPath]];
    }];
    
    
    self.showNews = launchOptions != nil; // Show News Controller
    
//    Notification
    NSDictionary *defaultsDictionary = @{
                                        SNAppNotificationTypeKey : @(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)
                                        };
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults registerDefaults:defaultsDictionary];
    
    
    UIRemoteNotificationType notificationType = [userDefaults integerForKey:SNAppNotificationTypeKey];
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:notificationType];
    
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [[DMManager sharedManager] saveContext];
}


+ (SNAppDelegate *)sharedDelegate
{
    return (SNAppDelegate *)[[UIApplication sharedApplication] delegate];
}

+ (SNParentController *)parentController
{
    SNAppDelegate *appDelegate = (SNAppDelegate *)[UIApplication sharedApplication].delegate;
    return (SNParentController *)appDelegate.window.rootViewController;
}


- (void)setBudgeNumber:(NSUInteger)number
{
    SNAppDelegate *appDelegate = (SNAppDelegate *)[UIApplication sharedApplication].delegate;
    SNParentController *parentController = (SNParentController *)appDelegate.window.rootViewController;
    
    UIImage *image = nil;
    if (number == 0) {
        image = [UIImage imageNamed:@"Icon-ZPunkt"];
    } else {
        image = [UIImage imageNamed:@"Icon-ZPunkt-Badged"];
    }
    
    [parentController.badgeButton setImage:image forState:UIControlStateNormal];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:number];
    
}

#pragma mark - Push Notifications methods

+ (NSString *)deviceToken
{
    NSString *devToken = [[NSUserDefaults standardUserDefaults] valueForKey:SNAppDeviceTokenKey];
    return devToken;
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    [self sendProviderDeviceToken:deviceToken];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    //NSLog(@"%s Failed to get token, error: %@", __FUNCTION__, error);
}

- (void)sendProviderDeviceToken:(NSData *)deviceToken
{
    NSString *tokenString = [deviceToken description];
    NSString *pushToken = [[tokenString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]
                           stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // Save the token to server
    
    [[NSUserDefaults standardUserDefaults] setObject:pushToken forKey:SNAppDeviceTokenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"%s token: %@", __FUNCTION__, pushToken);
    
    NSString *sURI = [SNAppNoticationDomainURIValue stringByAppendingFormat:@"%@&lang_code=%@", pushToken, [[SNLocalizationManager sharedManager] getLanguage]];
    NSURL *url = [NSURL URLWithString:sURI];
    NSURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    //NSLog(@"%@", sURI);
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSURLResponse *response = nil;
        NSError *error = nil;
        
        [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//        NSString *strin = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        //NSLog(@"%s %@", __FUNCTION__, strin);
    });
    
    
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [self setBudgeNumber:1];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:[userInfo valueForKeyPath:@"aps.alert"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [alertView show];
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    SNAppDelegate *appDelegate = (SNAppDelegate *)[UIApplication sharedApplication].delegate;
    SNParentController *parentController = (SNParentController *)appDelegate.window.rootViewController;
    [parentController didTapButtonZPunkt:nil];
    
}



@end
