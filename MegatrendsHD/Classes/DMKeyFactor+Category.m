//
//  DMKeyFactor+Category.m
//  MegatrendsHD
//
//  Created by ios on 5/7/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMTrend+Category.h"
#import "DMKeyFactor+Category.h"


NSString *const DMKeyFactorOrderKey         = @"kid";
BOOL      const DMKeyFactorAscendingValue   = YES;

@implementation DMKeyFactor (Category)

+ (DMKeyFactor *)keyFactorByID:(int16_t)kid forTrend:(DMTrend *)trend
{
    DMKeyFactor *returnValue = nil;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"kid == %d && trend == %@", kid, (DMTrend *)[context objectWithID:trend.objectID]];
    fetchRequest.predicate = predicate;
    fetchRequest.fetchLimit = 10;
    
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    if ([results count]) {
        returnValue = [results lastObject];
    }
    return returnValue;
}

- (NSString *)title
{
    NSString *key = [@"title_" stringByAppendingString:[[SNLocalizationManager sharedManager] getLanguage]];
    return [self valueForKey:key];
}

- (NSString *)teaser
{
    NSString *key = [@"teaser_" stringByAppendingString:[[SNLocalizationManager sharedManager] getLanguage]];
    return [self valueForKey:key];
}

- (NSString *)chartSource
{
    NSString *key = [@"chartSource_" stringByAppendingString:[[SNLocalizationManager sharedManager] getLanguage]];
    return [self valueForKey:key];
}

- (NSString *)chartDesc
{
    NSString *key = [@"chartDesc_" stringByAppendingString:[[SNLocalizationManager sharedManager] getLanguage]];
    return [self valueForKey:key];
}

@end
