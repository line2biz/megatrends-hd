//
//  DMAnalyse.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 17.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMAnalyse.h"
#import "DMDiagram.h"


@implementation DMAnalyse

@dynamic date;
@dynamic desc;
@dynamic sortOrder;
@dynamic title;
@dynamic diagrams;
@dynamic generated;

@end
