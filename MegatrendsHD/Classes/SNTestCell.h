//
//  SNTestCell.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 31.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMKeyDesc, DMDiagram;

@interface SNTestCell : UITableViewCell


@property (strong, nonatomic) DMKeyDesc *keyDesc;
@property (strong, nonatomic) DMDiagram *diagram;

@property (weak, nonatomic) IBOutlet UILabel *placeHolderLabel;

@end
