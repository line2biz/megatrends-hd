//
//  SNNavigationBar.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 26.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNTabBarView.h"
#import "SNAppearance.h"


@interface SNTabBarView()
{
    __weak IBOutlet UILabel *_titleLabel;
}
@end

@implementation SNTabBarView



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Navigation-Bkg"]];
    _titleLabel.font = [SNAppearance boldAppFontWithSize:_titleLabel.font.pointSize];
    
}
@end
