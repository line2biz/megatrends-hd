//
//  SNHomeCartView.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 26.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNHomeCartView.h"
#import "SNAppearance.h"

@implementation SNHomeCartView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.textView.editable = NO;
    self.titleLabel.font = [SNAppearance boldAppFontWithSize:self.titleLabel.font.pointSize];
    self.textView.font = [SNAppearance appFontWithSize:self.textView.font.pointSize];
    self.backgroundColor = [UIColor clearColor];
}

@end
