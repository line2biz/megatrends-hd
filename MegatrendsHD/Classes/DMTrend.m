//
//  DMTrend.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 15.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMTrend.h"
#import "DMDiagram.h"
#import "DMKeyFactor.h"


@implementation DMTrend

@dynamic chartSource_de;
@dynamic chartSource_en;
@dynamic desc_de;
@dynamic desc_en;
@dynamic teaser_de;
@dynamic teaser_en;
@dynamic tid;
@dynamic title_de;
@dynamic title_en;
@dynamic diagrams;
@dynamic keyFactors;

@end
