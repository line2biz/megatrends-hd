//
//  DMAnalyse.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 17.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DMDiagram;

@interface DMAnalyse : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic) int16_t sortOrder;
@property (nonatomic) BOOL generated;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *diagrams;
@end

@interface DMAnalyse (CoreDataGeneratedAccessors)

- (void)addDiagramsObject:(DMDiagram *)value;
- (void)removeDiagramsObject:(DMDiagram *)value;
- (void)addDiagrams:(NSSet *)values;
- (void)removeDiagrams:(NSSet *)values;

@end
