//
//  DMTrend+Category.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 26.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMTrend+Category.h"
#import "SNLocalizationManager.h"


NSString  *const DMTrendSortOrderKey            = @"tid";
BOOL       const DMTrendSortOrderAscendingValue = YES;


@implementation DMTrend (Category)

- (NSString *)title
{
    NSString *key = [@"title_" stringByAppendingString:[[SNLocalizationManager sharedManager] getLanguage]];
    return [self valueForKey:key];
}

- (NSString *)teaser
{
    NSString *key = [@"teaser_" stringByAppendingString:[[SNLocalizationManager sharedManager] getLanguage]];
    return [self valueForKey:key];
}

- (NSString *)desc
{
    NSString *key = [@"desc_" stringByAppendingString:[[SNLocalizationManager sharedManager] getLanguage]];
    return [self valueForKey:key];
}

- (NSString *)chartSource
{
    NSString *key = [@"chartSource_" stringByAppendingString:[[SNLocalizationManager sharedManager] getLanguage]];
    return [self valueForKey:key];
}

- (NSString *)imageName
{
    return [NSString stringWithFormat:@"trend_%02d", self.tid + 1];
}

- (NSString *)imageSmallName
{
    return [NSString stringWithFormat:@"trend_small_%02d", self.tid + 1];
}

#pragma mark - Utility Class Methods
+ (DMTrend *)trendById:(int16_t)tid
{
    DMTrend *trend = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"tid = %d", tid];
    fetchRequest.fetchLimit = 1;
    
    NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    if ([results count]) {
        trend = [results lastObject];
    }
    
    return trend;
}

+ (NSUInteger)numberOfTrends
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    [fetchRequest setIncludesSubentities:NO];
    return [[[DMManager sharedManager] managedObjectContext] countForFetchRequest:fetchRequest error:nil];
}

@end
