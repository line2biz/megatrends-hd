//
//  SNAnalysisController.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 13.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMAnalyse;

@interface SNAnalysisController : UIViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) DMAnalyse *currentAnalyse;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;


@end
