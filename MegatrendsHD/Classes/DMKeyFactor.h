//
//  DMKeyFactor.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 18.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DMKeyDesc, DMTrend;

@interface DMKeyFactor : NSManagedObject

@property (nonatomic) int16_t kid;
@property (nonatomic, retain) NSString * teaser_de;
@property (nonatomic, retain) NSString * teaser_en;
@property (nonatomic, retain) NSString * title_de;
@property (nonatomic, retain) NSString * title_en;
@property (nonatomic, retain) NSSet *keyDesces;
@property (nonatomic, retain) DMTrend *trend;

@property (nonatomic, retain) NSString * chartSource_de;
@property (nonatomic, retain) NSString * chartSource_en;

@property (nonatomic, retain) NSString * chartDesc_de;
@property (nonatomic, retain) NSString * chartDesc_en;



@end

@interface DMKeyFactor (CoreDataGeneratedAccessors)

- (void)addKeyDescesObject:(DMKeyDesc *)value;
- (void)removeKeyDescesObject:(DMKeyDesc *)value;
- (void)addKeyDesces:(NSSet *)values;
- (void)removeKeyDesces:(NSSet *)values;

@end
