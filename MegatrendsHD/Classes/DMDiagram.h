//
//  DMDiagram.h
//  MegatrendsHD
//
//  Created by Dev on 27.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DMAnalyse, DMKeyDesc, DMTrend;

@interface DMDiagram : NSManagedObject

@property (nonatomic, retain) NSString * desc;
@property (nonatomic) int64_t positionY;
@property (nonatomic) int64_t positionX;
@property (nonatomic) int16_t color;
@property (nonatomic) int16_t corner;
@property (nonatomic, retain) DMAnalyse *analyse;
@property (nonatomic, retain) NSSet *keyDescs;
@property (nonatomic, retain) DMTrend *trend;
@end

@interface DMDiagram (CoreDataGeneratedAccessors)

- (void)addKeyDescsObject:(DMKeyDesc *)value;
- (void)removeKeyDescsObject:(DMKeyDesc *)value;
- (void)addKeyDescs:(NSSet *)values;
- (void)removeKeyDescs:(NSSet *)values;

@end
