//
//  SNMessagesController.m
//  MegatrendsHD
//
//  Created by ios on 5/21/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNMessagesController.h"
#import "SNAlertController.h"
#import "SNAppDelegate.h"

@interface SNMessagesController () <UIWebViewDelegate>
{
    __weak IBOutlet UIWebView *_webViewer;
    __weak IBOutlet UIActivityIndicatorView *_activityIndicat;
    
  id _observer;
}


@end

@implementation SNMessagesController

- (void)configureView
{
    NSString *sURI = [[[SNLocalizationManager sharedManager] getLanguage] isEqualToString:@"de"] ? @"http://www.z-punkt.de/index.php?id=44&app=1" : @"http://www.z-punkt.de/index.php?id=428&app=1";
     NSURLRequest *requestObj=[NSURLRequest requestWithURL:[NSURL URLWithString:sURI]];
    [_webViewer loadRequest:requestObj];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    _webViewer.delegate = self;
    [_webViewer setBackgroundColor:[UIColor clearColor]];
    [_webViewer setOpaque:NO];
    [self.view setBackgroundColor:[UIColor clearColor]];
    _activityIndicat.hidesWhenStopped = YES;
    
    
	// Do any additional setup after loading the view.
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self configureView];
    }];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - NSWebViewProtocol


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_activityIndicat startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [[SNAppDelegate sharedDelegate] setBudgeNumber:0];
    [_activityIndicat stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [_activityIndicat stopAnimating];
    
    NSString *title = [LMLocalizedString(@"Achtung!", nil) uppercaseString];
    NSString *msg = error.code == -1009 ? LMLocalizedString(@"Keine Internetverbindung", nil) : [error localizedDescription];
    NSString *sYES = [LMLocalizedString(@"OK", nil) uppercaseString];
    
    [SNAlertController showAlertWithTitle:title message:msg okButtonTitle:sYES cancelButtonTitle:nil completionHandler:NULL];
}

-(BOOL)webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType
{
    if (inType == UIWebViewNavigationTypeLinkClicked ) {
        NSURL *url = [inRequest URL];
        
        NSRange range = [[url absoluteString] rangeOfString:@"#"];
        if (range.location == NSNotFound) {
            [[UIApplication sharedApplication] openURL:[inRequest URL]];
            return NO;
        }
        
    }
    
    return YES;
}


@end
