//
//  DMDiagram+Category.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 15.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMDiagram+Category.h"

NSString  *const DMDiagramSortOrderKey              = @"trend.tid";
BOOL       const DMDiagramSortOrderAscendingValue   = YES;
NSInteger const DMDiagramPositionMinValue           = 0;
NSInteger const DMDiagramPositionMaxValue           = 9;


@implementation DMDiagram (Category)

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    self.positionX = DMDiagramPositionMinValue - 1;
    self.positionY = DMDiagramPositionMinValue - 1;
}

- (void)checkCorner
{
    NSMutableArray *possibleCorners = [NSMutableArray new];

    if (self.positionX == DMDiagramPositionMinValue && self.positionY == DMDiagramPositionMaxValue) {
        [possibleCorners addObject:@(SNCornerRightBottom)];
        
    } else if (self.positionX > DMDiagramPositionMinValue && self.positionX < DMDiagramPositionMaxValue && self.positionY == DMDiagramPositionMaxValue) {
        [possibleCorners addObject:@(SNCornerRightBottom)];
        [possibleCorners addObject:@(SNCornerLeftBottom)];
        
    } else if (self.positionX == DMDiagramPositionMaxValue && self.positionY == DMDiagramPositionMaxValue) {
        [possibleCorners addObject:@(SNCornerLeftBottom)];
        
    } else if (self.positionX == DMDiagramPositionMaxValue && self.positionY < DMDiagramPositionMaxValue && self.positionY > DMDiagramPositionMinValue){
        [possibleCorners addObject:@(SNCornerLeftTop)];
        [possibleCorners addObject:@(SNCornerLeftBottom)];
        
    } else if (self.positionX == DMDiagramPositionMaxValue && self.positionY == DMDiagramPositionMinValue) {
        [possibleCorners addObject:@(SNCornerLeftTop)];
        
    } else if (self.positionX > DMDiagramPositionMinValue && self.positionX < DMDiagramPositionMaxValue && self.positionY == DMDiagramPositionMinValue) {
        [possibleCorners addObject:@(SNCornerLeftTop)];
        [possibleCorners addObject:@(SNCornerRightTop)];
        
    } else if (self.positionX == DMDiagramPositionMinValue && self.positionY == DMDiagramPositionMinValue) {
        [possibleCorners addObject:@(SNCornerRightTop)];
    
    } else if (self.positionX == DMDiagramPositionMinValue && self.positionY > DMDiagramPositionMinValue && self.positionY < DMDiagramPositionMaxValue) {
        [possibleCorners addObject:@(SNCornerRightTop)];
        [possibleCorners addObject:@(SNCornerRightBottom)];
        
    } else {
        [possibleCorners addObject:@(SNCornerLeftTop)];
        [possibleCorners addObject:@(SNCornerRightTop)];
        [possibleCorners addObject:@(SNCornerRightBottom)];
        [possibleCorners addObject:@(SNCornerLeftBottom)];
        
    }
    
    SNCorner newCornerValue = self.corner;
    
    if ([possibleCorners indexOfObject:@(self.corner)] == NSNotFound) {
        newCornerValue = [possibleCorners[0] intValue];
    }
    
    NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMDiagram class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"analyse == %@ && SELF != %@", self.analyse, self];
    NSInteger count = [context countForFetchRequest:fetchRequest error:NULL];
    
    if (count > 0 && [possibleCorners count] > 1) {
        NSMutableArray *countsArray = [NSMutableArray arrayWithCapacity:[possibleCorners count]];
        for (NSNumber *cornerNumber in possibleCorners) {
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"analyse == %@ && SELF != %@ && corner == %d", self.analyse, self, [cornerNumber intValue]];
            NSInteger result = [context countForFetchRequest:fetchRequest error:NULL];
            [countsArray addObject:@(result)];
        }
        
        for (int idx = 0; idx < [possibleCorners count]; idx++) {
            NSInteger currentValue = [[countsArray objectAtIndex:idx] intValue];
            NSInteger nextValue;
            if (idx == [possibleCorners count] - 1) {
                nextValue = [countsArray[0] intValue];
                newCornerValue = [possibleCorners[0] intValue];
            } else {
                nextValue = [countsArray[(idx + 1)] intValue];
                if (nextValue < currentValue) {
                    newCornerValue = [possibleCorners[idx + 1] intValue];
                    break;
                }
            }
            
            
        }

    }
    
    if (newCornerValue != self.corner) {
        self.corner = newCornerValue;
    }
   
}


- (void)updatePositionValues
{
    NSSet *set = self.keyDescs;
    
    if ([set count] == 0) {
        return;
    }
    
    BOOL checkCorners = NO;
    
    NSNumber *maxX = [set valueForKeyPath:@"@max.positionX"];
    
    if (self.positionX != [maxX intValue]) {
        checkCorners = YES;
        self.positionX = [maxX intValue];
    }
    
    NSNumber *maxY = [set valueForKeyPath:@"@max.positionY"];
    if (self.positionY != [maxY intValue]) {
        checkCorners = YES;
        self.positionY = [maxY intValue];
    }
    
    if (checkCorners) {
        [self checkCorner];
    }
    
    NSNumber *maxColor = [set valueForKeyPath:@"@max.type"];
    self.color = [maxColor intValue];
}

#pragma mark  - Class Mehtods
+ (NSArray *)sortDescriptorsArray
{

    NSSortDescriptor *sort_4 = [[NSSortDescriptor alloc] initWithKey:DMDiagramSortOrderKey ascending:DMDiagramSortOrderAscendingValue];
    
    return @[sort_4];
}

+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)theKey
{
    BOOL automatic = NO;
    if ([theKey isEqualToString:@"desc"]) {
        automatic = NO;
    } else {
        automatic = [super automaticallyNotifiesObserversForKey:theKey];
    }
    return automatic;
}

- (void)setPositionX:(int64_t)positionX
{
    [self willChangeValueForKey:@"positionX"];
    if (positionX > 9) {
        positionX = 9;
    } 
    [self setPrimitiveValue:[NSNumber numberWithLongLong:positionX] forKey:@"positionX"];
    [self didChangeValueForKey:@"positionX"];
}

- (void)setPositionY:(int64_t)positionY
{
    [self willChangeValueForKey:@"positionY"];
    if (positionY > 9) {
        positionY = 9;
    }
    [self setPrimitiveValue:[NSNumber numberWithLongLong:positionY] forKey:@"positionY"];
    [self didChangeValueForKey:@"positionY"];
}

@end
