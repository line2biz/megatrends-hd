//
//  SNKeyDetailEmptyCell.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 24.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNKeyDetailEmptyCell.h"
@interface SNKeyDetailEmptyCell()
{
    __weak IBOutlet UIView *_dottedView;
}
@end

@implementation SNKeyDetailEmptyCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    _dottedView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Dotted-Line"]];
}

@end
