//
//  SNRateController.h
//  MegatrendsHD
//
//  Created by Шурик on 5/24/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNRateController : UIViewController

@property (strong, nonatomic) NSManagedObject *managedObject;

+ (void)showRateWithManagedObject:(NSManagedObject *)managedObject;

@end
