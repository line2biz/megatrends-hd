//
//  SNHomeController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 26.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNHomeController.h"
#import "SNTrendCollectionCell.h"
#import "SNParentController.h"
#import "SNTrendsController.h"
#import "SNAppDelegate.h"

@interface SNHomeController () <NSFetchedResultsControllerDelegate>
{
    __weak IBOutlet UICollectionView *_collectionView;
    NSMutableArray *_objectChanges;
    NSMutableArray *_sectionChanges;
    NSCache *_imageCache;
    
    id _observer;
}

@end

@implementation SNHomeController

- (void)configureView
{
    [_collectionView reloadData];
    [self setHomeTextLabels];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    __weak SNHomeController *weekSelf = self;
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        SNHomeController *strongSelf = weekSelf;
        if (strongSelf) {
            [strongSelf configureView];
        }
    }];
    
    _collectionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Collection-Bkg"]];
    _objectChanges = [NSMutableArray new];
    _sectionChanges = [NSMutableArray new];
    _imageCache = [NSCache new];
    [self setHomeTextLabels];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonTrends:(id)sender
{
    SNParentController *parentController = [SNAppDelegate parentController];
    [parentController didTapButtonTrends:sender];
}

- (IBAction)didTapButtonFavorites:(id)sender
{
    SNParentController *parentController = [SNAppDelegate parentController];
    [parentController didTapButtonAnalysis:sender];
}

- (void) setHomeTextLabels
{
    NSString *lang = [[SNLocalizationManager sharedManager] getLanguage];
    if ([lang isEqualToString:@"de"]) {
    _lblUnderstanding.text = @"Megatrends verstehen";
    _textUnderstanding.text = @"Меgatrends sind die zentralen Treiber des globalen Wandels: Sie wirken über Jahrzehnte und Weltregionen hinweg, dabei beeinflussen sie Regierungen, Unternehmen und Einzelpersonen. Mit hoher Wahrscheinlichkeit können sie mindestens 15 Jahre in die Zukunft projiziert werden. Megatrends bilden eine Landkarte der Zukunft – sich über sie zu informieren ist Voraussetzung für erfolgreiche Strategiearbeit.";
    _lblAnalysing.text = @"Megatrends analysieren";
    _textAnalysing.text = @"Nicht alle Megatrends oder Subtrends sind für jedes Unternehmen gleichermaßen relevant – entscheidend ist die Branchenperspektive. Megatrends HD hilft Ihnen, die Entwicklung zukünftiger Märkte zu verstehen und zu analysieren. Filtern Sie die Megatrends nach Chancen und Risiken für das eigene Geschäft: In wenigen Schritten erstellen Sie hier Ihre persönlichen Zukunftsanalysen.";
    } else {
        _lblUnderstanding.text = @"Understanding megatrends";
        _textUnderstanding.text = @"Мegatrends are key drivers of global change; their impacts play out over the course of several decades and across all parts of the world. They influence governments, companies and individuals alike. It is possible to make predictions about them that stretch at least fifteen years into the future. Megatrends form a map of the future – being aware of them is a prerequisite for any successful strategist.";
        _lblAnalysing.text = @"Analysing megatrends";
        _textAnalysing.text = @"Not all megatrends or subtrends are equally relevant for all companies. Often, it is a question of the sector view. Megatrends HD helps you to understand and analyse how markets of the future might develop. You can filter megatrends according to the opportunity or threat they pose for your company. In just a few steps, you are able to create your own personalised piece of future analysis.";
    }
    
}

#pragma mark - Collection View Datasource
- (NSUInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (void)configureCollectionCell:(SNTrendCollectionCell *)cell forIndexPath:(NSIndexPath *)indexPath
{
    DMTrend *trend = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.numberLabel.text = [NSString stringWithFormat:@"%02ld", (unsigned long)(indexPath.row + 1)];
    cell.trend = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.imageView.image = [UIImage imageNamed:trend.imageSmallName];
    cell.dividerImageView.hidden = (indexPath.row == [[self.fetchedResultsController fetchedObjects] count] - 1);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SNTrendCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    [self configureCollectionCell:cell forIndexPath:indexPath];
    return cell;
}

#pragma mark - Collection View Delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    DMTrend *trend = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [(SNParentController *)self.parentViewController performSelector:@selector(trendsController:didSelecteTrend:) withObject:nil withObject:trend];
}

#pragma mark - FetchedReults Controller
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    if (self.managedObjectContext == nil) {
        self.managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([DMTrend class]) inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:DMTrendSortOrderKey ascending:DMTrendSortOrderAscendingValue];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Trends"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	    //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}

#pragma mark - Fetched Results Controller Delegate
- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    NSMutableDictionary *change = [NSMutableDictionary new];
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            change[@(type)] = @[@(sectionIndex)];
            break;
        case NSFetchedResultsChangeDelete:
            change[@(type)] = @[@(sectionIndex)];
            break;
    }
    
    [_sectionChanges addObject:change];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    NSMutableDictionary *change = [NSMutableDictionary new];
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            change[@(type)] = newIndexPath;
            break;
        case NSFetchedResultsChangeDelete:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeUpdate:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeMove:
            change[@(type)] = @[indexPath, newIndexPath];
            break;
    }
    [_objectChanges addObject:change];
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if ([_sectionChanges count] > 0)
    {
        [_collectionView performBatchUpdates:^{
            
            for (NSDictionary *change in _sectionChanges)
            {
                [change enumerateKeysAndObjectsUsingBlock:^(NSNumber *key, id obj, BOOL *stop) {
                    
                    NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                    switch (type)
                    {
                        case NSFetchedResultsChangeInsert:
                            [_collectionView insertSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                        case NSFetchedResultsChangeDelete:
                            [_collectionView deleteSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                        case NSFetchedResultsChangeUpdate:
                            [_collectionView reloadSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                    }
                }];
            }
        } completion:nil];
    }
    
    if ([_objectChanges count] > 0 && [_sectionChanges count] == 0)
    {
        [_collectionView performBatchUpdates:^{
            
            for (NSDictionary *change in _objectChanges)
            {
                [change enumerateKeysAndObjectsUsingBlock:^(NSNumber *key, id obj, BOOL *stop) {
                    
                    NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                    switch (type)
                    {
                        case NSFetchedResultsChangeInsert:
                            [_collectionView insertItemsAtIndexPaths:@[obj]];
                            break;
                        case NSFetchedResultsChangeDelete:
                            [_collectionView deleteItemsAtIndexPaths:@[obj]];
                            break;
                        case NSFetchedResultsChangeUpdate:
                            [_collectionView reloadItemsAtIndexPaths:@[obj]];
                            break;
                        case NSFetchedResultsChangeMove:
                            [_collectionView moveItemAtIndexPath:obj[0] toIndexPath:obj[1]];
                            break;
                    }
                }];
            }
        } completion:nil];
    }
    
    [_sectionChanges removeAllObjects];
    [_objectChanges removeAllObjects];
}

@end
