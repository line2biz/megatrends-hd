//
//  SNNavigationBar.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 10.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNNavigationBar.h"
#import "SNAppearance.h"


#define BACK_BUTTON_WIDTH 52.f
#define HORIZONTAL_SPACING 20.f

@interface SNNavigationBar()
{
    __weak IBOutlet NSLayoutConstraint *_backButtonWithConstriant;
    __weak IBOutlet NSLayoutConstraint *_horizontalSpacing;
    
}
@end

@implementation SNNavigationBar

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Navigation-Bar-Bkg"]];
    _backButtonWithConstriant.constant = 0;
    _horizontalSpacing.constant = HORIZONTAL_SPACING;
    self.titleLabel.font = [SNAppearance appFontWithSize:self.titleLabel.font.pointSize];
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _backButtonWithConstriant.constant = self.backButton.hidden ? 0 : BACK_BUTTON_WIDTH;
    _horizontalSpacing.constant = self.backButton.hidden ? HORIZONTAL_SPACING : 0;
    
    [UIView animateWithDuration:.2 animations:^{
        [self layoutIfNeeded];
    }];
    
}

@end
