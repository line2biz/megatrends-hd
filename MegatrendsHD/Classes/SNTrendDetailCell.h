//
//  SNTrendDetailCell.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 17.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMTrend;

@interface SNTrendDetailCell : UITableViewCell

@property (strong, nonatomic) DMTrend *trend;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *dottedLine;

@end
