//
//  DMKeyDesc.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 18.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DMDiagram, DMKeyFactor;

@interface DMKeyDesc : NSManagedObject

@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) DMDiagram *diagram;
@property (nonatomic, retain) DMKeyFactor *keyFactor;

@property (nonatomic) int16_t positionY;
@property (nonatomic) int16_t positionX;
@property (nonatomic) int16_t type;

@end
