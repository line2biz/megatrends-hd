//
//  SNZpunktController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 13.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNZpunktController.h"
#import "SNNavigationController.h"
#import "SNAppearance.h"
#import "SNTextController.h"
#import "SNMessagesController.h"
#import "SNAppDelegate.h"

typedef NS_ENUM (NSInteger, SNZpunktType) {
    SNZpunktTypeNews,
    SNZpunktTypeAbout,
    SNZpunktTypeContact
};

@interface SNZpunktController () <SNNavigationControllerProtocol>
{
    __weak IBOutlet UIButton *_newsButton;
    __weak IBOutlet UIButton *_aboutButton;
    __weak IBOutlet UIButton *_contactButton;
    __weak IBOutlet UIView *_containerView;
    __weak IBOutlet UIImageView *_logoImageView;
    
    id _observer;
    UIViewController *_currentContrller;
    UIButton *_selectedButton;
    
    NSArray *_buttonsArray;
    
}

@end

@implementation SNZpunktController


- (void)configureView
{
    
    void(^ConfigueTypeButton)(UIButton *button, SNZpunktType zType) = ^(UIButton *button, SNZpunktType zType) {
        
        button.tag = zType;
        button.titleLabel.numberOfLines = 2;
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
        button.titleLabel.font = [SNAppearance appFontWithSize:button.titleLabel.font.pointSize];
        
        NSString *title = nil;
        switch (zType) {
            case SNZpunktTypeNews:
                title = LMLocalizedString(@"News", @"Button info name");
                break;
            case SNZpunktTypeAbout:
                title = LMLocalizedString(@"Z_punkt", @"Button info name");
                break;
            case SNZpunktTypeContact:
                title = LMLocalizedString(@"Kontakt", @"Button info name");
                break;
                
        }
        
        [button setTitle:title forState:UIControlStateNormal];
        
    };
    
    
    ConfigueTypeButton(_newsButton, SNZpunktTypeNews);
    ConfigueTypeButton(_aboutButton, SNZpunktTypeAbout);
    ConfigueTypeButton(_contactButton, SNZpunktTypeContact);
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _buttonsArray = @[_newsButton, _aboutButton, _contactButton];
    
    _containerView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Profile-Container-Bkg"]];
    
    __weak SNZpunktController *weekSelf = self;
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
       SNZpunktController *strongSelf = weekSelf;
        if (strongSelf) {
            [strongSelf configureView];
            [self didTapButtonInfoType:_selectedButton];
        }
    }];
    
    [self didTapButtonInfoType:_newsButton];
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Container Methods
- (void)showNewController:(UIViewController *)viewController
{
    
    //    Show New Controller
    [self addChildViewController:viewController];
    viewController.view.frame = _containerView.bounds;
    viewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [_containerView addSubview:viewController.view];
    [viewController didMoveToParentViewController:self];
    
    //    Remove Old Controller
    [_currentContrller willMoveToParentViewController:nil];
    [_currentContrller.view removeFromSuperview];
    [_currentContrller removeFromParentViewController];
    
    _currentContrller = viewController;
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonInfoType:(id)sender
{    
    /*
    if (_selectedButton == sender) {
        return;
    }
    */
    for (UIButton *btn in _buttonsArray) {
        btn.selected = btn == sender;
    }
    
    _selectedButton = sender;
    
    SNNavigationController *navController = (SNNavigationController *)self.parentViewController.parentViewController;
    [navController configureView];
    
    if (_selectedButton == _aboutButton) {
        SNTextController *textController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTextController"];
        textController.attributedText = [self textForTyep:SNZpunktTypeAbout];
        [self showNewController:textController];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Profile-Container-Bkg"]];
        _logoImageView.hidden = NO;
    } else if (_selectedButton == _contactButton) {
        SNTextController *textController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTextController"];
        textController.attributedText = [self textForTyep:SNZpunktTypeContact];
        [self showNewController:textController];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Profile-Container-Bkg"]];
        _logoImageView.hidden = NO;
    } else if (_selectedButton == _newsButton) {
        SNTextController *webController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNMessagesController"];
        [self showNewController:webController];
        self.view.backgroundColor = [UIColor clearColor];
        _logoImageView.hidden = YES;
        
    }
    
}

- (NSAttributedString *)textForTyep:(SNZpunktType)type
{
    NSString *lang = [[SNLocalizationManager sharedManager] getLanguage];
    
    NSString *title = nil;
    
    NSArray *headers = [NSArray new];
    NSMutableArray *array = [NSMutableArray new];
    
    if (type == SNZpunktTypeAbout) {
        if ([lang isEqualToString:@"de"]) {
                        
            title = @"WE FUTURIZE YOUR BUSINESS";
            
            headers = @[[NSNull null], @"STRATEGISCHE ZUKUNFTSANALYSEN FÜR ENTSCHEIDUNGEN UNTER UNSICHERHEIT", @"360 GRAD IM BLICK: DIE RELEVANTEN FAKTOREN AUF DEM ZUKUNFTSRADAR", @"DIE RICHTIGEN TOOLS UND METHODEN FÜR DEN STRATEGISCHEN DIALOG", @"FORESIGHT PROZESSE: MEHRWERT DURCH WISSENSVERNETZUNG"];
            
            [array addObject:@"Z_punkt ist ein international tätiges Beratungsunternehmen für strategische Zukunftsfragen. Wir sind Experten für Corporate Foresight – die Übersetzung von Trend- und Zukunftsforschung in die Praxis des strategischen Managements. Mit wertorientierter Beratung gestaltet Z_punkt strategische Zukunftsdialoge und unterstützt Unternehmen bei der Erschließung nachhaltiger Zukunftsmärkte."];
            
            [array addObject:@"Welche Umbrüche kommen auf die Branche zu? Wo steht das Unternehmen in 10 Jahren? Ist das Portfolio zukunftssicher? Weichen, die heute gestellt werden, haben massive Auswirkungen auf die Zukunftsfähigkeit eines Unternehmens. Strategische Entscheidungen brauchen eine langfristige Perspektive als Orientierungsrahmen. Wir unterstützen unsere Kunden mit systematischen Zukunftsanalysen bei der Strategiefindung und Strategieabsicherung unter unsicheren und volatilen Bedingungen."];
            
            [array addObject:@"Unternehmen kennen ihre Märkte genau. Für die systemischen Einflüsse des Umfelds haben aber selbst große Organisationen oftmals kein ausgeprägtes Sensorium. In einer 360 Grad-Analyse nehmen wir alle relevanten Entwicklungen in der Gesellschaft und bei den Konsumenten, im wirtschaftlichen Umfeld, in Politik und Regulierung, bei Zukunftstechnologien und Ressourcen auf den Radar und übersetzen Trends und Szenarien in Handlungswissen für Unternehmen."];
            
            [array addObject:@"Zukunft braucht die konstruktive Debatte: Wir gestalten Prozesse und strategische Dialoge, die Unternehmen auf Zukunftskurs bringen. Wir greifen dafür auf ein breites Methodenspektrum zurück – von ausgereiften Szenario-Tools, innovativen Workshop-Formaten bis hin zur quantitativen Modellierung mit System Dynamics. Zugleich vertreten wir die relevanten neuen Themen für das Geschäft unserer Kunden und sehen uns in der Verantwortung, diese in deren Unternehmen zu tragen. Dabei fühlen wir uns auch in hohem Maße den Zielen einer nachhaltigen Entwicklung verpflichtet."];
            
            [array addObject:@"In der Regel arbeiten wir prozessorientiert und stehen in engem Austausch mit unseren Kunden. Unser Vorgehen passen wir an deren Bedürfnisse an. Wir steuern den methodischen Gesamtprozess, moderieren Workshops, erarbeiten Inhalte und Reports und unterstützen bei der Kommunikation der Ergebnisse im Unternehmen und zu den Stakeholdern. Darüber hinaus erstellen wir Auftragsstudien und gestalten Zukunftsworkshops mit Gruppen jeder Größe. In jedem Format legen wir hohen Wert auf die Anschlussfähigkeit der Ergebnisse an bestehende Strategie- und Innovationsprozesse, um das langfristige Denken in den Unternehmen nachhaltig zu verankern."];
            
            
        } else {
            title = @"WE FUTURIZE YOUR BUSINESS";
            
            headers = @[[NSNull null], @"STRATEGIC FUTURE ANALYSIS TO HELP MAKE DECISIONS IN AN UNCERTAIN ENVIRONMENT", @"A 360-DEGREE OVERVIEW: KEY FACTORS FOR THE FUTURE ON THE RADAR", @"THE RIGHT TOOLS AND METHODS FOR STRATEGIC DIALOGUE", @"FORESIGHT PROCESSES: ADDING VALUE BY LINKING KNOWLEDGE"];
            
            [array addObject:@"Z_punkt The Foresight Company is a leading strategy and foresight consultancy, operating internationally and focussing on strategic future issues. We are experts in corporate foresight - the translation of findings derived from trend and futures research into practical advice to assist with strategic management. With value-orientated advice at the heart of all our activities, Z_punkt shapes strategic future dialogues and supports companies to make the most of sustainable future markets."];
            
            [array addObject:@"Do you know what upheavals are in store for your sector? Do you have a clear idea of where your company will be in 10 years’ time? Is your portfolio future-proof? Courses set today can have a serious impact on the future viability of your company. Strategic decisions need to be geared around a long-term view. We support our clients by systematically analysing the future, developing and securing the right strategy, even in volatile and uncertain conditions."];
            
            [array addObject:@"Companies often know their markets extremely well. But, even large organisations sometimes lack a clear appreciation of the systematic influences affecting their corporate environment. We specialise in analysing the corporate environment. As part of a 360-degree analysis, we flag up all the key social developments and those concerning consumers, as well as factors influencing the economic environment, politics and regulation, technologies of the future, and resources – translating trends and scenarios into instructional knowledge for your company. In doing so, we are guided, in particular, by the objectives of sustainable development."];
            
            [array addObject:@"The future needs constructive debate. We design participative processes that help put a company on the right course for the future. The basis for achieving this is a wide spectrum of methods, ranging from tried-and-tested scenario tools and innovative workshop formats right up to the quantitative modelling using system dynamics. At the same time, we put forward new issues, and see it as our task to bring them to the company’s attention. In doing so, we are also closely wedded to the aims of sustainable development."];
            
            [array addObject:@"We usually work in a process-driven manner, i.e. in close exchange with the client, adapting our approach to the needs of our clients. We steer the whole methodological process, moderate workshops, create content and reports, and support you to communicate the results within the company and to your stakeholders. In addition, we also produce studies, such as trend reports, on behalf of clients, and put together future workshops for groups of any size. Each format places great emphasis on the compatibility of the results with existing strategy and innovation processes, so as to anchor long-term thinking permanently within the company."];
        }
            
    } else if (type == SNZpunktTypeContact) {
        if ([lang isEqualToString:@"en"]) {
            title = @"SUPPORT OPTIONS";
            
            headers = @[[NSNull null], @"Contact", @"Disclaimer"];
            
            [array addObject:@"We are on hand to answer any technical or content-related queries you may have. If you have any comments or improvements for the app, we’d love to hear from you."];
            
            [array addObject:@"Z_punkt GmbH The Foresight Company\nAnna-Schneider-Steig 2\n50678 Cologne\nGermany\n\nEmail: info@z-punkt.de\nwww.z-punkt.de\n\nTel. +49 (0) 221 3555 340\nFax +49 (0) 22135553422\n\n© Z_punkt The Foresight Company 2013\nDesign: www.informationdesign.de"];
            
            
            [array addObject:@"The trend information provided in this app is derived from publicly-accessible sources. Z_punkt The Foresight Company gives no warranty as to the accuracy or completeness of the information. Without any accompanying discussion or advice, the material provided remains incomplete."];
            
            
        } else {
            title = @"SUPPORT OPTIONEN";
            
            headers = @[[NSNull null], @"Kontakt", @"Disclaimer"];
            
            [array addObject:@"Wir vermitteln Ihnen jederzeit einen Ansprechpartner für alle inhaltlichen oder technischen Fragen. Lob und Kritik zu unserer App nehmen wir gerne entgegen."];
            
            [array addObject:@"Z_punkt GmbH The Foresight Company\nAnna-Schneider-Steig 2\n50678 Köln\n\nE-Mail: info@z-punkt.de\nwww.z-punkt.de\n\nFon +49.221.3555.34.0\nFax +49.221.3555.34.22\n\n© Z_punkt The Foresight Company 2013\nDesign: www.informationdesign.de"];
            
            [array addObject:@"￼￼￼￼￼Die in dieser App zur Verfügung gestellten Trendinformationen basieren auf öffentlich zugänglichen Quellen. Z_punkt The Foresight Company übernimmt keine Gewähr für die Richtigkeit und Vollständigkeit der dargestellten Inhalte. Das zur Verfügung gestellte Material ist ohne begleitende Beratung und Diskussion unvollständig."];
        }
    }
    
    
    
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[[title uppercaseString] stringByAppendingString:@"\n"]];
    
    UIFont *headerFont = [SNAppearance boldAppFontWithSize:17];
    UIFont *paraFont  = [SNAppearance appFontWithSize:17];
    
    NSRange rangeTitle = NSMakeRange(0, [attrString length]);
    [attrString addAttribute:NSFontAttributeName value:headerFont range:rangeTitle];
    [attrString addAttribute:NSForegroundColorAttributeName value:[SNAppearance colorVinous] range:rangeTitle];
    
    
    
    
    
    
    
    for (int idx = 0; idx < [headers count]; idx++) {
        
        NSUInteger location = [attrString.mutableString length];
        NSRange headerRange;
        if (headers[idx] == [NSNull null]) {
            headerRange = NSMakeRange(location, 0);
        } else {
            NSString *header = [NSString stringWithFormat:@"%@\n", headers[idx]];
            [attrString.mutableString appendString:header];
            
            headerRange = NSMakeRange(location, [header length]);
            [attrString addAttribute:NSFontAttributeName value:headerFont range:headerRange];
            [attrString addAttribute:NSFontAttributeName value:headerFont range:headerRange];
            [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:headerRange];
            
            
        }
        
        
        
        
        
        location = [attrString.mutableString length];
        
        NSString *paragraph = [NSString stringWithFormat:@"%@\n\n", array[idx]];
        [attrString.mutableString appendString:paragraph];
        NSRange paraRange = NSMakeRange(location, [paragraph length]);
        [attrString addAttribute:NSFontAttributeName value:paraFont range:paraRange];
        [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:paraRange];
        
        
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.paragraphSpacing = 5;
    // setting the font below makes line spacing become ignored
    [attrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:rangeTitle];
    
    return attrString;
    
}

#pragma mark - Container Protocol
- (NSAttributedString *)titleForNavigationBar
{
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] init];
    
    UIFont *boldFont = [SNAppearance boldAppFontWithSize:17];
    
    NSString *sTitle = [@"INFO Z_PUNKT" stringByAppendingString:@": "];
    NSRange range = NSMakeRange(0, sTitle.length);
    [attrString.mutableString appendString:sTitle];
    
    [attrString addAttribute:NSFontAttributeName value:boldFont range:range];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
    
    NSString *sSubTitle = nil;
    
    switch (_selectedButton.tag) {
        case SNZpunktTypeNews:
            sSubTitle = LMLocalizedString(@"Future\nNews", @"Button info name");
            break;
        case SNZpunktTypeAbout:
            sSubTitle = LMLocalizedString(@"Über\nz_punkt", @"Button info name");
            break;
        case SNZpunktTypeContact:
            sSubTitle = LMLocalizedString(@"Kontakt &\nSupport", @"Button info name");
            break;
    }
    
    sSubTitle = [sSubTitle stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    range = NSMakeRange(NSMaxRange(range), sSubTitle.length);
    [attrString.mutableString appendString:sSubTitle];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:range];
    
    return attrString;
    
}

@end
