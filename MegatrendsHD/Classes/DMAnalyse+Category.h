//
//  DMAnalyse+Category.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 13.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMAnalyse.h"

FOUNDATION_EXPORT NSString  *const DMAnalyseSortOrderKey;
FOUNDATION_EXPORT BOOL       const DMAnalyseSortOrderAscendingValue;

@class DMTrend, DMDiagram;

@interface DMAnalyse (Category)

- (DMDiagram *)addTrend:(DMTrend *)trend;

@end
