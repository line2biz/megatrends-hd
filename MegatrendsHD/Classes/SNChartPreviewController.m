//
//  SNChartPreviewController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 10.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNChartPreviewController.h"
#import "SNPreviewController.h"
#import "DMManager.h"
#import "SNChartController.h"
#import "SNKeyController.h"

@interface SNChartPreviewController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate>
{
    __weak IBOutlet UIView *_containerView;
    __weak IBOutlet UIPageControl *_pageControl;
    
    UIPageViewController *_pageViewController;

}


@end

@implementation SNChartPreviewController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _pageViewController.delegate = self;
    _pageViewController.dataSource = self;
    _pageViewController.view.backgroundColor = [UIColor blackColor];
    
    UIViewController *viewController = [self viewControllerForPageIndex:self.keyFactor.kid];
    [_pageViewController setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:NULL];
    
    _pageControl.numberOfPages = [self.trend.keyFactors count];
    _pageControl.currentPage = self.keyFactor.kid;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Segueway
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[UIPageViewController class]]) {
        _pageViewController = segue.destinationViewController;
        
        
    }
}

#pragma mark - Outlet Methods
- (IBAction)handleTap:(UITapGestureRecognizer *)sender
{
    CGPoint location = [sender locationInView:self.view];
    if (!CGRectContainsPoint(_containerView.frame, location)) {
        [self dismissChartController];
    }
    
}

- (void)dismissChartController
{
    [self dismissViewControllerAnimated:NO completion:^{
        
        [self.chartController setKeyFactor:self.keyFactor animated:NO];
//        self.chartController.keyFactor = self.keyFactor;
//        [self.chartController configureView];
        
    }];
}

#pragma mark - PageViewController DataSource
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    int16_t idx = [[(SNPreviewController *)viewController keyFactor] kid] - 1;
    return [self viewControllerForPageIndex:idx];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    int16_t idx = [[(SNPreviewController *)viewController keyFactor] kid]+ 1;
    return [self viewControllerForPageIndex:idx];
}

- (UIViewController *)viewControllerForPageIndex:(NSInteger)idx
{
    DMKeyFactor *keyFactor = [DMKeyFactor keyFactorByID:idx forTrend:self.trend];
    if (keyFactor) {
        SNPreviewController *viewController = [[SNPreviewController alloc] init];
        viewController.keyFactor  = keyFactor;
        return viewController;
    }
    
    return nil;
    
}

#pragma mark - PageController Delegate
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    SNPreviewController *keyController = (SNPreviewController *)[pageViewController.viewControllers lastObject];
    _pageControl.currentPage = keyController.keyFactor.kid;
    self.keyFactor = keyController.keyFactor;

}

@end
