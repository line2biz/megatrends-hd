//
//  SNTrendsPageController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 27.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNAppDelegate.h"
#import "SNTrendsPageController.h"
#import "SNTrendProfileController.h"
#import "SNChartPreviewController.h"
#import "SNNavigationController.h"
#import "SNAppearance.h"

@interface SNTrendsPageController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate, SNNavigationControllerProtocol>
{
    UIPageViewController *_pageViewController;
    __weak IBOutlet UIPageControl *_pageControl;
    
    DMTrend *_trend;
}

@end

@implementation SNTrendsPageController

@dynamic trend;

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _pageControl.numberOfPages = [DMTrend numberOfTrends];
    
    _pageViewController.view.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
    _pageViewController.dataSource = self;
    _pageViewController.delegate = self;
    self.trend = _trend;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Property Accessors
- (DMTrend *)trend
{
    if ([_pageViewController.viewControllers count]) {
        SNTrendProfileController *currentController = [_pageViewController.viewControllers lastObject];
        return currentController.trend;
    }
    
    return nil;
}

- (void)setTrend:(DMTrend *)trend
{
    if (_pageViewController == nil) { // If the trend is set before viewDidLoad
        _trend = trend;
        return;
    }
    
    SNTrendProfileController *currentController = [_pageViewController.viewControllers lastObject];
    if (currentController.trend != trend) {
        SNTrendProfileController *trendController = (SNTrendProfileController *)[self viewControllerForPageIndex:trend.tid];
        UIPageViewControllerNavigationDirection direction = (currentController.trend.tid < trendController.trend.tid) ? UIPageViewControllerNavigationDirectionForward : UIPageViewControllerNavigationDirectionReverse;
        
        __weak SNTrendsPageController *weakSelf = self;
        
        [_pageViewController setViewControllers:@[trendController] direction:direction animated:YES completion:^(BOOL finished) {
            
            SNTrendsPageController *strongSelf = weakSelf;
            if (strongSelf) {
                SNNavigationController *navCon = (SNNavigationController *)strongSelf.parentViewController.parentViewController;
                [navCon configureView];
                
            }
            
        }];
        
        
    }
    
    _pageControl.currentPage = trend.tid;
}


#pragma mark - Segueways
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[UIPageViewController class]]) {
        _pageViewController = segue.destinationViewController;
    }
}

#pragma mark - Outlet Methods
- (IBAction)pageControlValueChanged:(UIPageControl *)sender
{
    self.trend = [DMTrend trendById:sender.currentPage];
}


#pragma mark - PageViewController DataSource
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    int16_t idx = [[(SNTrendProfileController *)viewController trend] tid] - 1;
    DMTrend *trend = [DMTrend trendById:idx];
    if (trend) {
        return [self viewControllerForPageIndex:idx];
    } else {
        return nil;
    }

    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    int16_t idx = [[(SNTrendProfileController *)viewController trend] tid] + 1;
    DMTrend *trend = [DMTrend trendById:idx];
    if (trend) {
        return [self viewControllerForPageIndex:idx];
    } else {
        return nil;
    }
}

- (UIViewController *)viewControllerForPageIndex:(NSInteger)idx
{
    SNTrendProfileController *trendController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTrendProfileController"];
    trendController.trend  = [DMTrend trendById:idx];
    return trendController;
    
}

#pragma mark - PageController Delegate
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    SNTrendProfileController *trendController = (SNTrendProfileController *)[pageViewController.viewControllers lastObject];
    _pageControl.currentPage = trendController.trend.tid;
    self.trend = trendController.trend;

    if ([pageViewController.viewControllers count]) {
        SNNavigationController *navController = (SNNavigationController *)self.parentViewController.parentViewController;
        [navController configureView];
    }
}



#pragma mark - Container Protocol
- (NSAttributedString *)titleForNavigationBar
{
    NSString *sNumber = [NSString stringWithFormat:@"%02d: ", self.trend.tid + 1];
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:sNumber];
    
    UIFont *boldFont = [SNAppearance boldAppFontWithSize:17];
    
    NSRange range = NSMakeRange(0, attrString.length);
    [attrString addAttribute:NSFontAttributeName value:boldFont range:range];
    [attrString addAttribute:NSForegroundColorAttributeName value:[SNAppearance colorVinous] range:range];
    
    NSString *sTitle = [self.trend.title uppercaseString];
    range = NSMakeRange(NSMaxRange(range), sTitle.length);
    [attrString.mutableString appendString:sTitle];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
    
    return attrString;
    
}



@end
