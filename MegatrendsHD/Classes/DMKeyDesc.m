//
//  DMKeyDesc.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 18.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMKeyDesc.h"
#import "DMDiagram.h"
#import "DMKeyFactor.h"


@implementation DMKeyDesc

@dynamic desc;
@dynamic diagram;
@dynamic keyFactor;

@dynamic positionY;
@dynamic positionX;
@dynamic type;

@end
