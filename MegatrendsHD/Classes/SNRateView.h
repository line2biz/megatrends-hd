//
//  SNRateView.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 20.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, SNRateViewType){
    SNRateViewTypeChance,
    SNRateViewTypeRisk,
    SNRateViewTypeHandle
};

@interface SNRateView : UIView

@property (nonatomic) SNRateViewType rateType;
@property (nonatomic) NSUInteger rateValue;

@end
