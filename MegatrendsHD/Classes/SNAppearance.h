//
//  SNAppearance.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 26.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNAppearance : NSObject

+ (UIFont *)boldAppFontWithSize:(CGFloat)pointSize;

+ (UIFont *)appFontWithSize:(CGFloat)pointSize;

+ (UIColor *)colorVinous;
+ (UIColor *)colorGray;
+ (UIColor *)colorChance;
+ (UIColor *)colorRisk;
+ (UIColor *)colorForHandleValue:(NSUInteger)value;

@end
