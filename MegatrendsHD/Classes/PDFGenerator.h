//
//  PDFGenerator.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 20.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DMAnalyse;

@interface PDFGenerator : NSObject

@property (strong, readonly) NSString *fileName;

- (NSString *)generatePDFForAnalyze:(DMAnalyse *)analyze;

@end
