//
//  DMDiagram.m
//  MegatrendsHD
//
//  Created by Dev on 27.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMDiagram.h"
#import "DMAnalyse.h"
#import "DMKeyDesc.h"
#import "DMTrend.h"


@implementation DMDiagram

@dynamic desc;
@dynamic positionX;
@dynamic positionY;
@dynamic color;
@dynamic corner;
@dynamic analyse;
@dynamic keyDescs;
@dynamic trend;

@end
