//
//  SNAppearance.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 26.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNAppearance.h"

@implementation SNAppearance


+ (UIFont *)boldAppFontWithSize:(CGFloat)pointSize
{
    return [UIFont fontWithName:@"Avenir-Heavy" size:pointSize];
}

+ (UIFont *)appFontWithSize:(CGFloat)pointSize
{
   return [UIFont fontWithName:@"Avenir-Medium" size:pointSize];
}

+ (UIColor *)colorVinous
{
    return [UIColor colorWithRed:153/255.f green:0 blue:0 alpha:1];
}

+ (UIColor *)colorGray
{
    return [UIColor colorWithRed:200/255.f green:200/255.f blue:200/255.f alpha:1];
}

+ (UIColor *)colorChance
{
    return [UIColor colorWithRed:151/255.f green:191/255.f blue:13/255.f alpha:1];
}

+ (UIColor *)colorRisk
{
    return [UIColor colorWithRed:153/255.f green:0/255.f blue:0/255.f alpha:1];
}

+ (UIColor *)colorForHandleValue:(NSUInteger)value
{
    UIColor *color = [SNAppearance colorGray];
    switch (value) {
        case 1:
            color = [UIColor colorWithRed:253/255.f green:195/255.f blue:0/255.f alpha:1];
            break;
        case 2:
            color = [UIColor colorWithRed:243/255.f green:145/255.f blue:0/255.f alpha:1];
            break;
        case 3:
            color = [UIColor colorWithRed:232/255.f green:75/255.f blue:4/255.f alpha:1];
            break;
        case 4:
            color = [UIColor colorWithRed:209/255.f green:1/255.f blue:7/255.f alpha:1];
            break;
        case 5:
            color = [UIColor colorWithRed:151/255.f green:16/255.f blue:12/255.f alpha:1];
            break;
        default:
            break;
    }
    
    return color;
}


@end
