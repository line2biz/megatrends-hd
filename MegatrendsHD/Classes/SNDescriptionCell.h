//
//  SNDescriptionCell.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 17.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SNDescriptionCellDelegate;

@class DMKeyDesc;

@interface SNDescriptionCell : UITableViewCell

@property (strong, nonatomic) DMKeyDesc *keyDesc;
@property (strong, nonatomic) DMDiagram *diagram;

@property (weak, nonatomic) id<SNDescriptionCellDelegate> delegate;
@property (strong, nonatomic) UITapGestureRecognizer *tapRecognizer;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *placeHolderLabel;

@end

@protocol SNDescriptionCellDelegate <NSObject>

- (void)descriptionCell:(SNDescriptionCell *)descriptionCell didSelectObject:(id)object;

@end