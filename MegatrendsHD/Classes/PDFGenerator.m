//
//  PDFGenerator.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 20.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "PDFGenerator.h"
#import "SNAppearance.h"

#import <CoreText/CoreText.h>

#define PDF_WIDTH 595.f
#define PDF_HEIGHT 842.f

#define MARGIN_LEFT 40.f
#define MARGIN_RIGHT 20.f
#define MARGIN_BOTTOM 50.f
#define MARGIN_TOP 30.f
#define TREND_FRAME_WIDTH  25.f
#define TREND_FRAME_HEIGHT 25.f
#define COLUMN_MARGIN      20.f
#define TREND_MARGIN       5.f

@interface PDFGenerator()
{
    DMAnalyse *_analyze;
    NSUInteger _pageNumber;
}
@end

@implementation PDFGenerator


- (NSString *)generatePDFForAnalyze:(DMAnalyse *)analyze
{
    NSString *sName = [analyze title];
    while ([sName rangeOfString:@" "].location != NSNotFound) {
        sName = [sName stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    }
    
    sName = @"MyFutureAnalyze";
    
    _analyze = analyze;
    _fileName = sName;
    
   
    
    NSArray *arrayPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [arrayPaths objectAtIndex:0];
    _fileName = [[path stringByAppendingPathComponent:_fileName] stringByAppendingString:@".pdf"];
    
    //NSLog(@"%s %@", __FUNCTION__, _fileName);
    
    
    UIGraphicsBeginPDFContextToFile(_fileName, CGRectZero, nil);
    // Mark the beginning of a new page.
    
    _pageNumber = 1;
    
//    Draw First page
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
    [self drawLogo];
    [self drawPageNumber:_pageNumber];
    
    
//    Draw Second page
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
    
    _pageNumber = _pageNumber + 1;
    
    [self drawPageNumber:_pageNumber];
    CGFloat step = [self drawSecondPage];
    
    [self drawAnalysisFromStep:step];

    UIGraphicsEndPDFContext();
    
    return _fileName;
}

- (void)drawLogo
{
    CGFloat logoHeight = 80.f;
    
    UIColor *redColor = [UIColor colorWithRed:228/255.f green:30/255.f blue:10/255.f alpha:1.f];
    [self drawRect:CGRectMake(MARGIN_LEFT, 0, PDF_WIDTH - MARGIN_LEFT, logoHeight) color:redColor];
    
    UIImage *image = [UIImage imageNamed:@"Logo-PDF"];
    CGRect rectImage = CGRectMake(MARGIN_LEFT + 20, (logoHeight - image.size.height) / 2, image.size.width, image.size.height);
    [image drawInRect:rectImage];
    
    UIFont *theFont = [UIFont fontWithName:@"Avenir-Heavy" size:16];
    CGSize maxSize = CGSizeMake(PDF_WIDTH - MARGIN_LEFT - MARGIN_RIGHT, 72);
    
    [[UIColor whiteColor] set];
    NSString *sLogoTitle = [[LMLocalizedString(@"Megatrends HD", nil) uppercaseString] stringByAppendingFormat:@" | %@", [LMLocalizedString(@"Zukunftsanalyse", nil) capitalizedString]];
    CGSize sizeLogoTitle = [sLogoTitle sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect rectLogoTitle = CGRectMake(CGRectGetMaxX(rectImage) + 10, CGRectGetMidY(rectImage) - .5 - sizeLogoTitle.height / 2, sizeLogoTitle.width, sizeLogoTitle.height);
    [sLogoTitle drawInRect:rectLogoTitle withFont:theFont];
    
    
    [[SNAppearance colorVinous] set];
    
    
    NSString *pageTitle = [_analyze.title uppercaseString];
    
    CGSize titleSize = [pageTitle sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect titleRect = CGRectMake(MARGIN_LEFT, 100, titleSize.width, titleSize.height);
    [pageTitle drawInRect:titleRect withFont:theFont];
    
    
    [[UIColor blackColor] set];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[[SNLocalizationManager sharedManager] getLanguage]];
    NSString *format = [NSDateFormatter dateFormatFromTemplate:@"d MMM yyyy" options:0 locale:locale];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:format];
    NSString *dateString = [formatter stringFromDate:_analyze.date];
    CGSize dateSize = [dateString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect dateRect = CGRectMake(MARGIN_LEFT, CGRectGetMaxY(titleRect) + 5, dateSize.width, dateSize.height);
    [dateString drawInRect:dateRect withFont:theFont];
    
    
    CGFloat step = CGRectGetMaxY(dateRect) + 5;
    
    //-------
//    NSString *descString = _analyze.desc;
//    maxSize = CGSizeMake(PDF_WIDTH - MARGIN_LEFT - MARGIN_RIGHT, 500);
//    CGSize descSize = [descString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
//    CGRect descRect = CGRectMake(MARGIN_LEFT, CGRectGetMaxY(dateRect) + 5, descSize.width, descSize.height);
//    theFont = [UIFont fontWithName:@"Avenir-Medium" size:12];
//    [descString drawInRect:descRect withFont:theFont];


    UIFont *columnFont = [SNAppearance appFontWithSize:10];
    CGFloat columnWidth = (PDF_WIDTH - MARGIN_LEFT - MARGIN_RIGHT) / 2 -  COLUMN_MARGIN;
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:(_analyze.desc == nil) ? @"" : _analyze.desc];
    [attStr addAttribute:NSFontAttributeName value:columnFont range:NSMakeRange(0, attStr.length)];
    
    CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attStr);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectMake(0, 0, columnWidth, columnFont.lineHeight * 999999999));
    
    CTFrameRef frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, [attStr length]), path, NULL);
    
    CFArrayRef lines = CTFrameGetLines(frame);
    NSUInteger numberOfLines  = CFArrayGetCount(lines);
    
    
    CGRect lineRect = CGRectMake(MARGIN_LEFT, step, columnWidth, columnFont.lineHeight);
    
    NSUInteger linesInOneColumn =  MIN((int)ceil(numberOfLines / 2.f), 7);
    
    for (int idx = 0; idx < linesInOneColumn; idx++) {
        CTLineRef line = CFArrayGetValueAtIndex(lines, idx);
        CFRange range = CTLineGetStringRange(line);
        
        lineRect.origin.y = step + columnFont.lineHeight * idx;
        NSString *string = [attStr.string substringWithRange:NSMakeRange(range.location, range.length)];
        [string drawInRect:lineRect withFont:columnFont];
        
//        NSLog(@"%ld %ld", range.location, range.length);
//        CFArrayRef runs = CTLineGetGlyphRuns(line);
//        NSLog(@"runs count %ld", CFArrayGetCount(runs));
//        
//        if (CFArrayGetCount(runs) > 0) {
//            CTRunRef run = CFArrayGetValueAtIndex(runs, 0);
//            NSLog(@"%@", run);
//        }
        
        if (idx == linesInOneColumn) {
            lineRect.origin.x = MARGIN_LEFT + columnWidth + COLUMN_MARGIN;
            lineRect.origin.y = step;
        }
        
        
    }

    lineRect.origin.x = MARGIN_LEFT + columnWidth + COLUMN_MARGIN;
    lineRect.origin.y = step;

    for (NSInteger idx = linesInOneColumn; idx < numberOfLines; idx++) {
        
        if (idx >= linesInOneColumn * 2) {
            break;
        }
        
        CTLineRef line = CFArrayGetValueAtIndex(lines, idx);
        CFRange range = CTLineGetStringRange(line);
        
        lineRect.origin.y = step + columnFont.lineHeight * (idx - linesInOneColumn);
        NSString *string = [attStr.string substringWithRange:NSMakeRange(range.location, range.length)];
        [string drawInRect:lineRect withFont:columnFont];
        
    }

    

    
    image = [UIImage imageNamed:@"Diagram-PDF"];
    
    CGRect imageRect = CGRectMake(MARGIN_LEFT + 60, 280, image.size.width, image.size.height);
    [image drawInRect:imageRect];
    
    [self placeTrendsOnDiagramInRect:imageRect];
    
    
    NSString *chancenString = [LMLocalizedString(@"Chancen", nil) uppercaseString];
    maxSize = CGSizeMake(CGRectGetWidth(imageRect), 72);
    [[UIColor colorWithRed:125/255.f green:180/255.f blue:11/255.f alpha:1] set];
    CGSize chancenSize = [chancenString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect chancenRect = CGRectMake(CGRectGetMinX(imageRect), CGRectGetMinY(imageRect) - chancenSize.height, chancenSize.width, chancenSize.height);
    [chancenString drawInRect:chancenRect withFont:theFont];
    
    
    NSString *riskString = [LMLocalizedString(@"Risiken", nil) uppercaseString];
    [[SNAppearance colorVinous] set];
    CGSize riskSize = [riskString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect riskRect = CGRectMake(CGRectGetMaxX(imageRect) - riskSize.width, CGRectGetMaxY(imageRect), riskSize.width, riskSize.height);
    [riskString drawInRect:riskRect withFont:theFont];
    
    
    NSString *highString = [LMLocalizedString(@"hoch", nil) lowercaseString];
    [[UIColor blackColor] set];
    theFont = [UIFont fontWithName:@"Avenir-Medium" size:10];
    CGSize highSize = [highString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect highRect = CGRectMake(CGRectGetMinX(imageRect) - highSize.width - 5, CGRectGetMinY(imageRect) + CGRectGetHeight(imageRect) / 9 * 1.5 - highSize.height / 2, highSize.width, highSize.height);
    [highString drawInRect:highRect withFont:theFont];
    
    highRect = CGRectMake(CGRectGetMinX(imageRect) + CGRectGetWidth(imageRect) / 9 * 7.5 - highSize.width / 2, CGRectGetMaxY(imageRect), highSize.width, highSize.height);
    [highString drawInRect:highRect withFont:theFont];
    
    NSString *mediumString = [LMLocalizedString(@"mittel", nil) lowercaseString];
    [[UIColor blackColor] set];
    CGSize mediumSize = [mediumString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect mediumRect = CGRectMake(CGRectGetMinX(imageRect) - mediumSize.width - 5, CGRectGetMinY(imageRect) + CGRectGetHeight(imageRect) / 9 * 4.5 - mediumSize.height / 2, mediumSize.width, mediumSize.height);
    [mediumString drawInRect:mediumRect withFont:theFont];
    
    mediumRect = CGRectMake(CGRectGetMinX(imageRect) + CGRectGetWidth(imageRect) / 9 * 4.5 - mediumSize.width / 2, CGRectGetMaxY(imageRect), mediumSize.width, mediumSize.height);
    [mediumString drawInRect:mediumRect withFont:theFont];
    
    
    
    NSString *lowString = [LMLocalizedString(@"gering", nil) lowercaseString];
    [[UIColor blackColor] set];
    CGSize lowSize = [lowString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect lowRect = CGRectMake(CGRectGetMinX(imageRect) - lowSize.width - 5, CGRectGetMinY(imageRect) + CGRectGetHeight(imageRect) / 9 * 7.5 - lowSize.height / 2, lowSize.width, lowSize.height);
    [lowString drawInRect:lowRect withFont:theFont];
    
    lowRect = CGRectMake(CGRectGetMinX(imageRect) + CGRectGetWidth(imageRect) / 9 * 1.5 - lowSize.width / 2, CGRectGetMaxY(imageRect), lowSize.width, lowSize.height);
    [lowString drawInRect:lowRect withFont:theFont];
    
    CGRect smallRect = CGRectMake(0, 0, 20, 20);
    CGFloat smallMargin = 5;
    
    smallRect.origin.x = CGRectGetMaxX(imageRect) - CGRectGetWidth(smallRect);
    smallRect.origin.y = CGRectGetMaxY(imageRect) + 35;
    
    CGFloat leftCorner = 0;
    
    for (int idx = 5; idx > 0; idx--) {
        [self drawRect:smallRect color:[SNAppearance colorForHandleValue:idx]];
        leftCorner = smallRect.origin.x;
        smallRect.origin.x = CGRectGetMinX(smallRect) - smallMargin - CGRectGetWidth(smallRect);
    }
    
    theFont = [SNAppearance appFontWithSize:10];
    [[UIColor blackColor] set];
    NSString *sPress = [LMLocalizedString(@"Handlungsdruck", nil) uppercaseString];
    CGRect rectPress = CGRectMake(leftCorner, CGRectGetMinY(smallRect) - theFont.lineHeight, 200, theFont.lineHeight);
    [sPress drawInRect:rectPress withFont:theFont];
    
    NSString *sLow = LMLocalizedString(@"schwach", nil);
    CGRect rectLow = CGRectMake(leftCorner, CGRectGetMaxY(smallRect), 100, theFont.lineHeight);
    [sLow drawInRect:rectLow withFont:theFont];
    
    NSString *sStrong = LMLocalizedString(@"stark", nil);
    maxSize = CGSizeMake(99999999, theFont.lineHeight);
    CGSize sizeStrong = [sStrong sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect rectStrong = CGRectMake(CGRectGetMaxX(imageRect) - sizeStrong.width, CGRectGetMaxY(smallRect), sizeStrong.width, sizeStrong.height);
    [sStrong drawInRect:rectStrong withFont:theFont];
    
}

- (CGFloat)drawTitle:(NSString *)title number:(NSString *)number step:(CGFloat)step
{
    UIFont *theFont = [UIFont fontWithName:@"Avenir-Heavy" size:16];
    CGSize maxSize = CGSizeMake(24, 20);
    
    title = [title uppercaseString];
    
    CGSize numberSize = [number sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect numberRect = CGRectMake(MARGIN_LEFT, step, numberSize.width, numberSize.height);
    
    maxSize = CGSizeMake(PDF_WIDTH - CGRectGetMaxX(numberRect) - MARGIN_RIGHT - 10, 200);
    CGSize nameSize = [title sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect nameRect = CGRectMake(CGRectGetMaxX(numberRect) + 10, CGRectGetMinY(numberRect), nameSize.width, nameSize.height);
    
    if (CGRectGetMaxY(nameRect) > PDF_HEIGHT - MARGIN_BOTTOM || (PDF_HEIGHT - MARGIN_TOP) - CGRectGetMaxY(nameRect) < 60) {
        _pageNumber = _pageNumber + 1;
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
        [self drawPageNumber:_pageNumber];
        
        nameRect.origin.y = numberRect.origin.y = MARGIN_TOP;
    }
    
    [[SNAppearance colorVinous] set];
    [number drawInRect:numberRect withFont:theFont];
    [[UIColor blackColor] set];
    [title drawInRect:nameRect withFont:theFont];
    
    return CGRectGetMaxY(nameRect);
}

- (CGFloat)drawSecondPage
{
 
    CGFloat step = MARGIN_TOP;
    NSString *titleString = LMLocalizedString(@"Die ausgewählten Megatrends und Schlüsselfaktoren im Überblick", nil);
    
    
    UIFont *theFont = [UIFont fontWithName:@"Avenir-Heavy" size:16];
    CGSize maxSize = CGSizeMake(PDF_WIDTH - MARGIN_LEFT - MARGIN_RIGHT, 72);
    
    step = [self drawTitle:titleString number:@"I" step:step];

    NSArray *array = [self diagramsArray];
    
    step = step + 10;
    
    
    theFont = [UIFont fontWithName:@"Avenir-Medium" size:14];
    
    for (DMDiagram *diagram in array) {
        NSString *numberString  = [NSString stringWithFormat:@"%02d:", diagram.trend.tid + 1];
        
        
        maxSize = CGSizeMake(24, 20);
        CGSize numberSize = [numberString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
        CGRect numberRect = CGRectMake(MARGIN_LEFT, step, numberSize.width, numberSize.height);
        
        
        NSString *nameString = [diagram.trend.title uppercaseString];
        maxSize = CGSizeMake(PDF_WIDTH - CGRectGetMaxX(numberRect) - MARGIN_RIGHT - 10, 200);
        CGSize nameSize = [nameString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
        CGRect nameRect = CGRectMake(CGRectGetMaxX(numberRect) + 10, CGRectGetMinY(numberRect), nameSize.width, nameSize.height);
        
        if (CGRectGetMaxY(nameRect) > PDF_HEIGHT - MARGIN_BOTTOM) {
            _pageNumber = _pageNumber + 1;
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
            [self drawPageNumber:_pageNumber];
            
            nameRect.origin.y = numberRect.origin.y = MARGIN_TOP;
        }
        
        [[SNAppearance colorVinous] set];
        [numberString drawInRect:numberRect withFont:theFont];
        [[UIColor blackColor] set];
        [nameString drawInRect:nameRect withFont:theFont];
        
        step = CGRectGetMaxY(nameRect);
        
        NSArray *anotherArray = [diagram.keyDescs sortedArrayUsingDescriptors:[DMKeyDesc sortDescriptorsArray]];
        for (DMKeyDesc *keyDesc in anotherArray) {
            
            NSString *numStr  = [NSString stringWithFormat:@"%02d.%d", diagram.trend.tid + 1, keyDesc.keyFactor.kid + 1];
            
            maxSize = CGSizeMake(35, 72);
            CGSize numSize = [numStr sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
            CGRect numRect = CGRectMake(CGRectGetMinX(nameRect), step, numSize.width, numSize.height);
            
            NSString *subnameString = keyDesc.keyFactor.title;
            maxSize = CGSizeMake(PDF_WIDTH - CGRectGetMaxX(numRect) - MARGIN_RIGHT - 10, 200);
            CGSize subnameSize = [subnameString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
            CGRect subnameRect = CGRectMake(CGRectGetMaxX(numRect) + 10, CGRectGetMinY(numRect), subnameSize.width, subnameSize.height);
            
            if (CGRectGetMaxY(subnameRect) > PDF_HEIGHT - MARGIN_BOTTOM) {
                _pageNumber = _pageNumber + 1;
                UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
                [self drawPageNumber:_pageNumber];
                
                numRect.origin.y = subnameRect.origin.y = MARGIN_TOP;
            }
            
            [[SNAppearance colorVinous] set];
            [numStr drawInRect:numRect withFont:theFont];
            [[UIColor blackColor] set];
            [subnameString drawInRect:subnameRect withFont:theFont];
            
            step = CGRectGetMaxY(subnameRect);
        }
        
        step = step + 10;
    }

    return step;
}

- (void)drawPageNumber:(NSInteger)pageNum
{
    [[UIColor blackColor] set];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[[SNLocalizationManager sharedManager] getLanguage]];
    
    NSString *format = [NSDateFormatter dateFormatFromTemplate:@"d MMM yyyy" options:0 locale:locale];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:format];
    
    
    NSString *sPage = [NSString stringWithFormat:@"MEGATRENDS HD | %@ | %@ | ", LMLocalizedString(@"Zukunftsanalyse", nil), [formatter stringFromDate:_analyze.date]];
    
    
    NSString *sURL = @"www.z-punkt.de";
    NSString *sNumber = [NSString stringWithFormat:@"%ld", (long)pageNum];
    
    
    
    
    UIFont *theFont = [UIFont fontWithName:@"Avenir-Medium" size:10];
    CGSize maxSize = CGSizeMake(612, 72);
    
    CGSize sizeNumber = [sNumber sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect rectNumber = CGRectMake(PDF_WIDTH - MARGIN_RIGHT - sizeNumber.width, PDF_HEIGHT - (72.0 - sizeNumber.height) / 2.0, sizeNumber.width, sizeNumber.height);
    [sNumber drawInRect:rectNumber withFont:theFont];
                                   
    CGSize sizeURL = [sURL sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect rectURL = CGRectMake(CGRectGetMinX(rectNumber) - sizeURL.width - 10, CGRectGetMinY(rectNumber), sizeURL.width, sizeURL.height);
    [sURL drawInRect:rectURL withFont:theFont];
    UIGraphicsSetPDFContextURLForRect([NSURL URLWithString:@"http://www.z-punkt.de"], rectURL);
    
    CGSize sizePage = [sPage sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect rectPage = CGRectMake(CGRectGetMinX(rectURL) - sizePage.width, CGRectGetMinY(rectNumber), sizePage.width, sizePage.height);
    [sPage drawInRect:rectPage withFont:theFont];
    
    
//    CGSize pageStringSize = [sPage sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
//    CGRect stringRect = CGRectMake( PDF_WIDTH - MARGIN_RIGHT - pageStringSize.width,
//                                   PDF_HEIGHT - ((72.0 - pageStringSize.height) / 2.0),
//                                   pageStringSize.width,
//                                   pageStringSize.height);
//    
//    [sPage drawInRect:stringRect withFont:theFont];
    
    [self drawRect:CGRectMake(MARGIN_LEFT, CGRectGetMinY(rectNumber) - 5, PDF_WIDTH - MARGIN_RIGHT - MARGIN_LEFT, 1) color:[UIColor blackColor]];
    
//    UIImage *image = [UIImage imageNamed:@"Logo"];
//    [image drawInRect:CGRectMake(MARGIN_LEFT, CGRectGetMinY(stringRect) - image.size.height / 2, image.size.width, image.size.height)];
    
}

- (void)drawAnalysisFromStep:(CGFloat)step
{
    NSString *titleString = LMLocalizedString(@"Detailanalyse", nil);
    
    step = step + 20;
    
    UIFont *theFont = [UIFont fontWithName:@"Avenir-Heavy" size:16];
    CGSize maxSize = CGSizeMake(PDF_WIDTH - MARGIN_LEFT - MARGIN_RIGHT, 72);
    
    step = [self drawTitle:titleString number:@"II" step:step];

    NSArray *array = [self diagramsArray];
    step = step + 10;
    
    
    theFont = [SNAppearance appFontWithSize:14];
    UIFont *boldFond = [SNAppearance boldAppFontWithSize:14];
    
    CGFloat marginTop = 5;
    CGFloat marginLeft = 10;
    
    for (DMDiagram *diagram in array) {
        theFont = [SNAppearance appFontWithSize:14];
        NSString *sHeader = [LMLocalizedString(@"Megatrend", nil) uppercaseString];
        maxSize = CGSizeMake(PDF_WIDTH - MARGIN_RIGHT - MARGIN_LEFT - marginLeft * 2, 72);
        
        CGSize sizeHeader = [sHeader sizeWithFont:boldFond constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
        CGRect rectHeader = CGRectMake(MARGIN_LEFT + marginLeft, step + marginLeft, sizeHeader.width, sizeHeader.height);
//        rectHeader.size.width = rectHeader.size.width + 50;
        
        
        step = CGRectGetMaxY(rectHeader);
        
        NSString *numberString  = [NSString stringWithFormat:@"%02d:", diagram.trend.tid + 1];
        
        
        maxSize = CGSizeMake(24, 20);
        CGSize numberSize = [numberString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
        CGRect numberRect = CGRectMake(MARGIN_LEFT + marginLeft, step, numberSize.width, numberSize.height);
        
        
        NSString *nameString = [diagram.trend.title uppercaseString];
        maxSize = CGSizeMake(PDF_WIDTH - CGRectGetMaxX(numberRect) - MARGIN_RIGHT - 10, 200);
        CGSize nameSize = [nameString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
        CGRect nameRect = CGRectMake(CGRectGetMaxX(numberRect) + 10, CGRectGetMinY(numberRect), nameSize.width, nameSize.height);
        
        CGRect frameRect = CGRectMake(MARGIN_LEFT, CGRectGetMinY(rectHeader) - marginTop, PDF_WIDTH - MARGIN_LEFT - MARGIN_RIGHT, CGRectGetMaxY(nameRect) - CGRectGetMinY(rectHeader) + marginTop * 2);
        
        
        
        
        
        
        if (CGRectGetMaxY(frameRect) > PDF_HEIGHT - MARGIN_BOTTOM) {
            _pageNumber = _pageNumber + 1;
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
            [self drawPageNumber:_pageNumber];
            
            frameRect.origin.y = MARGIN_TOP;
            rectHeader.origin.y = MARGIN_TOP + marginTop;
            numberRect.origin.y = nameRect.origin.y = CGRectGetMaxY(rectHeader);
        }
        
        
        
        
        
        step = CGRectGetMaxY(nameRect) + 5;
        
        
        NSString *sDesc = diagram.desc;
        
        CGFloat width = 200;
        
        theFont = [SNAppearance appFontWithSize:12];
        
        maxSize = CGSizeMake(PDF_WIDTH - MARGIN_LEFT - MARGIN_RIGHT - width, PDF_HEIGHT - MARGIN_TOP - MARGIN_BOTTOM);
        CGSize sizeDesc = [sDesc sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByWordWrapping];
        CGRect rectDesc = CGRectMake(MARGIN_LEFT + width, step, sizeDesc.width, sizeDesc.height);
        
        
        CGFloat max = MAX(step + 72 + 3, CGRectGetMaxY(rectDesc));
        
        if (max > PDF_HEIGHT - MARGIN_BOTTOM) {
            _pageNumber = _pageNumber + 1;
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
            [self drawPageNumber:_pageNumber];
            step = MARGIN_TOP;
            frameRect.origin.y = step;
            rectHeader.origin.y = step + marginTop;
            numberRect.origin.y = nameRect.origin.y = CGRectGetMaxY(rectHeader);
            
            rectDesc.origin.y = CGRectGetMaxY(frameRect);
//            rectDesc.origin.y = MAX(CGRectGetMaxY(nameRect), CGRectGetMaxY(numberRect));
            step = CGRectGetMaxY(frameRect);
            
        }
        
        [self drawRect:frameRect color:[UIColor colorWithRed:236/255.f green:236/255.f blue:236/255.f alpha:1]];
        [[UIColor blackColor] set];
        
        [sHeader drawInRect:rectHeader withFont:boldFond];
        [[SNAppearance colorVinous] set];
        [numberString drawInRect:numberRect withFont:theFont];
        [[UIColor blackColor] set];
        [nameString drawInRect:nameRect withFont:theFont];
        
        [sDesc drawInRect:rectDesc withFont:theFont];
        step = [self showRateType:0 managedObject:diagram fromStep:step];
        step = [self showRateType:1 managedObject:diagram fromStep:step];
        step = [self showRateType:2 managedObject:diagram fromStep:step];
        step = MAX(step, CGRectGetMaxY(rectDesc)) + 10;
        
        NSArray *anotherArray = [self keyDescArrayForDiagram:diagram];
        
        
        
        for (NSUInteger idxKey = 0; idxKey < [anotherArray count]; idxKey++) {
            DMKeyDesc *keyDesc = anotherArray[idxKey];
            theFont = [SNAppearance appFontWithSize:14];
            NSString *numberString  = [NSString stringWithFormat:@"%02d.%d", diagram.trend.tid + 1, keyDesc.keyFactor.kid + 1];
            
            
            maxSize = CGSizeMake(34, 20);
            CGSize numberSize = [numberString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
            CGRect numberRect = CGRectMake(MARGIN_LEFT + marginLeft, step, numberSize.width, numberSize.height);
            
            
            NSString *nameString = keyDesc.keyFactor.title;
            maxSize = CGSizeMake(PDF_WIDTH - CGRectGetMaxX(numberRect) - MARGIN_RIGHT - 10, 200);
            CGSize nameSize = [nameString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
            CGRect nameRect = CGRectMake(CGRectGetMaxX(numberRect) + 10, CGRectGetMinY(numberRect), nameSize.width, nameSize.height);
            
            if (CGRectGetMaxY(frameRect) > PDF_HEIGHT - MARGIN_BOTTOM) {
                _pageNumber = _pageNumber + 1;
                UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
                [self drawPageNumber:_pageNumber];
                numberRect.origin.y = nameRect.origin.y = CGRectGetMaxY(rectHeader);
            }
            
            
            
            step = CGRectGetMaxY(nameRect) + 5 + 3;
            
            
            NSString *sDesc = keyDesc.desc; // keyDesc.desc; // keyDesc.keyFactor.teaser;
            theFont = [SNAppearance appFontWithSize:12];
            
            maxSize = CGSizeMake(PDF_WIDTH - MARGIN_LEFT - MARGIN_RIGHT - width, PDF_HEIGHT - MARGIN_TOP - MARGIN_BOTTOM);
            CGSize sizeDesc = [sDesc sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByWordWrapping];
            CGRect rectDesc = CGRectMake(MARGIN_LEFT + width, step, sizeDesc.width, sizeDesc.height);
            
            
            CGFloat max = MAX(step + 72 + 3, CGRectGetMaxY(rectDesc));
            
            if (max > PDF_HEIGHT - MARGIN_BOTTOM) {
                _pageNumber = _pageNumber + 1;
                UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
                [self drawPageNumber:_pageNumber];
                step = MARGIN_TOP + 3;
                numberRect.origin.y = nameRect.origin.y = step;
                rectDesc.origin.y = MAX(CGRectGetMaxY(numberRect), CGRectGetMaxY(nameRect)) + 5;
                step = CGRectGetMinY(rectDesc);
            }
            
            [self drawDottedLine:CGRectGetMinY(numberRect) - 3];
            
            [[SNAppearance colorVinous] set];
            [numberString drawInRect:numberRect withFont:theFont];
            [[UIColor blackColor] set];
            [nameString drawInRect:nameRect withFont:theFont];
            
            [sDesc drawInRect:rectDesc withFont:theFont];
            
            
            step = [self showRateType:0 managedObject:keyDesc fromStep:step];
            step = [self showRateType:1 managedObject:keyDesc fromStep:step];
            step = [self showRateType:2 managedObject:keyDesc fromStep:step];
            step = MAX(step, CGRectGetMaxY(rectDesc)) + 10;
            
            
        }
        
    }
}

- (CGFloat)showRateType:(int)type managedObject:(NSManagedObject *)managedObject fromStep:(CGFloat)step
{
    NSString *sChanse = nil;
    NSInteger positionX;
    NSInteger maxCount = 10;
    
    switch (type) {
        case 0:
            sChanse = LMLocalizedString(@"Chancen", nil);
            if ([managedObject isKindOfClass:[DMDiagram class]]) {
                positionX = [(DMDiagram *)managedObject positionY];
            } else {
                positionX = [(DMKeyDesc *)managedObject positionY];
            }
            break;
        case 1:
            sChanse = LMLocalizedString(@"Risiken", nil);
            if ([managedObject isKindOfClass:[DMDiagram class]]) {
                positionX = [(DMDiagram *)managedObject positionX];
            } else {
                positionX = [(DMKeyDesc *)managedObject positionX];
            }
            
            break;
        default:
            maxCount = 6;
            if ([managedObject isKindOfClass:[DMDiagram class]]) {
                positionX = [(DMDiagram *)managedObject color];
            } else {
                positionX = [(DMKeyDesc *)managedObject type];
            }
            sChanse = LMLocalizedString(@"Handlungsdruck", nil);
            break;
    }
    
    
    
    UIFont *theFont = [SNAppearance appFontWithSize:10];
    CGSize maxSize = CGSizeMake(PDF_WIDTH - MARGIN_LEFT - MARGIN_RIGHT, 72);
    
    CGSize sizeChanse = [sChanse sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
    CGRect rectChance = CGRectMake(MARGIN_LEFT + 10, step, sizeChanse.width, sizeChanse.height);
    [[UIColor blackColor] set];
    [sChanse drawInRect:rectChance withFont:theFont];
    
    step = CGRectGetMaxY(rectChance);
    
    
    
    CGFloat stepX = MARGIN_LEFT;
    CGFloat maxStep = 0;
    for (int idx = 1;  idx < maxCount; idx++) {
        CGRect rect = CGRectMake(stepX, step, 10, 10);
        
        UIColor *color = [UIColor lightGrayColor];
        if (type == 0) {
            color = (positionX >= idx) ? [SNAppearance colorChance] : [SNAppearance colorGray];;
        } else if (type == 1) {
            color = (positionX >= idx) ? [SNAppearance colorRisk] : [SNAppearance colorGray];
        } else {
            color = (positionX >= idx) ? [SNAppearance colorForHandleValue:idx] : [SNAppearance colorGray];
        }
        
        stepX = CGRectGetMaxX(rect) + 5;
        [self drawRect:rect color:color];
        
        maxStep = CGRectGetMaxY(rect);
    }
    
    
    return maxStep;
}


- (void)placeTrendsOnDiagramInRect:(CGRect)rect
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMDiagram class])];
    fetchRequest.sortDescriptors = [DMDiagram sortDescriptorsArray];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"analyse == %@ && positionX >= 0 && positionY >= 0", _analyze];
    NSArray *results = [[[DMManager sharedManager] managedObjectContext] executeFetchRequest:fetchRequest error:NULL];
    
    UIFont *font = [SNAppearance boldAppFontWithSize:12];
    CGSize maxSize = CGSizeMake(TREND_FRAME_WIDTH, TREND_FRAME_HEIGHT);
    
    CGFloat height = rect.size.height / 9;
    CGFloat width = rect.size.width / 9;
    
    for (NSInteger idx = [results count] - 1; idx >= 0; idx--) {
        DMDiagram *diagram = results[idx];
        CGRect frame = CGRectMake(0, 0, TREND_FRAME_WIDTH, TREND_FRAME_HEIGHT);
        
        NSInteger corner = diagram.corner;
        
        UIImage *imageCrossMark = [UIImage imageNamed:@"Cross-Mark"];
        CGRect imageRect = CGRectMake(0, 0, imageCrossMark.size.width, imageCrossMark.size.height);
        
        
        
        
//        NSLog(@"%s corner: %@", __FUNCTION__, sConrner);
        
        frame.origin.x = CGRectGetMinX(rect) + width * diagram.positionX;
        
        if (corner == SNCornerLeftTop || corner == SNCornerLeftBottom) {
            frame.origin.x = frame.origin.x - TREND_FRAME_WIDTH - TREND_MARGIN;
            imageRect.origin.x = CGRectGetMinX(rect) + width * (diagram.positionX)  - CGRectGetWidth(imageRect) / 2;
        } else {
            frame.origin.x = frame.origin.x + TREND_MARGIN;
            imageRect.origin.x = CGRectGetMinX(rect) + width * (diagram.positionX) - CGRectGetWidth(imageRect) / 2;
        }
        
        frame.origin.y = CGRectGetMaxY(rect) - height * diagram.positionY;
        
        if (corner == SNCornerRightBottom || corner == SNCornerLeftBottom) {
            frame.origin.y = frame.origin.y + TREND_MARGIN;
            imageRect.origin.y = CGRectGetMaxY(rect) - height * (diagram.positionY) - CGRectGetHeight(imageRect) / 2;
            
        } else {
            frame.origin.y = frame.origin.y - TREND_FRAME_HEIGHT - TREND_MARGIN;
            imageRect.origin.y = CGRectGetMaxY(rect) - height * (diagram.positionY) - CGRectGetHeight(imageRect) / 2;
        }
        
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"positionX == %lld && positionY == %lld && corner = %d", diagram.positionX, diagram.positionY, diagram.corner];
        
        NSArray *array = [results filteredArrayUsingPredicate:predicate];
        if ([array count] > 1) {
            
            UIFont *font = [SNAppearance appFontWithSize:10];
            UIColor *color = [SNAppearance colorForHandleValue:diagram.color];
            UIColor *fontColor = diagram.color == 0 ? [UIColor blackColor] : [UIColor whiteColor];
            
            NSString *sNum = [NSString stringWithFormat:@"%02d", diagram.trend.tid + 1];
            CGSize maxSize = CGSizeMake(font.lineHeight, font.lineHeight);
            CGSize numSize = [sNum sizeWithFont:font constrainedToSize:maxSize lineBreakMode:NSLineBreakByClipping];
            CGRect rectNum = CGRectMake(0, 0, numSize.width, numSize.height);
            
            CGFloat margin = 2.f;
            
            NSUInteger idx = [array indexOfObject:diagram];
            if (idx % 2) { // odd
//                bezierPath add
                
                
                CGContextRef ctx = UIGraphicsGetCurrentContext();
                
                CGContextBeginPath(ctx);
                CGContextMoveToPoint   (ctx, CGRectGetMinX(frame), CGRectGetMinY(frame));  // top left
                CGContextAddLineToPoint(ctx, CGRectGetMinX(frame), CGRectGetMaxY(frame));  // mid right
                CGContextAddLineToPoint(ctx, CGRectGetMaxX(frame), CGRectGetMinY(frame));  // bottom left
                CGContextClosePath(ctx);
                
                rectNum.origin.x = frame.origin.x + margin;
                rectNum.origin.y = frame.origin.y;
                
                CGContextSetFillColorWithColor(ctx, [color CGColor]);
                CGContextFillPath(ctx);
                
                
            } else { // even
                
                CGContextRef ctx = UIGraphicsGetCurrentContext();
                
                CGContextBeginPath(ctx);
                CGContextMoveToPoint   (ctx, CGRectGetMinX(frame), CGRectGetMaxY(frame));  // top left
                CGContextAddLineToPoint(ctx, CGRectGetMaxX(frame), CGRectGetMinY(frame));  // mid right
                CGContextAddLineToPoint(ctx, CGRectGetMaxX(frame), CGRectGetMaxY(frame));  // bottom left
                CGContextClosePath(ctx);
                
                CGContextSetFillColorWithColor(ctx, [color CGColor]);
                CGContextFillPath(ctx);
                
                
                rectNum.origin.x = CGRectGetMaxX(frame) - CGRectGetWidth(rectNum) - margin;
                rectNum.origin.y = CGRectGetMaxY(frame) - CGRectGetHeight(rectNum);
            }
            
            
            [fontColor set];
            
            [sNum drawInRect:rectNum withFont:font];
            
            
        } else {
            [self drawRect:frame color:[SNAppearance colorForHandleValue:diagram.color]];
            NSString *sNumber = [NSString stringWithFormat:@"%02d", diagram.trend.tid + 1];
            CGSize numberSize = [sNumber sizeWithFont:font constrainedToSize:maxSize];
            CGRect numberRect = CGRectMake(CGRectGetMinX(frame) + (TREND_FRAME_WIDTH - numberSize.width) / 2 ,
                                           CGRectGetMinY(frame) + (TREND_FRAME_HEIGHT - numberSize.height) / 2,
                                           numberSize.width,
                                           numberSize.height);
            
            
            UIColor *color = diagram.color > 0 ? [UIColor whiteColor] : [UIColor blackColor];
            [color set];
            
            [sNumber drawInRect:numberRect withFont:font];
        }
        
        
        
        
        
        
        
        [imageCrossMark drawInRect:imageRect];

        
    }
    
}

- (void)drawRect:(CGRect)rect color:(UIColor *)color
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextSetBlendMode(context, kCGBlendModeLighten);
    CGColorRef redColor = color.CGColor;
    
    CGContextSetFillColorWithColor(context, redColor);
    CGContextFillRect(context, rect);
    CGContextRestoreGState(context);
}

#pragma mark 

- (NSArray *)diagramsArray
{
    NSArray *returnValue = nil;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMDiagram class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"positionX >= 0 && positionY >= 0 && analyse == %@", _analyze];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:DMDiagramSortOrderKey ascending:DMDiagramSortOrderAscendingValue];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    
    NSArray *results = [[[DMManager sharedManager] managedObjectContext] executeFetchRequest:fetchRequest error:NULL];
    if ([results count]) {
        returnValue = results;
    }
    
    
    return returnValue;
}

- (NSArray *)keyDescArrayForDiagram:(DMDiagram *)diagram
{
    NSArray *returnValue = nil;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMKeyDesc class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(positionX > 0 || positionY > 0) && diagram == %@", diagram];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:DMKeyDescOrderKey ascending:DMKeyDescAscendingValue];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    
    NSArray *results = [[[DMManager sharedManager] managedObjectContext] executeFetchRequest:fetchRequest error:NULL];
    if ([results count]) {
        returnValue = results;
    }
    
    
    return returnValue;
}

- (CGFloat)drawDottedLine:(CGFloat)step
{
    
    
    //Long-Dotted-Line@2x
    
    UIImage *image = [UIImage imageNamed:@"Long-Dotted-Line"];
    CGRect rect = CGRectMake(MARGIN_LEFT, step, PDF_WIDTH -  MARGIN_RIGHT - MARGIN_LEFT, 2);
    [image drawInRect:rect];
    
    return step + CGRectGetHeight(rect);
    
    
//    CGContextRef currentContext = UIGraphicsGetCurrentContext();
//    
//    CGContextSetStrokeColorWithColor(currentContext, [UIColor blackColor].CGColor);
//    CGFloat dotRadius = 2;
//    CGFloat lengths[] = {0, dotRadius * 2};
//    CGContextSetLineCap(currentContext, kCGLineCapRound);
//    CGContextSetLineWidth(currentContext, dotRadius);
//    CGContextSetLineDash(currentContext, 0.0f, lengths, 2);
//    
//    CGContextBeginPath(currentContext);
//    CGContextMoveToPoint(currentContext, MARGIN_LEFT, step);
//    CGContextAddLineToPoint(currentContext, PDF_WIDTH -  MARGIN_RIGHT, step);
//    
//    CGContextClosePath(currentContext);
//    CGContextDrawPath(currentContext, kCGPathStroke);
//    
//    step = step + dotRadius * 2;
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
