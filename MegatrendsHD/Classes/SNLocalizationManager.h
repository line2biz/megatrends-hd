//
//  SNLocalizationManager.h
//  MegaTrends
//
//  Created by Александр Жовтый on 19.04.13.
//  Copyright (c) 2013 Line2Biz. All rights reserved.
//

#import <Foundation/Foundation.h>

#undef SNLocalizedString
#define SNLocalizedString(key, comment) \
[[SNLocalizationManager sharedManager] localizedStringForKey:(key) value:(comment)]

#define LMLocalizedString(key, comment) \
[[SNLocalizationManager sharedManager] localizedStringForKey:(key) value:(comment)]

FOUNDATION_EXPORT NSString *const SNLocalizationManagerDidChangeLanguage;

@interface SNLocalizationManager : NSObject

+ (SNLocalizationManager *)sharedManager;


- (NSBundle *)bundle;
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment;
- (void)setLanguage:(NSString*)langIdentifier;
- (NSString*)getLanguage;
- (void)resetLocalization;

@end
