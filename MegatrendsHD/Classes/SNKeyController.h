//
//  SNKeyController.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 24.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMKeyFactor;

@interface SNKeyController : UIViewController

@property (strong,  nonatomic) DMKeyFactor *keyFactor;

- (void)configureView;

@end
