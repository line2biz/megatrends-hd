//
//  DMManager.h
//  MSDCatalog
//
//  Created by Александр Жовтый on 09.02.13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DMTrend+Category.h"
#import "DMKeyFactor+Category.h"
#import "DMAnalyse+Category.h"
#import "DMDiagram+Category.h"
#import "DMKeyDesc+Category.h"

FOUNDATION_EXPORT NSString *const DMManagerModelNameValue;

@interface DMManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext * managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel * managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator * persistentStoreCoordinator;

+ (DMManager *)sharedManager;
+ (NSManagedObjectContext *)managedObjectContext;

- (void)saveContext;

- (void)serializeData:(NSArray *)array;

@end
