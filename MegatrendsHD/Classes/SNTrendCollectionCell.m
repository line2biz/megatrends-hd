//
//  SNTrenCollectionView.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 26.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNTrendCollectionCell.h"
#import "DMManager.h"

@interface SNTrendCollectionCell()
{
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet NSLayoutConstraint *_titleWidthConstraint;
    __weak IBOutlet NSLayoutConstraint *_titleHeightConstrait;
}
@end

@implementation SNTrendCollectionCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.numberLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Trend-Number-Bkg"]];
}

- (void)setTrend:(DMTrend *)trend
{
    _trend = trend;
    _titleLabel.text = [trend title];
    
    [self layoutSubviews];
    
}

- (void)layoutSubviews
{
    CGSize size = CGSizeMake(_titleWidthConstraint.constant, 60);
    
    if ([[self.trend title] length] > 0) {
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            NSString *text = [self.trend title];
            UIFont *font = _titleLabel.font;
            NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{ NSFontAttributeName: font }];
            CGRect rect = [attributedText boundingRectWithSize:(CGSize){CGRectGetWidth(_titleLabel.frame), _titleLabel.font.lineHeight * _titleLabel.numberOfLines} options:NSStringDrawingUsesLineFragmentOrigin context:nil];
            CGSize finalSize = rect.size;
            _titleHeightConstrait.constant = finalSize.height + 5;
            
        } else {
            size = [_titleLabel.text sizeWithFont:_titleLabel.font constrainedToSize:size lineBreakMode:_titleLabel.lineBreakMode];
            _titleHeightConstrait.constant = size.height;
        }
        
    }
    
    
}

@end
