//
//  SNSettingController.m
//  MegatrendsHD
//
//  Created by ios on 5/13/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNSettingController.h"
#import "SNLocalizationManager.h"
#import "SNAppearance.h"
#import "SNAppDelegate.h"

@interface SNSettingController ()
{
    __weak IBOutlet UIView *_containerView;
    
    __weak IBOutlet UILabel *_labelLanguage;
    __weak IBOutlet UISwitch *_switchLanguage;
    
    __weak IBOutlet UISegmentedControl *_segmentedLanguage;
    __weak IBOutlet UILabel *_labelNewsletter;
    __weak IBOutlet UISwitch *_switchNewsletter;
    
}
@end

@implementation SNSettingController

#pragma mark - View Life Cycle

- (void)configureView
{
    _labelLanguage.font = [SNAppearance appFontWithSize:_labelLanguage.font.pointSize];
    _labelNewsletter.font = [SNAppearance appFontWithSize:_labelNewsletter.font.pointSize];
    _labelLanguage.text = [LMLocalizedString(@"Sprache", nil) uppercaseString];
    _labelNewsletter.text = [LMLocalizedString(@"NEWSLETTER Push-Mitteilung", nil) uppercaseString];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _switchNewsletter.on = ([[NSUserDefaults standardUserDefaults] integerForKey:SNAppNotificationTypeKey] != UIRemoteNotificationTypeNone);
    
    [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self configureView];
    }];
    
    NSString *lang = [[SNLocalizationManager sharedManager] getLanguage];

//    [_switchLanguage setOn:[lang isEqualToString:@"de"]];
//    [_switchLanguage setOnImage:[UIImage imageNamed:@"langswitch_de.png"]];
//    [_switchLanguage setOffImage:[UIImage imageNamed:@"langswitch_eng.png"]];
    
    [_segmentedLanguage setSelectedSegmentIndex:[lang isEqualToString:@"en"]];
    [self configureView];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Rotations
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
*/

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Outlet Methods
- (IBAction)valueChangedSwitchLanguage:(UISegmentedControl *)sender
{
    NSString *lang = sender.selectedSegmentIndex == 0 ? @"de" : @"en";
    [[SNLocalizationManager sharedManager] setLanguage:lang];
    
    
    NSString *pushToken = [[NSUserDefaults standardUserDefaults] valueForKey:SNAppDeviceTokenKey];
    
    NSString *sURI = [SNAppNoticationDomainURIValue stringByAppendingFormat:@"%@&lang_code=%@", pushToken, [[SNLocalizationManager sharedManager] getLanguage]];
    NSURL *url = [NSURL URLWithString:sURI];
    NSURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    //NSLog(@"%@", sURI);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURLResponse *response = nil;
        NSError *error = nil;
        [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//        NSString *strin = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        //NSLog(@"%s %@", __FUNCTION__, strin);
    });
    
    
}

- (IBAction)valueChangedSwtichNewsletter:(UISwitch *)sender
{
    if (sender.on) {
        [[NSUserDefaults standardUserDefaults] setInteger:7 forKey:SNAppNotificationTypeKey];
        
    } else {
        [[NSUserDefaults standardUserDefaults] setInteger:UIRemoteNotificationTypeNone forKey:SNAppNotificationTypeKey];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:[[NSUserDefaults standardUserDefaults] integerForKey:SNAppNotificationTypeKey]];
}

- (IBAction)handleTap:(UITapGestureRecognizer *)sender
{
    CGPoint location = [sender locationInView:self.view];
    if (!CGRectContainsPoint(_containerView.frame, location)) {
        [self dismissViewControllerAnimated:NO completion:^{
            _senderButton.selected = NO;
        }];
    }
    
}
@end
