//
//  SNTrendsPageController.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 27.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMTrend;

@interface SNTrendsPageController : UIViewController

@property (strong, nonatomic) DMTrend *trend;

//- (void)showTrend:(DMTrend *)trend;

@end
