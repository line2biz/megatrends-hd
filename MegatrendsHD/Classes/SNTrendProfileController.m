//
//  SNTrendController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 27.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNAppDelegate.h"
#import "SNTrendProfileController.h"
#import "SNNavigationControllerProtocol.h"
#import "DMKeyFactor.h"
#import "DMKeyFactor+Category.h"
#import "SNKeyFactorCell.h"
#import "SNChartController.h"
#import "SNNavigationController.h"
#import "SNAppearance.h"


#import <QuartzCore/QuartzCore.h>

@interface SNTrendProfileController () <SNNavigationControllerProtocol, UITableViewDelegate>
{
    __weak IBOutlet UIImageView *_imageView;
    __weak IBOutlet UITextView *_textView;
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet NSLayoutConstraint *_menuKeyItemsContstraint;
    __weak IBOutlet UIView *_menuView;
    __weak IBOutlet UILabel *_menuTitleLabel;
    
    id _observer;
}

@end

@implementation SNTrendProfileController

- (void)configureView
{
    _imageView.image = [UIImage imageNamed:_trend.imageName];
    _textView.font = [SNAppearance appFontWithSize:_textView.font.pointSize];
    _textView.text = _trend.desc;
    _menuTitleLabel.text = [LMLocalizedString(@"Schlüsselaspekte", nil) uppercaseString];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _textView.editable = NO;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Profile-Container-Bkg"]];
    
    self.managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    
    _menuView.layer.masksToBounds = NO;
    _menuView.layer.shadowOffset = CGSizeMake(0, 0);
    _menuView.layer.shadowRadius = 10;
    _menuView.layer.shadowOpacity = 0.5;
    
    _menuKeyItemsContstraint.constant = 1024 + _menuView.layer.shadowRadius;
    
    __weak SNTrendProfileController *weekSelf = self;
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        SNTrendProfileController *strongSelf = weekSelf;
        if (strongSelf) {
            [strongSelf configureView];
            [_tableView reloadData];
        }
    }];
    /*
    [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        //[[self.fetchedResultsController fetchedObjects]  ] ;
    }];
    */

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self configureView];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self setKeysMenuHidden:YES animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}

- (void)setTrend:(DMTrend *)trend
{
    _trend = trend;
    _imageView.image = [UIImage imageNamed:_trend.imageName];

}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Controller Methods
- (void)setKeysMenuHidden:(BOOL)hidden animated:(BOOL)animated
{
    CGFloat alpha;
    if (hidden) {
        alpha = 0;
        _menuKeyItemsContstraint.constant = CGRectGetWidth(self.view.frame) + _menuView.layer.shadowRadius;;
    } else {
        alpha = 1;
        _menuKeyItemsContstraint.constant = CGRectGetWidth(self.view.frame) - CGRectGetWidth(_menuView.frame);
    }
    
    if (animated) {
        _menuView.alpha = alpha;
        [UIView animateWithDuration:.2f animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            _menuView.alpha = alpha;
        }];
    } else {
        _menuView.alpha = alpha;
    }
    
}

#pragma mark - Segueways
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[SNChartController class]]) {
        
        NSIndexPath *indexPath = [_tableView indexPathForSelectedRow];
        [[segue destinationViewController] setTrend:self.trend];
        [[segue destinationViewController] setKeyFactor:[self.fetchedResultsController objectAtIndexPath:indexPath]];
    }
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonShowMenu:(id)sender
{
    [self setKeysMenuHidden:NO animated:YES];

}

- (IBAction)didTapButtonHideMenu:(id)sender
{
//    _menuKeyItemsContstraint.constant = CGRectGetWidth(self.view.frame) + _menuView.layer.shadowRadius;;
    [self setKeysMenuHidden:YES animated:YES];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (void)configureCell:(SNKeyFactorCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    DMKeyFactor *keyFactor = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.lblNumber.text = [NSString stringWithFormat:@"%02d.%ld", (_trend.tid+1), (indexPath.row + 1)];
    cell.lblTitle.text = keyFactor.title;
    
    cell.lblNumber.text = [NSString stringWithFormat:@"%02d.%d", _trend.tid+1, keyFactor.kid + 1];
    cell.lblTitle.text = keyFactor.title;
 
}


- (SNKeyFactorCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SNKeyFactorCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMKeyFactor class])];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"trend = %@", self.trend];
    [fetchRequest setPredicate:predicate];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:DMKeyFactorOrderKey ascending:DMKeyFactorAscendingValue];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    _fetchedResultsController.delegate = self;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    return _fetchedResultsController;
}

#pragma mark - Fetched Resulrs Controller Delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [_tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [_tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = _tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(SNKeyFactorCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView endUpdates];
}

#pragma mark - Container Protocol
- (NSString *)titleForNavigationBar
{
    return [_trend.title uppercaseString];
}

- (NSString *)numberTitleForNavigationBar
{
    return [NSString stringWithFormat:@"%02d:", _trend.tid + 1];
}

@end
