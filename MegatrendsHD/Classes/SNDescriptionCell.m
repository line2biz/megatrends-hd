//
//  SNDescriptionCell.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 17.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNDescriptionCell.h"
#import "SNRateView.h"
#import "SNRateController.h"
#import "SNAppearance.h"
#import "SNAlertController.h"

@interface SNDescriptionCell()
{
    __weak IBOutlet UILabel *_numberLabel;
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet NSLayoutConstraint *_leadingSpaceConstraint;
    __weak IBOutlet NSLayoutConstraint *_widthConstraint;
    __weak IBOutlet NSLayoutConstraint *_titleLabelHeight;
    
    __weak IBOutlet SNRateView *_ratePossibilityView;
    __weak IBOutlet SNRateView *_rateRiskView;
    __weak IBOutlet SNRateView *_rateHadleView;
    
    __weak IBOutlet UIView *_dottedView;
    
    __weak IBOutlet UIButton *_infoButton;
    
}



@end;


@implementation SNDescriptionCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _ratePossibilityView.rateType = SNRateViewTypeChance;
    _rateRiskView.rateType = SNRateViewTypeRisk;
    _rateHadleView.rateType = SNRateViewTypeHandle;
    _textView.userInteractionEnabled = NO;
    _dottedView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Dotted-Line"]];
}

- (void)setHighlighted:(BOOL)highlighted
{
    self.backgroundColor = [UIColor clearColor];
    
//    if (highlighted) {
//        self.backgroundColor = [UIColor clearColor];
//    } else {
//        self.backgroundColor = [UIColor colorWithRed:210/255.f green:210/255.f blue:210/255.f alpha:1];
//    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [self setHighlighted:highlighted];
}

- (void)setSelected:(BOOL)selected
{
    [self setHighlighted:selected];
    if (selected) {
        _textView.userInteractionEnabled = YES;
        _textView.editable = YES;
        _textView.scrollEnabled = YES;
        _textView.showsVerticalScrollIndicator = YES;
        [self addGestureRecognizer:self.tapRecognizer];
    } else {
        _textView.userInteractionEnabled = NO;
        _textView.editable = NO;
        [_textView scrollRectToVisible:CGRectZero animated:YES];
        _textView.scrollEnabled = NO;
        _textView.showsVerticalScrollIndicator = NO;
        [self removeGestureRecognizer:self.tapRecognizer];
        self.tapRecognizer = nil;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [self setSelected:selected];
    
}

- (UITapGestureRecognizer *)tapRecognizer
{
    if (_tapRecognizer ==  nil) {
        _tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hadleTap:)];
        _tapRecognizer.numberOfTapsRequired = 1;
    }
    
    return _tapRecognizer;
}

- (void)hadleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    CGRect rect = CGRectMake(CGRectGetMinX(_ratePossibilityView.frame), CGRectGetMinY(_ratePossibilityView.frame),
                             CGRectGetWidth(_ratePossibilityView.frame), CGRectGetMaxY(_rateHadleView.frame) - CGRectGetMinY(_ratePossibilityView.frame));
    CGPoint location = [gestureRecognizer locationInView:self.contentView];
    if (CGRectContainsPoint(rect, location)) {
        
        id object = (self.keyDesc == nil) ? self.diagram : self.keyDesc;
        
        if ([object isKindOfClass:[DMDiagram class]]) {
            DMDiagram *diagram = (DMDiagram *)object;
            if ([diagram.keyDescs count] > 0) {
                NSString *title = [LMLocalizedString(@"Achtung!" , nil) uppercaseString];
                NSString *msg = LMLocalizedString(@"Keine Änderung möglich. Bewerten Sie bitte die aktivierten Schlüsselaspekte.", nil);
                
                NSString *sYES = [LMLocalizedString(@"OK", nil) uppercaseString];
                
                
                [SNAlertController showAlertWithTitle:title message:msg okButtonTitle:sYES cancelButtonTitle:nil completionHandler:NULL];
                return;
            }
            
        }
        
        [SNRateController showRateWithManagedObject:object];
    }
}

- (void)configureView
{
    CGSize maxSize = CGSizeMake(391-2, 500);
    maxSize = [[self.keyDesc.keyFactor title] sizeWithFont:[SNAppearance appFontWithSize:14] constrainedToSize:maxSize lineBreakMode:NSLineBreakByCharWrapping];
    //NSLog (@"%f",maxSize.height); // MAX(41, maxSize.height);
    
    
    if (self.diagram == nil) {
        _leadingSpaceConstraint.constant = 37;
        if (maxSize.height > 25) {
            _titleLabelHeight.constant = 43;
            _numberLabel.text = [NSString stringWithFormat:@"%02d.%d%@", self.keyDesc.keyFactor.trend.tid + 1, self.keyDesc.keyFactor.kid + 1,@"\n "];
            } else {
            _titleLabelHeight.constant = 21;
            _numberLabel.text = [NSString stringWithFormat:@"%02d.%d", self.keyDesc.keyFactor.trend.tid + 1, self.keyDesc.keyFactor.kid + 1];
            }
        
        static const unichar softHypen = 0x00AD; 
        NSString *titleText = [self.keyDesc.keyFactor title];
        NSString *strNew = [titleText stringByReplacingOccurrencesOfString:[NSString stringWithCharacters: &softHypen length: 1] withString:@""];
        
        _titleLabel.text = strNew;
        _widthConstraint.constant = 35;
        
        
        
        _ratePossibilityView.rateValue = self.keyDesc.positionY;
        _rateRiskView.rateValue = self.keyDesc.positionX;
        _rateHadleView.rateValue = self.keyDesc.type;
        
        _textView.text = self.keyDesc.desc;
        
        _placeHolderLabel.hidden = [self.keyDesc.desc length] > 0;
        
    } else {
        _leadingSpaceConstraint.constant = 15;
        _numberLabel.text = [NSString stringWithFormat:@"%02d:", self.diagram.trend.tid + 1];
        _titleLabel.text = [[self.diagram.trend title] uppercaseString];
        _widthConstraint.constant = 24;
        
        _textView.text = self.diagram.desc;
        
        _ratePossibilityView.rateValue = self.diagram.positionY;
        _rateRiskView.rateValue = self.diagram.positionX;
        _rateHadleView.rateValue = self.diagram.color;
        
        _placeHolderLabel.hidden = [self.diagram.desc length] > 0;

    }
    
     _placeHolderLabel.hidden = [_textView.text length] > 0;
    
}


#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Property Accessor
- (void)setKeyDesc:(DMKeyDesc *)keyDesc
{
    _keyDesc = keyDesc;
    _diagram = nil;
    [self configureView];
}

- (void)setDiagram:(DMDiagram *)diagram
{
    _diagram = diagram;
    _keyDesc = nil;
    [self configureView];
}

#pragma mark - Outlet Methods
- (IBAction)didTapBuyttonInfo:(id)sender
{
    id obj = self.diagram == nil ? self.keyDesc : self.diagram;
    [self.delegate descriptionCell:self didSelectObject:obj];
}

#pragma mark - UITextView Delegate
- (void)textViewDidChange:(UITextView *)textView
{
    if (self.diagram == nil) {
        self.keyDesc.desc = textView.text;
    } else {
        self.diagram.desc = textView.text;
    }

    _placeHolderLabel.hidden = [_textView.text length] > 0;
}


@end
