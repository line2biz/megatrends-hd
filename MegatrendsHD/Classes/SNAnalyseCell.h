//
//  SNAnalyseCell.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 13.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SNAnalyseCellDelegate;

@class DMAnalyse;

@interface SNAnalyseCell : UITableViewCell

@property (strong, nonatomic) DMAnalyse *analyse;

@property (weak, nonatomic) IBOutlet id<SNAnalyseCellDelegate> delegate;


@end

@protocol SNAnalyseCellDelegate <NSObject>

- (void)analyseCell:(SNAnalyseCell *)analyseCell didShowAnalyse:(DMAnalyse *)analyse;

@end
