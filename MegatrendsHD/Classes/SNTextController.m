//
//  SNTextController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 11.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNTextController.h"

@interface SNTextController ()
{
    __weak IBOutlet UITextView *_textView;
}

@end

@implementation SNTextController

@dynamic attributedText;

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.view.backgroundColor = [UIColor clearColor];
    _textView.backgroundColor = [UIColor clearColor];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Property Accessors
- (NSAttributedString *)attributedText
{
    
    return _textView.attributedText;
}

- (void)setAttributedText:(NSAttributedString *)attributedText
{
    _textView.attributedText = attributedText;
    //_textView.dataDetectorTypes = UIDataDetectorTypeLink | UIDataDetectorTypePhoneNumber;
}

@end
