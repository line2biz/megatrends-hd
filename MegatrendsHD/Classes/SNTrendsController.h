//
//  SNTrendsController.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 27.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMTrend;

@protocol SNTrendsControllerDelegate;

@interface SNTrendsController : UIViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) id<SNTrendsControllerDelegate> delegate;

@end


@protocol SNTrendsControllerDelegate <NSObject>

- (void)trendsController:(SNTrendsController *)trendsController didSelecteTrend:(DMTrend *)trend;

@end