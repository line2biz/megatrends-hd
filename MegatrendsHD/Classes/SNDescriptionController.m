//
//  SNDescriptionController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 17.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNDescriptionController.h"
#import "SNDescriptionCell.h"
#import "SNViewerController.h"
#import "SNDetailController.h"

#import "SNTestCell.h"

@interface SNDescriptionController () <NSFetchedResultsControllerDelegate, SNDescriptionCellDelegate>
{
    NSIndexPath *_selectedIndexPath;
    id _observer;
}

@end

@implementation SNDescriptionController



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self registerObserver];
}

- (void)dealloc
{
    [self unregisterObserver];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[DMManager sharedManager] saveContext];
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Observer
- (void)registerObserver
{
    __weak SNDescriptionController *weekSelf = self;

    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        SNDescriptionController *strongSelf = weekSelf;
        if (strongSelf) {
            [strongSelf.tableView reloadData];
        }
    }];
}

- (void)unregisterObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}

#pragma mark - View Life Methods
- (void)reloadDiagramCell
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self configureCell:(SNDescriptionCell *)[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
}

#pragma mark - Property Accesssor
- (void)setDiagram:(DMDiagram *)diagram
{
    if (_diagram != diagram) {
        [self willChangeValueForKey:@"diagram"];
        _diagram = diagram;
        [self didChangeValueForKey:@"diagram"];
        _fetchedResultsController = nil;
        [self.tableView reloadData];
    }
    

}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.diagram == nil) {
        return 0;
    }
    return [[self.fetchedResultsController sections] count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    
    if ([[self.fetchedResultsController sections] count]) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][0];
        return [sectionInfo numberOfObjects];
    }
    
    return 0;
    
}

- (void)configureCell:(SNDescriptionCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.placeHolderLabel.text = LMLocalizedString(@"Kommentar", nil);
    
    if (indexPath.section == 0) {
        [cell setDiagram:self.diagram];
        
    } else {
        NSIndexPath *idxPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section - 1];
        [cell setKeyDesc:[self.fetchedResultsController objectAtIndexPath:idxPath]];
    }
    
    [cell setDelegate:self];
    
//    NSLog(@"%s", __FUNCTION__);
//    
//    if (_selectedIndexPath == indexPath) {
//        
////        cell.textView.editable = YES;
////        cell.textView.userInteractionEnabled = YES;
//        
//        cell.contentView.backgroundColor = [UIColor clearColor];
//        
//    } else {
////        cell.userInteractionEnabled = NO;
////        cell.textView.editable = NO;
////        
//        
//        cell.contentView.backgroundColor = [UIColor darkGrayColor];
//    }
//    
    

//    if (indexPath.section == _selectedIndexPath.section && indexPath.row == _selectedIndexPath.row) {
//        [cell addGestureRecognizer:cell.tapRecognizer];
//        cell.textView.editable = YES;
//    } else {
//        [cell removeGestureRecognizer:cell.tapRecognizer];
//        cell.tapRecognizer = nil;
//        cell.textView.editable = NO;
//        
//    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SNDescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (_selectedIndexPath != nil && _selectedIndexPath.section == indexPath.section && _selectedIndexPath.row == indexPath.row) {
//        return;
//    }
//    
//    NSIndexPath *oldIndexPath = _selectedIndexPath;
//    _selectedIndexPath = indexPath;
//    
//    [self.tableView beginUpdates];
//    if (oldIndexPath == nil) {
//        oldIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//    }
//    
//    if (oldIndexPath.section == 1 && [self.tableView numberOfRowsInSection:1] > 0) {
//        [self.tableView reloadRowsAtIndexPaths:@[oldIndexPath] withRowAnimation:UITableViewRowAnimationFade];
//    }
//    
    
//    NSLog(@"%s begin", __FUNCTION__);
//
//    NSIndexPath *oldIndexPath = _selectedIndexPath;
//    _selectedIndexPath = indexPath;
//    [self.tableView beginUpdates];
//    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
//    
//    [self.tableView endUpdates];
//
//    if (oldIndexPath != nil) {
//        [self configureCell:(SNDescriptionCell *)[self.tableView cellForRowAtIndexPath:oldIndexPath] atIndexPath:oldIndexPath];
//    }
//    
//    [self configureCell:(SNDescriptionCell *)[self.tableView cellForRowAtIndexPath:_selectedIndexPath] atIndexPath:_selectedIndexPath];
//    
//    

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == _selectedIndexPath.section && indexPath.row == _selectedIndexPath.row) {
        NSString *str = nil;
        if (indexPath.section == 0) {
            str = self.diagram.desc;
            
        } else {
            NSIndexPath *idxPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section - 1];
            DMKeyDesc *keyDesc = [self.fetchedResultsController objectAtIndexPath:idxPath];
            str = keyDesc.desc;
        }
        
        
        CGSize size = [str
                       sizeWithFont:[UIFont fontWithName:@"Avenir-Medium" size:14.0f]
                       constrainedToSize:CGSizeMake(265, 2000)
                       lineBreakMode:NSLineBreakByWordWrapping];
        
        CGFloat height = MAX(136, size.height + 17);
        
        return height + 58;
        
    } else {
        return 136 + 58;
    }
    
    
    if ([[tableView indexPathForSelectedRow] compare:indexPath] != NSOrderedSame || _selectedIndexPath == nil) {
        return 136 + 58;
    }
    
    
    
}

#pragma mark -  Cell Delegate
- (void)descriptionCell:(SNDescriptionCell *)descriptionCell didSelectObject:(id)object
{
    [(SNDetailController *)self.parentViewController showDescriptionOfObject:object];
}

#pragma mark - FetchedReults Controller
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext) {
        return _managedObjectContext;
    }
    
    _managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    return _managedObjectContext;
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (self.diagram == nil) {
        _fetchedResultsController  = nil;
        return nil;
        
    }
    
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMKeyDesc class])];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"diagram = %@", self.diagram];
    fetchRequest.predicate = predicate;
    
//    [fetchRequest setSortDescriptors:[DMKeyDesc sortDescriptorsArray]];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:DMKeyDescOrderKey ascending:DMKeyDescAscendingValue];
    [fetchRequest setSortDescriptors:@[sort]];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}


#pragma mark - Fetched Results Controller Delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex + 1] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex + 1] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:newIndexPath.row inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexPath.row inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(SNDescriptionCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:1]] atIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:1]];
            break;
            
        case NSFetchedResultsChangeMove:
            indexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:1];
            newIndexPath = [NSIndexPath indexPathForRow:newIndexPath.row inSection:1];
            
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
