//
//  SNDiagramView.m
//  MegatrendsHD
//
//  Created by Dev on 16.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNDiagramView.h"
#import "DMDiagram+Category.h"
#import "SNAppearance.h"

@interface SNDiagramView ()
{
//    UILabel *_numberLabel;
}
@end

@implementation SNDiagramView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _numberLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _numberLabel.textAlignment = NSTextAlignmentCenter;
        _numberLabel.font = [SNAppearance appFontWithSize:12];
        _numberLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:_numberLabel];
        
        self.backgroundColor = [UIColor blackColor];
    }
    return self;
}

- (void)setDiagram:(DMDiagram *)diagram
{
    if (diagram != _diagram) {
        [self willChangeValueForKey:@"diagram"];
        _diagram = diagram;
        [self didChangeValueForKey:@"diagram"];
        
        
        self.numberLabel.text = [NSString stringWithFormat:@"%02d", _diagram.trend.tid + 1];
    }
    
    self.numberLabel.textColor = _diagram.color > 0 ? [UIColor whiteColor] : [SNAppearance colorVinous];
    self.backgroundColor = [SNAppearance colorForHandleValue:diagram.color];
    
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
