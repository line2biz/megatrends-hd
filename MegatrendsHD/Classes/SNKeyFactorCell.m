//
//  SNTrendProfileCell.m
//  MegatrendsHD
//
//  Created by ios on 5/8/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNKeyFactorCell.h"
#import "SNAppearance.h"

@implementation SNKeyFactorCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.lblNumber.font = [SNAppearance appFontWithSize:self.lblNumber.font.pointSize];
    self.lblTitle.font = [SNAppearance appFontWithSize:self.lblTitle.font.pointSize];
}

@end
