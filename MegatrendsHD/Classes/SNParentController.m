//
//  SNParentController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 25.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNParentController.h"
#import "SNTrendsController.h"
#import "SNHomeController.h"
#import "SNNavigationController.h"
#import "SNTrendsPageController.h"
#import "SNInfoController.h"
#import "SNZpunktController.h"
#import "SNSettingController.h"
#import "SNAnalysisController.h"
#import "SNAppDelegate.h"

#define BOTTOM_SPACE 724.f

@interface SNParentController () <SNTrendsControllerDelegate>
{
    __weak IBOutlet NSLayoutConstraint *_leadingSpaceMenu;
    __weak IBOutlet UIView *_containerView;
    __weak IBOutlet UIView *_trendsContainerView;
    
    UIViewController *_currentContrller;
}

@end

@implementation SNParentController

- (void)awakeFromNib
{
    [super awakeFromNib];
    _trendsContainerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Menu-Bkg"]];
    self.view.backgroundColor = [UIColor blackColor];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTrendsMenuHidden:YES animated:NO];
    
    UIImage *image = nil;
    if ([[UIApplication sharedApplication] applicationIconBadgeNumber] > 0) {
        image = [UIImage imageNamed:@"Icon-ZPunkt-Badged"];
    } else {
        image = [UIImage imageNamed:@"Icon-ZPunkt"];
    }
    [self.badgeButton setImage:image forState:UIControlStateNormal];
    
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([[SNAppDelegate sharedDelegate] showNews]) {
        [[SNAppDelegate sharedDelegate] setShowNews:NO];
        [self didTapButtonZPunkt:nil];
    }
    
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Segueway
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue destinationViewController] respondsToSelector:@selector(managedObjectContext)]) {
        [segue.destinationViewController setManagedObjectContext:[[DMManager sharedManager] managedObjectContext]];
    }
    
    if ([segue.destinationViewController isKindOfClass:[SNSettingController class]]) {
        [self setTrendsMenuHidden:YES animated:YES];
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        [[segue destinationViewController] setSenderButton:sender];
        [(UIButton *)sender setSelected:YES];
    }
    
    if ([segue.destinationViewController isKindOfClass:[SNTrendsController class]]) {
        [self setTrendsMenuHidden:YES animated:YES];
        [segue.destinationViewController setDelegate:self];
    }
  
    if ([segue.destinationViewController isKindOfClass:[SNHomeController class]]) {
        [self setTrendsMenuHidden:YES animated:YES];
        _currentContrller = segue.destinationViewController;
    }
  
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonTrends:(id)sender
{
    _leadingSpaceMenu.constant = (_leadingSpaceMenu.constant == 0) ? - CGRectGetWidth(_trendsContainerView.frame) : 0;
    [UIView animateWithDuration:.2f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)didTapButtonHome:(id)sender
{
    [self setTrendsMenuHidden:YES animated:YES];
    
    if (![_currentContrller isKindOfClass:[SNHomeController class]]) {
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNHomeController"];
        [self showNewController:viewController sender:nil];
    }
    
}

- (IBAction)didTapButtonInfo:(id)sender
{
    [self setTrendsMenuHidden:YES animated:YES];
    
    if ([_currentContrller isKindOfClass:[SNNavigationController class]]) {
        SNNavigationController *navController = (SNNavigationController *)_currentContrller;
        if ([navController.rootViewController isKindOfClass:[SNInfoController class]]) {
            [(SNNavigationController *)_currentContrller popToRootViewControllerAnimated:NO];
            return;
        }
    }
    
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNInfoController"];
    SNNavigationController *navController = [SNNavigationController navigationControllerWithRootController:viewController];
    [self showNewController:navController sender:nil];
}

- (IBAction)didTapButtonZPunkt:(id)sender
{
    [self setTrendsMenuHidden:YES animated:YES];
    
    if ([_currentContrller isKindOfClass:[SNNavigationController class]]) {
        SNNavigationController *navController = (SNNavigationController *)_currentContrller;
        if ([navController.rootViewController isKindOfClass:[SNZpunktController class]]) {
            [(SNNavigationController *)_currentContrller popToRootViewControllerAnimated:NO];
            return;
        }
    }
    
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNZpunktController"];
    SNNavigationController *navController = [SNNavigationController navigationControllerWithRootController:viewController];
    [self showNewController:navController sender:nil];
}

- (IBAction)didTapButtonAnalysis:(id)sender
{
    [self setTrendsMenuHidden:YES animated:YES];
    
    if ([_currentContrller isKindOfClass:[SNNavigationController class]]) {
        SNNavigationController *navController = (SNNavigationController *)_currentContrller;
        if ([navController.rootViewController isKindOfClass:[SNAnalysisController class]]) {
            [(SNNavigationController *)_currentContrller popToRootViewControllerAnimated:NO];
            return;
        }
    }
    
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNAnalysisController"];
    SNNavigationController *navController = [SNNavigationController navigationControllerWithRootController:viewController];
    [self showNewController:navController sender:nil];
}


#pragma mark - Trends Controller Delegage
- (void)trendsController:(SNTrendsController *)trendsController didSelecteTrend:(DMTrend *)trend
{
    [self setTrendsMenuHidden:YES animated:YES];
    
    if ([_currentContrller isKindOfClass:[SNNavigationController class]]) {
        SNNavigationController *navController = (SNNavigationController *)_currentContrller;
        if ([navController.rootViewController isKindOfClass:[SNTrendsPageController class]]) {
            [(SNNavigationController *)_currentContrller popToRootViewControllerAnimated:NO];
            [(SNTrendsPageController *)navController.rootViewController setTrend:trend];
            
            return;
        }
    }
    
    SNTrendsPageController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTrendsPageController"];
    viewController.trend = trend;
    SNNavigationController *navController = [SNNavigationController navigationControllerWithRootController:viewController];
    [self showNewController:navController sender:nil];
    
}

#pragma mark - Utility Methods
- (void)setTrendsMenuHidden:(BOOL)hidden animated:(BOOL)animated
{
    _leadingSpaceMenu.constant = hidden ? - CGRectGetWidth(_trendsContainerView.frame) : 0;
    if (animated) {
        [UIView animateWithDuration:.2f animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

- (void)showNewController:(UIViewController *)viewController sender:(id)sender
{
    
//    Show New Controller
    [self addChildViewController:viewController];
    viewController.view.frame = _containerView.bounds;
    [_containerView addSubview:viewController.view];
    [viewController didMoveToParentViewController:self];
    
//    Remove Old Controller
    if ([_currentContrller isKindOfClass:[UINavigationController class]]) {
        [(UINavigationController *)_currentContrller popToRootViewControllerAnimated:NO];
    }
    
    if ([_currentContrller isKindOfClass:[SNNavigationController class]]) {
        [(SNNavigationController *)_currentContrller popToRootViewControllerAnimated:NO];
    }
    
    [_currentContrller willMoveToParentViewController:nil];
    [_currentContrller.view removeFromSuperview];
    [_currentContrller removeFromParentViewController];
    
    _currentContrller = viewController;
}

@end
