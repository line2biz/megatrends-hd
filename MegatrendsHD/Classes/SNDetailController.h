//
//  SNAnalyseDetailController.h
//  MegatrendsHD
//
//  Created by ios on 5/14/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMAnalyse;

@interface SNDetailController : UIViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) DMAnalyse *currentAnalyse;

- (void)showDescriptionOfObject:(NSManagedObject *)managedObject;


@end
