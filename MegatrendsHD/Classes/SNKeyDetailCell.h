//
//  SNKeyCell.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 16.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMKeyFactor;

@interface SNKeyDetailCell : UITableViewCell

@property (strong, nonatomic) DMKeyFactor *keyFactor;
@property (strong, nonatomic) DMDiagram *diagram;

@end
