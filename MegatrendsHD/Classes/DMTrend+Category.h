//
//  DMTrend+Category.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 26.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMTrend.h"

FOUNDATION_EXPORT NSString  *const DMTrendSortOrderKey;
FOUNDATION_EXPORT BOOL       const DMTrendSortOrderAscendingValue;

@interface DMTrend (Category)

- (NSString *)title;
- (NSString *)teaser;
- (NSString *)desc;
- (NSString *)chartSource;
- (NSString *)imageName;
- (NSString *)imageSmallName;

+ (DMTrend *)trendById:(int16_t)tid;
+ (NSUInteger)numberOfTrends;



@end
