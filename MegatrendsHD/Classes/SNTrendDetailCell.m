//
//  SNTrendDetailCell.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 17.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNTrendDetailCell.h"
@interface SNTrendDetailCell()
{
    __weak IBOutlet UILabel *_numberLabel;
}
@end


@implementation SNTrendDetailCell

- (void)configureView
{
    _numberLabel.text = [NSString stringWithFormat:@"%02d:", self.trend.tid + 1];
    _titleLabel.text = [self.trend.title uppercaseString];
}

- (void)setTrend:(DMTrend *)trend
{
    _trend = trend;
    [self configureView];
}

@end
