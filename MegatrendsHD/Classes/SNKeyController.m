//
//  SNKeyController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 24.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNKeyController.h"
#import "SNAppearance.h"
#import "SNChartPreviewController.h"
#import "SNParentController.h"
#import "SNAppDelegate.h"

@interface SNKeyController ()
{
    __weak IBOutlet UIImageView *_imageView;
    __weak IBOutlet UITextView *_chartTextView;
    __weak IBOutlet UITextView *_keyTextView;
    
    __weak IBOutlet UIView *_chartView;
    id _observer;
}


@end

@implementation SNKeyController


- (void)configureView
{
    //    Trend Chart
    NSString *imageName = [NSString stringWithFormat:@"%02d.%dsvg@2x", self.keyFactor.trend.tid + 1, self.keyFactor.kid + 1];
    
    NSBundle *bundle = [[SNLocalizationManager sharedManager] bundle];
    NSString *pictPath = [bundle pathForResource:imageName ofType:@"png"];
    _imageView.image = [UIImage imageWithContentsOfFile:pictPath];
    
    //    Trend Chart Description
    UIFont *headerFont = [SNAppearance boldAppFontWithSize:17 - 3];
    UIFont *paraFont  = [SNAppearance appFontWithSize:17];
    UIFont *sourceFont  = [SNAppearance appFontWithSize:17 - 3];
    
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] init];
    NSString *paragraph = [NSString stringWithFormat:@"%@\n\n", self.keyFactor.chartDesc];
    [attrString.mutableString appendString:paragraph];
    
    NSRange paraRange = NSMakeRange(0, [paragraph length]);
    [attrString addAttribute:NSFontAttributeName value:paraFont range:paraRange];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:paraRange];
    
    //    _textView.text = [SNAppDelegate chartDescriptionForIndex:self.index];
    //    _textView.font = [UIFont fontWithName:@"AvenirLTPro-Medium" size:_textView.font.pointSize];
    
    NSString *str = LMLocalizedString(@"Chart Quelle", nil);
    str = [str stringByAppendingString:@": "];
    
    paraRange = NSMakeRange([attrString length], [str length]);
    [attrString.mutableString appendString:str];
    [attrString addAttribute:NSFontAttributeName value:headerFont range:paraRange];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:paraRange];
    
    
    str = self.keyFactor.chartSource;
    
    paraRange = NSMakeRange([attrString length], [str length]);
    [attrString.mutableString appendString:str];
    [attrString addAttribute:NSFontAttributeName value:sourceFont range:paraRange];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:paraRange];
    
    
    _chartTextView.attributedText = attrString;
    _chartTextView.dataDetectorTypes = UIDataDetectorTypeLink | UIDataDetectorTypePhoneNumber;
    
    _keyTextView.text = self.keyFactor.teaser;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_chartView.backgroundColor = [UIColor clearColor];
    _keyTextView.backgroundColor = [UIColor clearColor];
    _keyTextView.editable = NO;
    _chartTextView.editable = NO;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Profile-Container-Bkg"]];
    
    UITapGestureRecognizer *tapRecognizer  = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapRecognizer];
    
    __weak SNKeyController *weekSelf = self;
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        SNKeyController *strongSelf = weekSelf;
        if (strongSelf) {
            [strongSelf configureView];
        }
    }];
    
    [self configureView];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Outlet Methods
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    CGPoint location = [gestureRecognizer locationInView:self.view];
    if (CGRectContainsPoint(_imageView.frame, location)) {
        
        SNChartPreviewController *chartPreviewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNChartPreviewController"];
        chartPreviewController.trend = self.keyFactor.trend;
        chartPreviewController.keyFactor = self.keyFactor;
        
        chartPreviewController.chartController = (SNChartController *)self.parentViewController.parentViewController;
        
        SNParentController *parentController = [SNAppDelegate parentController];
        parentController.modalPresentationStyle = UIModalPresentationCurrentContext;
        [parentController presentViewController:chartPreviewController animated:NO completion:NULL];
        //        [self performSegueWithIdentifier:@"Show Chart" sender:self];
    }
    
}

@end
