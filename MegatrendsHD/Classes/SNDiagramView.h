//
//  SNDiagramView.h
//  MegatrendsHD
//
//  Created by Dev on 16.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMDiagram;

@interface SNDiagramView : UIView

@property (strong, nonatomic) DMDiagram *diagram;
@property (strong, nonatomic) UILabel *numberLabel;

@end
