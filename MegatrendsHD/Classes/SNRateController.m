//
//  SNRateController.m
//  MegatrendsHD
//
//  Created by Шурик on 5/24/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNRateController.h"
#import "SNAppDelegate.h"
#import "SNParentController.h"
#import "SNAppearance.h"
#import "SNAlertController.h"

@interface SNRateController ()
{
    __weak IBOutlet UIView *_containerView;
    
    __weak IBOutlet UIView *_chanceView;
    __weak IBOutlet UIView *_riskView;
    __weak IBOutlet UIView *_handleView;
    
    __weak IBOutlet UILabel *_numberLabel;
    __weak IBOutlet UILabel *_titelLabel;
    __weak IBOutlet UILabel *_chanceLabel;
    __weak IBOutlet UILabel *_riskLabel;
    __weak IBOutlet UILabel *_handleLable;
    
    __weak IBOutlet UIButton *_buttonYES;
    __weak IBOutlet UIButton *_buttonNO;
    __weak IBOutlet NSLayoutConstraint *_titelHeight;
    __weak IBOutlet NSLayoutConstraint *_titelIndent;
}

@property (nonatomic) NSUInteger chanceValue;
@property (nonatomic) NSUInteger riskValue;
@property (nonatomic) NSUInteger handleValue;

@property (nonatomic) NSUInteger maxChanceValue;
@property (nonatomic) NSUInteger maxRiskValue;
@property (nonatomic) NSUInteger maxHandleValue;

@end

@implementation SNRateController

- (void)configureView
{
    if ([_chanceView.subviews count] == 0) {
        
        CGFloat step = 0;
        for (int idx = 1; idx < 10; idx++) {
            CGRect frame = CGRectMake(step, 0, 44, 44);
            UIView *view = [[UIView alloc] initWithFrame:frame];
            view.tag = idx;
            view.backgroundColor = [SNAppearance colorGray];
            [_chanceView addSubview:view];
            
            step = CGRectGetMaxX(view.frame) + 5;
        }
    }
    
    if ([_riskView.subviews count] == 0) {
        CGFloat step = 0;
        for (int idx = 1; idx < 10; idx++) {
            CGRect frame = CGRectMake(step, 0, 44, 44);
            UIView *view = [[UIView alloc] initWithFrame:frame];
            view.tag = idx;
            view.backgroundColor = [SNAppearance colorGray];
            [_riskView addSubview:view];
            
            step = CGRectGetMaxX(view.frame) + 5;
        }
    }
    
    if ([_handleView.subviews count] == 0) {
        CGFloat step = 0;
        for (int idx = 1; idx < 6; idx++) {
            CGRect frame = CGRectMake(step, 0, 44, 44);
            UIView *view = [[UIView alloc] initWithFrame:frame];
            view.tag = idx;
            view.backgroundColor = [SNAppearance colorGray];
            [_handleView addSubview:view];
            
            step = CGRectGetMaxX(view.frame) + 5;
        }
    }
    
    for (UIView *view in _chanceView.subviews) {
        UIColor *color = self.chanceValue >= view.tag ? [SNAppearance colorChance] : [SNAppearance colorGray];
        view.backgroundColor = color;
    }
    
    for (UIView *view in _riskView.subviews) {
        UIColor *color = self.riskValue >= view.tag ? [SNAppearance colorRisk] : [SNAppearance colorGray];
        view.backgroundColor = color;
    }
    
    for (UIView *view in _handleView.subviews) {
        UIColor *color = self.handleValue >= view.tag ? [SNAppearance colorForHandleValue:view.tag] : [SNAppearance colorGray];
        view.backgroundColor = color;
    }

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.managedObject isKindOfClass:[DMDiagram class]]) {
        DMDiagram *diagram = (DMDiagram *)self.managedObject;
        self.chanceValue = diagram.positionY;
        self.riskValue = diagram.positionX;
        self.handleValue = diagram.color;
        _titelLabel.text = [diagram.trend.title uppercaseString];
        _numberLabel.text = [NSString stringWithFormat:@"%02d:", diagram.trend.tid+1];
        _titelIndent.constant = 50;
        [[[DMManager sharedManager] managedObjectContext] save:NULL];
        
    } else if ([self.managedObject isKindOfClass:[DMKeyDesc class]]) {
        DMKeyDesc *key = (DMKeyDesc *)self.managedObject;
        self.chanceValue = key.positionY;
        self.riskValue = key.positionX;
        self.handleValue = key.type;
        _numberLabel.text = [NSString stringWithFormat:@"%02d.%d:", key.keyFactor.trend.tid+1,key.keyFactor.kid+1];
        _titelLabel.text = key.keyFactor.title;
        _titelIndent.constant = 69;
        [[[DMManager sharedManager] managedObjectContext] save:NULL];
    }
    
    CGSize size = [_titelLabel.text sizeWithFont:[UIFont fontWithName:@"Avenir-Heavy" size:17.0f]
                   constrainedToSize:CGSizeMake(387-2, 2000)
                   lineBreakMode:NSLineBreakByWordWrapping];
    if (size.height > 25) {
        _titelHeight.constant = 48;
        } else {
        _titelHeight.constant = 24;
        }
    

    
    _chanceLabel.text = LMLocalizedString(@"Chancen", nil);
    _riskLabel.text = LMLocalizedString(@"Risiken", nil);
    _handleLable.text = LMLocalizedString(@"Handlungsdruck", nil);
    
    
    _containerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Right-Container-Bkg"]];
    
    _chanceView.backgroundColor = [UIColor clearColor];
    _riskView.backgroundColor = [UIColor clearColor];
    _handleView.backgroundColor = [UIColor clearColor];
    
    [_buttonYES setTitle:[LMLocalizedString(@"OK", nil) uppercaseString] forState:UIControlStateNormal];
    [_buttonNO setTitle:[LMLocalizedString(@"Abbrechen", nil) uppercaseString] forState:UIControlStateNormal];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    
}


+ (void)showRateWithManagedObject:(NSManagedObject *)managedObject
{
    UIViewController *viewController = (UIViewController *)[SNAppDelegate parentController];
    SNRateController *rateController = [viewController.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    rateController.managedObject = managedObject;
    
    viewController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [viewController presentViewController:rateController animated:NO completion:NULL];
}

- (void)hideController
{
    [self dismissViewControllerAnimated:NO completion:NULL];

}


#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Property Accessors
- (void)setChanceValue:(NSUInteger)chanceValue animated:(BOOL)animated;
{
    if (self.chanceValue == 1 && chanceValue == 1) {
        self.chanceValue = 0;
    } else {
        self.chanceValue = chanceValue;
    }
    
    [UIView animateWithDuration:.3 animations:^{
        [self configureView];
    }];
}

- (void)setRiskValue:(NSUInteger)riskValue animated:(BOOL)animate
{
    if (self.riskValue == 1 && riskValue == 1) {
        self.riskValue = 0;
    } else {
        self.riskValue = riskValue;
    }
    [UIView animateWithDuration:.3 animations:^{
        [self configureView];
    }];
}

- (void)setHandleValue:(NSUInteger)handleValue animated:(BOOL)animated
{
    if (self.handleValue == 1 && handleValue == 1) {
        self.handleValue = 0;
    } else {
        self.handleValue = handleValue;
    }
    [UIView animateWithDuration:.3 animations:^{
        [self configureView];
    }];
}

#pragma mark - Outlet Methods
- (IBAction)handleTap:(UIGestureRecognizer *)sender
{
    CGPoint location = [sender locationInView:self.view];
    
    if (CGRectContainsPoint(_containerView.frame, location)) {
        
        location = [sender locationInView:_containerView];
        
        if (CGRectContainsPoint(_chanceView.frame, location)) {
            for (UIView *view in _chanceView.subviews) {
                CGPoint loc = [sender locationInView:_chanceView];
                if (CGRectContainsPoint(view.frame, loc)) {
                    [self setChanceValue:view.tag animated:YES];
                    return;
                }
            }
        }
        
        if (CGRectContainsPoint(_riskView.frame, location)) {
            for (UIView *view in _riskView.subviews) {
                CGPoint loc = [sender locationInView:_riskView];
                if (CGRectContainsPoint(view.frame, loc)) {
                    [self setRiskValue:view.tag animated:YES];
                    return;
                }
            }
        }
        
        if (CGRectContainsPoint(_handleView.frame, location)) {
            for (UIView *view in _handleView.subviews) {
                CGPoint loc = [sender locationInView:_handleView];
                if (CGRectContainsPoint(view.frame, loc)) {
                    [self setHandleValue:view.tag animated:YES];
                    return;
                }
            }
        }
        
        
    } else {
        [self hideController];
    }
}


- (IBAction)didTapButton:(id)sender
{
    if (sender == _buttonYES) {
        if ([self.managedObject isKindOfClass:[DMDiagram class]]) {
            DMDiagram *diagram = (DMDiagram *)self.managedObject;
            diagram.positionY = self.chanceValue;
            diagram.positionX = self.riskValue;
            [diagram checkCorner];
            diagram.color = self.handleValue;
            [[[DMManager sharedManager] managedObjectContext] save:NULL];
           
        } else if ([self.managedObject isKindOfClass:[DMKeyDesc class]]) {
            
            DMKeyDesc *key = (DMKeyDesc *)self.managedObject;
            key.positionY = self.chanceValue;
            key.positionX = self.riskValue;
            key.type = self.handleValue;

            DMDiagram *diagram = key.diagram;
            [diagram updatePositionValues];
            
            [[[DMManager sharedManager] managedObjectContext] save:NULL];            
        }
    }
    
    [self hideController];
}

@end
