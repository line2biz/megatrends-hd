//
//  SNWorkController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 15.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNWorkController.h"

#import "SNWorkCell.h"

#define CELL_IDENTIFER        @"CELL_IDENTIFER"
#define CELL_TEXT             @"CELL_TEXT"
#define CELL_NUMBER           @"CELL_NUMBER"
#define CELL_IMAGE            @"CELL_IMAGE"

#define CELL_HEADER_01          @"CellHeader01"
#define CELL_SIMPLE_TEXT        @"CellText"
#define CELL_HEADER_02          @"CellHeader02"
#define CELL_HEADER_03          @"CellHeader03"
#define CELL_IMAGE_TEXT         @"CellImageText"
#define CELL_NUMBER_TEXT        @"CellNumberText"
#define CELL_TABLECELL          @"Cell"

@interface SNWorkController ()
{
    NSArray *_array;
}

@end

@implementation SNWorkController

#pragma mark - View Life Cycle
- (void)configureView
{
    _array = @[
               @{CELL_IDENTIFER: CELL_HEADER_01, CELL_TEXT : [LMLocalizedString(@"Header 1", nil) uppercaseString] },
               @{CELL_IDENTIFER: CELL_SIMPLE_TEXT, CELL_TEXT : LMLocalizedString(@"Text 1", nil) },
               @{CELL_IDENTIFER: CELL_HEADER_02, CELL_TEXT : LMLocalizedString(@"Header 2", nil) },
               @{CELL_IDENTIFER: CELL_HEADER_03, CELL_TEXT : LMLocalizedString(@"Header 21", nil) },
               @{CELL_IDENTIFER: CELL_IMAGE_TEXT, CELL_TEXT : LMLocalizedString(@"Text 21", nil), CELL_IMAGE: @"Icon-Megatrends"},
               @{CELL_IDENTIFER: CELL_IMAGE_TEXT, CELL_TEXT : LMLocalizedString(@"Text 22", nil), CELL_IMAGE: @"keyFactorOpen"},
               
               //@{CELL_IDENTIFER: CELL_HEADER_02, CELL_TEXT : LMLocalizedString(@"", nil) },
               
               @{CELL_IDENTIFER: CELL_HEADER_03, CELL_TEXT : LMLocalizedString(@"Header 31", nil) },
               
               @{CELL_IDENTIFER: CELL_IMAGE_TEXT, CELL_TEXT : LMLocalizedString(@"Text 31", nil), CELL_IMAGE: @"Icon-Profile"},//(Icon personalisierte Liste)
               
               // 1 Zukunftsanalyse anlegen
               @{CELL_IDENTIFER: CELL_NUMBER_TEXT, CELL_TEXT : LMLocalizedString(@"Text 32", nil), CELL_NUMBER: @"1."},//(Schaubild Liste „Meine Zukunftsanalysen“)
               // 2 Megatrends bewerten
               @{CELL_IDENTIFER: CELL_NUMBER_TEXT, CELL_TEXT : LMLocalizedString(@"Text 33", nil), CELL_NUMBER: @"2."},//(Schaubild Matrix)
               @{CELL_IDENTIFER: CELL_NUMBER_TEXT, CELL_TEXT : LMLocalizedString(@"Text 34", nil), CELL_NUMBER: @"3."},//(Schaubild Handlungsdruck-Regler)
               // 3 Analyse detaillieren
               @{CELL_IDENTIFER: CELL_NUMBER_TEXT, CELL_TEXT : LMLocalizedString(@"Text 35", nil), CELL_NUMBER: @"4."},//(Schaubild Liste mit Schlüsselfaktoren und aktiv-inaktiv Reglern)
               @{CELL_IDENTIFER: CELL_IMAGE_TEXT, CELL_TEXT : LMLocalizedString(@"Text 36", nil), CELL_IMAGE: @"file_1340"},//(Icon Textansicht Schlüsselfaktor)
               // 4 Analyse abschließen und teilen
               @{CELL_IDENTIFER: CELL_NUMBER_TEXT, CELL_TEXT : LMLocalizedString(@"Text 39", nil), CELL_NUMBER: @"5."},
               @{CELL_IDENTIFER: CELL_HEADER_03, CELL_TEXT : LMLocalizedString(@"Header 41", nil) },
               @{CELL_IDENTIFER: CELL_IMAGE_TEXT, CELL_TEXT : LMLocalizedString(@"Text 41", nil), CELL_IMAGE: @"Icon-Info-html"},//(Icon Z)
               @{CELL_IDENTIFER: CELL_IMAGE_TEXT, CELL_TEXT : LMLocalizedString(@"Text 42", nil), CELL_IMAGE: @"Icon-Punk-Badge"},//(Icon Z mit Aktualisierungs-Flag)
               @{CELL_IDENTIFER: CELL_IMAGE_TEXT, CELL_TEXT : LMLocalizedString(@"Text 43", nil), CELL_IMAGE: @"Icon-Settings-html"},//(Icon Einstellungen)
];
}

- (void)viewDidLoad
{
    __weak typeof(self) weakSelf = self;
    
    NSLog(@"%s", __FUNCTION__);
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = [UIColor clearColor];
    
    
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            [strongSelf configureView];
            [strongSelf.tableView reloadData];
        }
    }];
    [self configureView];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_array count];
}
- (void)configureCell:(SNWorkCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* dict = [_array objectAtIndex:[indexPath row]];
    if ([dict objectForKey:CELL_IMAGE] != nil) {
        cell.iconImageView.image = [UIImage imageNamed:[dict objectForKey:CELL_IMAGE]];
        cell.titleLabel.text = [dict objectForKey:CELL_TEXT];
        }  else if ([dict objectForKey:CELL_NUMBER] != nil){
            cell.numberLabel.text = [dict objectForKey:CELL_NUMBER];
            cell.titleLabel.text = [dict objectForKey:CELL_TEXT];
            } else {
                cell.textLabel.text = [dict objectForKey:CELL_TEXT];
            }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIden = [[_array objectAtIndex:indexPath.row] valueForKey:CELL_IDENTIFER];
    SNWorkCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIden];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#define HEIGHT_MARGIN_IOS_7 4.f

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictionary = [_array objectAtIndex:indexPath.row];
    UIFont *font = [UIFont fontWithName:@"Avenir-Medium" size:17];
    NSString *title= [dictionary valueForKey:CELL_TEXT];
    
    NSString *cellIden = [[_array objectAtIndex:indexPath.row] valueForKey:CELL_IDENTIFER];
    if (indexPath.row == 0) {
        return 30.f;
        
    } else if ([cellIden isEqualToString:CELL_HEADER_02]) {
        return 40.f;
     
    } else if ([cellIden isEqualToString:CELL_HEADER_03]) {
        return 30.f;
    } else if ([cellIden isEqualToString:CELL_IMAGE_TEXT]) {
        CGSize size = CGSizeMake(CGRectGetWidth(self.view.frame) - 76, 2000) ;
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:title attributes:@{ NSFontAttributeName: font }];
            CGRect rect = [attributedText boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin context:nil];
            size = rect.size;
            size.height = size.height + HEIGHT_MARGIN_IOS_7;
            
        } else {
            size = [title sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
        }
        
        
        CGFloat cellHeight = size.height + 8 + 12;
        return cellHeight;
        
        
    } else if ([cellIden isEqualToString:CELL_SIMPLE_TEXT]) {
        
        
        CGSize size = CGSizeMake(CGRectGetWidth(self.view.frame) - 10, 2000);
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:title attributes:@{ NSFontAttributeName: font }];
            CGRect rect = [attributedText boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin context:nil];
            size = rect.size;
            size.height = size.height + HEIGHT_MARGIN_IOS_7;
            
        } else {
            size = [title sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
        }
        int cellHeight = size.height + 8 + 12;
        return cellHeight;

    } else if ([cellIden isEqualToString:CELL_NUMBER_TEXT]) {
        
        CGSize size = CGSizeMake(CGRectGetWidth(self.view.frame) - 76, 2000);
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:title attributes:@{ NSFontAttributeName: font }];
            CGRect rect = [attributedText boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin context:nil];
            size = rect.size;
            size.height = size.height + HEIGHT_MARGIN_IOS_7;
            
        } else {
            size = [title sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
        }
        
        int cellHeight = size.height + 8 + 12;
        return cellHeight;
    }
    return 44.f;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (cell.backgroundColor != [UIColor clearColor]) {
        cell.backgroundColor = [UIColor clearColor];
    }
    
    if (cell.contentView.backgroundColor != [UIColor clearColor]) {
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
}
@end
