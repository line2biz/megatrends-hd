//
//  DMKeyDesc+Category.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 20.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMKeyDesc+Category.h"

NSString *const DMKeyDescOrderKey = @"keyFactor.kid";
BOOL      const DMKeyDescAscendingValue = YES;

@implementation DMKeyDesc (Category)

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    
    self.positionX = 0;
    self.positionY = 0;
    self.type = 0;
    
}

+ (DMKeyDesc *)keyDescForDiagram:(DMDiagram *)diagram keyFactor:(DMKeyFactor *)keyFactor
{
    DMKeyDesc *keyDesc = nil;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"diagram == %@ AND keyFactor == %@", diagram, keyFactor];
    fetchRequest.fetchLimit = 1;
    
    NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
    NSArray *results = [context executeFetchRequest:fetchRequest error:NULL];
    if ([results count]) {
        keyDesc = [results lastObject];
    }
    
    return keyDesc;
}

+ (NSArray *)sortDescriptorsArray
{
    NSSortDescriptor *sort_1 = [[NSSortDescriptor alloc] initWithKey:@"positionY" ascending:NO];
    NSSortDescriptor *sort_2 = [[NSSortDescriptor alloc] initWithKey:@"positionX" ascending:NO];
    NSSortDescriptor *sort_3 = [[NSSortDescriptor alloc] initWithKey:@"type" ascending:NO];
    NSSortDescriptor *sort_4 = [[NSSortDescriptor alloc] initWithKey:DMKeyDescOrderKey ascending:DMKeyDescAscendingValue];
    
    return @[sort_1, sort_2, sort_3, sort_4];
}

//- (void)setPositionX:(int64_t)value
//{
//    [self willChangeValueForKey:@"positionX"];
//    [self setPrimitiveValue:[NSNumber numberWithLongLong:value] forKey:@"positionX"];
//    [self didChangeValueForKey:@"positionX"];
//    
//    
//    
//    NSExpression *ex = [NSExpression expressionForFunction:@"max:" arguments:@[ [NSExpression expressionForKeyPath:@"positionX"] ]];
//    
//    NSExpressionDescription *ed = [[NSExpressionDescription alloc] init];
//    [ed setName:@"result"];
//    [ed setExpression:ex];
//    [ed setExpressionResultType:NSInteger16AttributeType];
//    
//    NSArray *properties = [NSArray arrayWithObject:ed];
//    
//    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([self class])];
//    [request setPropertiesToFetch:properties];
//    [request setResultType:NSDictionaryResultType];
//    
//    
//    
//    NSArray *results = [[[DMManager  sharedManager] managedObjectContext] executeFetchRequest:request error:nil];
//    
//    NSDictionary *resultsDictionary = [results objectAtIndex:0];
//    NSNumber *resultValue = [resultsDictionary objectForKey:@"result"];
//    
//    NSInteger maxValue = MAX((int)value, [resultValue integerValue]);
//
//    
//    self.diagram.positionX = MAX(self.diagram.positionX, maxValue);
//    
//    
//}
//
//- (void)setPositionY:(int16_t)value
//{
//    [self willChangeValueForKey:@"positionY"];
//    [self setPrimitiveValue:[NSNumber numberWithLongLong:value] forKey:@"positionY"];
//    [self didChangeValueForKey:@"positionY"];
//    
//    
//    
//    NSExpression *ex = [NSExpression expressionForFunction:@"max:" arguments:@[ [NSExpression expressionForKeyPath:@"positionY"] ]];
//    
//    NSExpressionDescription *ed = [[NSExpressionDescription alloc] init];
//    [ed setName:@"result"];
//    [ed setExpression:ex];
//    [ed setExpressionResultType:NSInteger16AttributeType];
//    
//    NSArray *properties = [NSArray arrayWithObject:ed];
//    
//    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([self class])];
//    [request setPropertiesToFetch:properties];
//    [request setResultType:NSDictionaryResultType];
//    
//    
//    
//    NSArray *results = [[[DMManager  sharedManager] managedObjectContext] executeFetchRequest:request error:nil];
//    
//    NSDictionary *resultsDictionary = [results objectAtIndex:0];
//    NSNumber *resultValue = [resultsDictionary objectForKey:@"result"];
//    
//    NSInteger maxValue = MAX((int)value, [resultValue integerValue]);
//    
//    
//    self.diagram.positionY = MAX(self.diagram.positionY, maxValue);
//    
//    
//}

@end
