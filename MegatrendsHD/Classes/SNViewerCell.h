//
//  SNViewerCell.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 21.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNViewerCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
