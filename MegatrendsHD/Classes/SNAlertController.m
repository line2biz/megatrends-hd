//
//  SNAlertController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 22.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNAlertController.h"
#import "SNAppDelegate.h"
#import "SNAppearance.h"

@interface SNAlertController ()
{
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UIView *_dottedLineView;
    __weak IBOutlet UILabel *_messageLabel;
    
    __weak IBOutlet NSLayoutConstraint *_heightConstraint;
    __weak IBOutlet NSLayoutConstraint *_dividerPosition;
    __weak IBOutlet NSLayoutConstraint *_buttonNoWidth;
    
    __weak IBOutlet UIButton *_buttonYES;
    __weak IBOutlet UIButton *_buttonNO;
    __weak IBOutlet UIView *_divider;
}

@property (copy) CompletionHandler completionHandler;

@end

@implementation SNAlertController

- (void)viewDidLoad
{
    [super viewDidLoad];
	_dottedLineView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Dotted-Line"]];
    _titleLabel.text = self.alertTitle;
    _messageLabel.text = self.messageText;
    
    CGSize size = [self.messageText sizeWithFont:[SNAppearance appFontWithSize:15.0f] constrainedToSize:CGSizeMake(241, 4*19) lineBreakMode:NSLineBreakByWordWrapping];
    
    if (self.cancelButtonTitle != nil ) {
        [_buttonYES setTitle:self.okButtonTitle forState:UIControlStateNormal];
        [_buttonNO setTitle:self.cancelButtonTitle forState:UIControlStateNormal];
        
        CGSize size = [self.cancelButtonTitle sizeWithFont:_buttonNO.titleLabel.font constrainedToSize:CGSizeMake(400, 30) lineBreakMode:NSLineBreakByClipping];
        _buttonNoWidth.constant = size.width + 30;
        
        _buttonNO.hidden = NO;
    } else {
        [_buttonYES setTitle:[LMLocalizedString(@"OK", nil) uppercaseString] forState:UIControlStateNormal];
        _buttonNO.hidden = YES;
    }
    
    _heightConstraint.constant = size.height;
    if ([[LMLocalizedString(@"ja", nil) uppercaseString] isEqualToString:@"YES"]){
        _dividerPosition.constant = 58;
    } else {
        _dividerPosition.constant = 40;
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods


+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle completionHandler:(CompletionHandler)completionHandler
{
    
    UIViewController *viewController = (UIViewController *)[SNAppDelegate parentController];
    SNAlertController *alertController = [viewController.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    
    alertController.alertTitle = title;
    alertController.messageText = message;
    alertController.okButtonTitle = okButtonTitle;
    alertController.cancelButtonTitle = cancelButtonTitle;
    alertController.completionHandler = completionHandler;
    if (completionHandler == nil) {alertController.oneButton = YES;}
    
    viewController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [viewController presentViewController:alertController animated:NO completion:NULL];
    
    
}

#pragma mark - Outlet Methods
- (IBAction)didTapButton:(id)sender
{
    if (sender == _buttonYES) {
        CompletionHandler block = self.completionHandler;
        if (block != nil) {
            block(YES);
        }
    } else {
        CompletionHandler block = self.completionHandler;
        if (block != nil) {
            block(NO);
        }
    }
    
    [self dismissViewControllerAnimated:NO completion:NULL];
    
    
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
