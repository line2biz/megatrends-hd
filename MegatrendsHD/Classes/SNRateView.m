//
//  SNRateView.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 20.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNRateView.h"
#import "SNAppearance.h"

@interface SNRateView()
{
    UILabel *_titleLabel;
    
    NSMutableArray *_viewsArray;
}
@end

@implementation SNRateView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    
    _viewsArray = [NSMutableArray new];
    
    self.backgroundColor = [UIColor clearColor];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(6, 0, 150, 17)];
    _titleLabel.font = [SNAppearance appFontWithSize:10.f];
    _titleLabel.backgroundColor = [UIColor clearColor];
    
    CGFloat step = 0.f;
    
    NSUInteger maxCount = 10;
    if (self.rateType == SNRateViewTypeHandle) {
        maxCount = 6;
    }
    
    for (int idx = 1; idx < maxCount; idx++) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(step, 19, 15, 15)];
        view.tag = idx;
        view.backgroundColor = [SNAppearance colorGray];
        [self addSubview:view];
        [_viewsArray addObject:view];
        
        step = CGRectGetMaxX(view.frame) + 5.f;
    }
    
//    _titleLabel.text = @"";
    [self configureView];
    
    [self addSubview:_titleLabel];
    
}

- (void)configureView
{
    NSString *sTitle = nil;
    switch (self.rateType) {
        case SNRateViewTypeChance:
            sTitle = LMLocalizedString(@"Chancen", nil);
            break;
        case SNRateViewTypeRisk:
            sTitle = LMLocalizedString(@"Risiken", nil);
            break;
        case SNRateViewTypeHandle:
            sTitle = LMLocalizedString(@"Handlungsdruck", nil);
            break;
    }
    
    for (UIView *view in _viewsArray) {
        UIColor *color = [SNAppearance colorGray];
        if (self.rateType == SNRateViewTypeChance) {
            color = self.rateValue >= view.tag ? [SNAppearance colorChance] : [SNAppearance colorGray];
        } else if (self.rateType == SNRateViewTypeRisk) {
            color = self.rateValue >= view.tag ? [SNAppearance colorRisk] : [SNAppearance colorGray];
        } else {
            color = self.rateValue >= view.tag ? [SNAppearance colorForHandleValue:view.tag] : [SNAppearance colorGray];
        }
        
        view.backgroundColor = color;
    }
    

    
    _titleLabel.text = [sTitle uppercaseString];
    
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Property Accessor
- (void)setRateValue:(NSUInteger)rateValue
{
    _rateValue = rateValue;
    [self configureView];
}



@end
