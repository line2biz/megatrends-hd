//
//  SNInfoController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 11.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNInfoController.h"
#import "SNNavigationController.h"
#import "SNAppearance.h"
#import "SNTextController.h"
//#import "SNHelpController.h"
#include "SNWorkController.h"

typedef NS_ENUM (NSInteger, SNInfoType) {
    SNInfoTypeAbout,
    SNInfoTypeHelp
};

@interface SNInfoController () <SNNavigationControllerProtocol>
{
    __weak IBOutlet UIButton *_aboutButton;
    __weak IBOutlet UIButton *_helpButton;
    __weak IBOutlet UIView *_containerView;
    
    id _observer;
    UIViewController *_currentContrller;
    UIButton *_selectedButton;
    
    NSArray *_buttonsArray;
    
}
@end

@implementation SNInfoController

- (void)configureView
{
    
    void(^ConfigueTypeButton)(UIButton *button, SNInfoType infoType) = ^(UIButton *button, SNInfoType infoType) {
        
        button.tag = infoType;
        button.titleLabel.numberOfLines = 2;
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
        button.titleLabel.font = [SNAppearance appFontWithSize:button.titleLabel.font.pointSize];
        
        NSString *title = nil;
        switch (infoType) {
            case SNInfoTypeAbout:
                title = LMLocalizedString(@"Intro", @"Button info name");
                break;
            case SNInfoTypeHelp:
                title = LMLocalizedString(@"Hilfe", @"Button info name");
                break;
        }
        
        [button setTitle:title forState:UIControlStateNormal];
        
    };
    
    
    ConfigueTypeButton(_helpButton, SNInfoTypeHelp);
    ConfigueTypeButton(_aboutButton, SNInfoTypeAbout);
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    _buttonsArray = @[_aboutButton, _helpButton];
    
    _containerView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Profile-Container-Bkg"]];
    
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self configureView];
        [self didTapButtonInfoType:_selectedButton];
    }];
    
    [self didTapButtonInfoType:_aboutButton];
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Container Methods
- (void)showNewController:(UIViewController *)viewController
{
    
    //    Show New Controller
    [self addChildViewController:viewController];
    viewController.view.frame = _containerView.bounds;
    viewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [_containerView addSubview:viewController.view];
    [viewController didMoveToParentViewController:self];
    
    //    Remove Old Controller
    [_currentContrller willMoveToParentViewController:nil];
    [_currentContrller.view removeFromSuperview];
    [_currentContrller removeFromParentViewController];
    
    _currentContrller = viewController;
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonInfoType:(id)sender
{

   
/*    if (_selectedButton == sender) {
        return;
    }
  */
    for (UIButton *btn in _buttonsArray) {
        btn.selected = btn == sender;
    }
    
    _selectedButton = sender;

    SNNavigationController *navController = (SNNavigationController *)self.parentViewController.parentViewController;
    [navController configureView];
    
    if (_selectedButton == _aboutButton) {
        SNTextController *textController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNTextController"];
        textController.attributedText = [self textForTyep:SNInfoTypeAbout];
        [self showNewController:textController];
    } else if (_selectedButton == _helpButton) {
        SNWorkController *helpController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNWorkController"];
        [self showNewController:helpController];
    }

}

- (NSAttributedString *)textForTyep:(SNInfoType)type
{
    //NSLog(@"%s",__FUNCTION__);
    NSString *lang = [[SNLocalizationManager sharedManager] getLanguage];
    
    NSString *title = nil;
    
    NSArray *headers = [NSArray new];
    NSMutableArray *array = [NSMutableArray new];
    
    if (type == SNInfoTypeAbout) {
        if ([lang isEqualToString:@"de"]) {
            
            title = @"Was sind Megatrends?";
            
            headers = @[@"", @"Zeithorizont", @"Reichweite", @"Wirkungsstärke", @"Wünschen Sie Unterstützung bei der strategischen Zukunftsananlyse?", @"Strategische Herausforderungen", @"Wachstumschancen und Innovationspotentiale", @"Portfolioentwicklung"];
            
            [array addObject:@"Megatrends sind langfristige und übergreifende Transformationsprozesse. Wir sehen sie als wirkungsmächtige Einflussgrößen, welche die Märkte der Zukunft prägen. Sie unterscheiden sich von anderen Trends in dreierlei Hinsicht:"];
            
            [array addObject:@"Megatrends sind über einen Zeitraum von Jahrzehnten beobachtbar. Für die Gegenwart existieren bereits quantitative, empirisch eindeutige Indikatoren. Sie können mit hoher Wahrscheinlichkeit noch über mindestens 15 Jahre in die Zukunft projiziert werden."];
            
            [array addObject:@"Megatrends wirken umfassend. Ihr Geltungsbereich erstreckt sich auf alle Weltregionen und alle Akteure – Regierungen, Individuen und ihr Konsumverhalten, aber auch auf Unternehmen und ihre Strategien."];
            
            [array addObject:@"Megatrends bewirken tiefgreifende, mehrdimensionale Umwälzungen aller gesellschaftlichen Teilsysteme – politisch, sozial und wirtschaftlich. Ihre spezifischen Ausprägungen unterscheiden sich von Region zu Region"];
            
            [array addObject:@"Megatrends sind als stabile Treiber des globalen Wandels häufig Startpunkt strategischer Zukunftsanalysen. Kritische Unsicherheiten im Unternehmensumfeld werden darüber hinaus mit Hilfe von Szenarien erfasst. Z_punkt The Foresight Company bietet unterschiedliche Beratungskonzepte und Workshops auf Grundlage einer kundenspezifischen Megatrendanalyse an:"];
            
            [array addObject:@"Megatrends transformieren das Umfeld. Daher stellen wir gemeinsam mit Ihnen die Kernfrage: Was kommt auf Sie zu? Ist das Unternehmen auf den Wandel tatsächlich vorbereitet? Welche Themen sollten auf Führungsebene konsequenter durchdacht werden?"];
            
            [array addObject:@"Megatrends treiben Zukunftsmärkte. Aus Megatrends können daher systematisch Wachstumsfelder abgeleitet werden. Megatrends inspirieren in der frühen Phase des Innovationsprozesses. Sie helfen aber auch dabei, die Kreativität des Teams in die richtigen Bahnen zu lenken."];
            
            [array addObject:@"Die vernetzten Wirkungen von Megatrends werden häufig unterschätzt. In ihrem Zusammenwirken können sie in einzelnen Marktkontexten zu schnellen und disruptiven Veränderungen führen. Ist Ihr Produktportfolio unter diesen Bedingungen zukunftssicher?"];
            
        } else {
            
            title = @"Megatrends";
            
            headers = @[@"", @"Time horizon", @"Reach", @"Intensity of impact", @"Do you need assistance with your strategic future analysis?", @"Strategic challenges", @"Opportunities for growth and innovation potential", @"Portfolio development"];
            
            [array addObject:@"Megatrends are long-term processes of transformation with a broad scope and a dramatic impact. They are considered to be powerful factors which shape future markets. There are three characteristics in which megatrends differ from other trends:"];
            
            [array addObject:@"Megatrends can be observed over decades. Quantitative, empirically unambiguous indicators are available for the present. They can be projected – with high probabilities – at least 15 years into the future."];
            
            [array addObject:@"Megatrends impact comprehensively on all regions and actors – governments, individuals and their consumption patterns, but also businesses and their strategies."];
            
            [array addObject:@"Megatrends cause fundamental, multidimensional transformations of all societal subsystems, whether in politics, society, or economy. Their precise features vary according to the region in question."];
            
            [array addObject:@"As stabile drivers of global change, megatrends are often used as a starting point for strategic foresight. Critical uncertainties within the corporate environment are documented with the help of scenarios. Z_punkt offers a range of advice and a variety of workshops all supported by megatrends analysis that is tailored to each client’s needs:"];
                
            [array addObject:@"Megatrends transform our environment. Therefore, we address the key issue of what the future has in store for you. Is your company really prepared for such change? And, what are the issues that need more careful consideration at board level?"];
                
            [array addObject:@"Megatrends drive markets of the future. As such, it is possible to draw conclusions systematically about future areas of growth. Megatrends provide inspiration in the early stages of the innovation process. However, they also assist in steering the creative efforts of your team down the right track."];
                
            [array addObject:@"The interlinked effects of megatrends are often underestimated. The interplay between megatrends can often lead to rapid and disruptive changes in individual market sectors. You need to ask, therefore: is your product portfolio prepared for the future in light of this fact?"];
            
        }
    } 
    
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[[title uppercaseString] stringByAppendingString:@"\n"]];
    
    UIFont *headerFont = [SNAppearance boldAppFontWithSize:17];
    UIFont *paraFont  = [SNAppearance appFontWithSize:17];
    
    NSRange rangeTitle = NSMakeRange(0, [attrString length]);
    [attrString addAttribute:NSFontAttributeName value:headerFont range:rangeTitle];
    [attrString addAttribute:NSForegroundColorAttributeName value:[SNAppearance colorVinous] range:rangeTitle];
    
 
    
    for (int idx = 0; idx < [headers count]; idx++) {
        NSString *header = [NSString stringWithFormat:@"%@\n", headers[idx]];
        NSUInteger location = [attrString.mutableString length];
        
        [attrString.mutableString appendString:header];
        
        NSRange headerRange = NSMakeRange(location, [header length]);
        [attrString addAttribute:NSFontAttributeName value:headerFont range:headerRange];
        [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:headerRange];
        
        
        location = [attrString.mutableString length];
        
        NSString *paragraph = [NSString stringWithFormat:@"%@\n\n", array[idx]];
        [attrString.mutableString appendString:paragraph];
        NSRange paraRange = NSMakeRange(location, [paragraph length]);
        [attrString addAttribute:NSFontAttributeName value:paraFont range:paraRange];
        [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:paraRange];
        
        
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.paragraphSpacing = 5;
    // setting the font below makes line spacing become ignored
    [attrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:rangeTitle];
    
    return attrString;
    
}

#pragma mark - Container Protocol
- (NSAttributedString *)titleForNavigationBar
{
    //NSLog(@"%s",__FUNCTION__);
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] init];
    
    UIFont *boldFont = [SNAppearance boldAppFontWithSize:17];
    
    NSString *sTitle = [@"INFO" stringByAppendingString:@": "];
    NSRange range = NSMakeRange(0, sTitle.length);
    [attrString.mutableString appendString:sTitle];
    
    [attrString addAttribute:NSFontAttributeName value:boldFont range:range];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
    
    NSString *sSubTitle = nil;
    
    switch (_selectedButton.tag) {
        case SNInfoTypeAbout:
            sSubTitle = LMLocalizedString(@"Über\nMegatrends", @"Button info name");
            break;
        case SNInfoTypeHelp:
            sSubTitle = LMLocalizedString(@"Arbeiten mit\nMegatrends", @"Button info name");
            break;
    }
    
    sSubTitle = [sSubTitle stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    range = NSMakeRange(NSMaxRange(range), sSubTitle.length);
    [attrString.mutableString appendString:sSubTitle];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:range];
    
    return attrString;
    
}

@end
