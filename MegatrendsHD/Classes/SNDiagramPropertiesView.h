//
//  SNDiagramPropertiesView.h
//  MegatrendsHD
//
//  Created by Admin on 5/24/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMDiagram;


@interface SNDiagramPropertiesView : UIView

@property (strong, nonatomic) NSArray *diagramArray;

@property (strong, nonatomic) NSArray *positionArray;

@end
