//
//  SNDiagramPropertiesView.m
//  MegatrendsHD
//
//  Created by Admin on 5/24/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNDiagramPropertiesView.h"
#import "SNAppearance.h"
#import <QuartzCore/QuartzCore.h>


#define kButtonSize CGSizeMake(44, 44)
#define kMargin 10

@interface SNDiagramPropertiesView()
{
    UILabel  *_handleLabel;
    DMDiagram *_diagram;
    NSMutableArray *_buttonsArray;
}
@end

@implementation SNDiagramPropertiesView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *bgView = [[UIView alloc] initWithFrame:self.bounds];
        bgView.backgroundColor = [UIColor clearColor];
        [self addSubview:bgView];
        
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        [bgView addGestureRecognizer:recognizer];
    }
    return self;
}

- (void)hide
{
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)handleTapGesture:(UITapGestureRecognizer *)recognizer
{
    for (UIView *view in _buttonsArray) {
        CGPoint location = [recognizer locationInView:view];
        if (CGRectContainsPoint(view.frame, location)) {
            return;
        }
    }
    
    [self hide];
}

- (void)didMoveToSuperview
{
    [super didMoveToSuperview];
    
    CGPoint _detailPosition;
    
    if ([self.diagramArray count] > 1) {
        [self showMoreDiagrams];
    } else {
       _diagram = [self.diagramArray lastObject];
        NSValue *pos = [self.positionArray lastObject];
        _detailPosition = [pos CGPointValue];
        
        [self showRateSelectorForDiagram:_diagram position:_detailPosition];
    }
    
}

- (void)showMoreDiagrams
{
    for (UIView *view in _buttonsArray) {
        [view removeFromSuperview];
    }
    
    _buttonsArray = [NSMutableArray array];
    
    for (int idx = 0; idx < [self.diagramArray count]; idx++) {
        CGPoint position = [self.positionArray[idx] CGPointValue];
        DMDiagram *diagram = [self.diagramArray objectAtIndex:idx];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        button.frame = CGRectMake(position.x, position.y - 28, kButtonSize.width, kButtonSize.height);
        button.backgroundColor = [SNAppearance colorForHandleValue:diagram.color];
        button.tag = idx;
        
        button.layer.borderColor = [UIColor darkGrayColor].CGColor;
        button.layer.borderWidth = 1.0f;
        
        NSString *sTitle = [NSString stringWithFormat:@"%02d", diagram.trend.tid + 1];
        [button setTitle:sTitle forState:UIControlStateNormal];
        
        UIColor *color = diagram.color == 0 ? [SNAppearance colorVinous] : [UIColor whiteColor];
        [button setTitleColor:color forState:UIControlStateNormal];
        
        button.titleLabel.font = [SNAppearance boldAppFontWithSize:button.titleLabel.font.pointSize];
        
        [button addTarget:self action:@selector(didTapButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
    
        [_buttonsArray addObject:button];
    }
    
    [UIView animateWithDuration:.3 animations:^{
        
        
        
        CGFloat margin = 5;
        CGFloat step = ((kButtonSize.width + margin) * [_buttonsArray count]) - margin;
        CGFloat fullLength = step;
        CGPoint first = [[self.positionArray objectAtIndex:0] CGPointValue];
        
        CGFloat minY = 0;
        for (UIButton *button in _buttonsArray) {
            if (minY == 0) {
                minY = CGRectGetMinY(button.frame);
            } else {
                minY = MIN(minY, CGRectGetMinY(button.frame));
            }
        }
        
        if (fullLength + (first.x - (step / 2)) > 1024){
            step = 1024 - fullLength;
        } else {
            step = first.x - (step / 2) + 17;
        }
        if (step < 422) {
            step = 422;
        }
               
        for (UIButton *button in _buttonsArray) {
            CGRect frame = button.frame;
            frame.origin.y = minY - 50;
            frame.origin.x = step;
            button.frame = frame;
            step = CGRectGetMaxX(frame) + margin;
            
        }
    }];
    
}

- (void)didTapButton:(UIButton *)button
{
    for (UIButton *but in _buttonsArray) {
        if (but.tag != button.tag) {
            [but removeFromSuperview];
        }
    }
    
    DMDiagram *diagram = [self.diagramArray objectAtIndex:button.tag];
    _diagram = diagram;
    CGPoint position = CGPointMake(CGRectGetMinX(button.frame), CGRectGetMaxY(button.frame));
    [self showRateSelectorForDiagram:diagram position:position];
    
}

- (void)showRateSelectorForDiagram:(DMDiagram *)diagram position:(CGPoint)position
{
    CGSize size = [[diagram.trend.title uppercaseString]
                   sizeWithFont:[SNAppearance appFontWithSize:14]
                   constrainedToSize:CGSizeMake(291, 200)
                   lineBreakMode:NSLineBreakByWordWrapping];
    
    float w = 6 * (kButtonSize.width + kMargin) + kMargin;
    float h = 0;
    if (size.height > 22){
            h = 151;
        } else
        {
            h = 130;
        }
    float x = position.x;
    float y = position.y;
    
    if (CGRectGetMaxX(self.frame) < x + w) {
        x = CGRectGetMaxX(self.frame) - w;
    }
    
    if (CGRectGetMaxY(self.frame) < y + h) {
        y = CGRectGetMaxY(self.frame) - h;
    }
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 4+h-100, w, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.text = LMLocalizedString(@"Handlungsdruck", nil);
    label.font = [SNAppearance boldAppFontWithSize:15];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(x, y, w, h)];
    view.backgroundColor = [UIColor whiteColor];
    
    [view addSubview:label];
    
    [self addSubview:view];
    
    self.alpha = 0;
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 1;
    }];
    
    for (int i = 0; i < 6; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(i * (kButtonSize.width + kMargin) + kMargin, CGRectGetMaxY(label.frame) + kMargin, kButtonSize.width, kButtonSize.height);
        button.backgroundColor = [SNAppearance colorForHandleValue:i];
        button.tag = i;
        
        
        if (i == _diagram.color) {
            button.layer.borderColor = [UIColor blueColor].CGColor;
        } else {
            button.layer.borderColor = [UIColor darkGrayColor].CGColor;
        }
        
        button.layer.borderWidth = 1.0f;
        
        [button addTarget:self action:@selector(didTapChangeColor:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:button];
    }
    
    UILabel *weakLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 80+h-100, 100, 15)];
    weakLabel.text = LMLocalizedString(@"schwach", nil);
    weakLabel.font = [SNAppearance appFontWithSize:12];
    [view addSubview:weakLabel];
    
    UILabel *strongLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(view.frame) - 100 - 10, 80+h-100, 100, 15)];
    
    strongLabel.text = LMLocalizedString(@"stark", nil);
    strongLabel.textAlignment = NSTextAlignmentRight;
    strongLabel.font = weakLabel.font;
    [view addSubview:strongLabel];
    
    //------------------------------------
    UILabel *numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 24, 21)];
    numberLabel.text = [NSString stringWithFormat:@"%02d:",diagram.trend.tid+1];
    numberLabel.font = [SNAppearance appFontWithSize:14];
    numberLabel.textColor = [SNAppearance colorVinous];
    numberLabel.backgroundColor = [UIColor clearColor];
    [view addSubview:numberLabel];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10+24, 10, 291, h-109)];
    titleLabel.text = [diagram.trend.title uppercaseString];
    titleLabel.font = [SNAppearance appFontWithSize:14];
    titleLabel.lineBreakMode =  NSLineBreakByWordWrapping;
    titleLabel.numberOfLines = 0; 
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    [view addSubview:titleLabel];
    
}

- (void)didTapChangeColor:(UIButton *)button
{
    _diagram.color = button.tag;
    [_diagram.managedObjectContext save:NULL];
    [self hide];
}


#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
