//
//  DMTrend.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 15.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DMDiagram, DMKeyFactor;

@interface DMTrend : NSManagedObject

@property (nonatomic, retain) NSString * chartSource_de;
@property (nonatomic, retain) NSString * chartSource_en;
@property (nonatomic, retain) NSString * desc_de;
@property (nonatomic, retain) NSString * desc_en;
@property (nonatomic, retain) NSString * teaser_de;
@property (nonatomic, retain) NSString * teaser_en;
@property (nonatomic) int16_t tid;
@property (nonatomic, retain) NSString * title_de;
@property (nonatomic, retain) NSString * title_en;
@property (nonatomic, retain) NSSet *diagrams;
@property (nonatomic, retain) NSSet *keyFactors;
@end

@interface DMTrend (CoreDataGeneratedAccessors)

- (void)addDiagramsObject:(DMDiagram *)value;
- (void)removeDiagramsObject:(DMDiagram *)value;
- (void)addDiagrams:(NSSet *)values;
- (void)removeDiagrams:(NSSet *)values;

- (void)addKeyFactorsObject:(DMKeyFactor *)value;
- (void)removeKeyFactorsObject:(DMKeyFactor *)value;
- (void)addKeyFactors:(NSSet *)values;
- (void)removeKeyFactors:(NSSet *)values;

@end
