//
//  SNSelectAndEvaluateCell.m
//  MegatrendsHD
//
//  Created by ios on 5/14/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNSelectAndEvaluateCell.h"
#import "SNAppearance.h"

@implementation SNSelectAndEvaluateCell
{
    __weak IBOutlet UILabel *_numberLabel;
    __weak IBOutlet UILabel *_titleLabel;
}


- (void)setHighlighted:(BOOL)highlighted
{
    if (highlighted) {
        self.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Cell-Active"]];
    } else {
        self.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Cell-Inactive"]];
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [self setHighlighted:highlighted];
}

- (void)setSelected:(BOOL)selected
{
    if (selected) {
        self.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Cell-Active"]];
    } else {
        self.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Cell-Inactive"]];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [self setSelected:selected];
}


- (void)setTrend:(DMTrend *)trend
{
    _trend = trend;
    _numberLabel.text = [NSString stringWithFormat:@"%02d:", _trend.tid + 1];
    _titleLabel.text = [_trend title];
    
    
    
}

- (void)setDiagram:(DMDiagram *)diagram
{
    _diagram = diagram;
    
    if (self.diagram != nil && (self.diagram.positionX >= DMDiagramPositionMinValue || self.diagram.positionY >= DMDiagramPositionMinValue)) {
        _titleLabel.textColor = [SNAppearance colorVinous];
        _numberLabel.textColor = [SNAppearance colorVinous];
        
    } else {
        _titleLabel.textColor = [UIColor darkGrayColor];
        _numberLabel.textColor = [UIColor darkGrayColor];
    }
}


@end
