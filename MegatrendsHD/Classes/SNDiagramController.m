//
//  SNSelectAndEvaluateController.m
//  MegatrendsHD
//
//  Created by ios on 5/14/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNDiagramController.h"
#import "SNDetailController.h"
#import "SNSelectAndEvaluateCell.h"
#import "SNNavigationControllerProtocol.h"
#import "SNAppearance.h"
#import "SNDiagramView.h"
#import "DMAnalyse+Category.h"
#import "SNDiagramPropertiesView.h"
#import "SNAlertController.h"
#import "SNAppDelegate.h"
#import <QuartzCore/QuartzCore.h>


#define TREND_FRAME_WIDTH  30
#define TREND_FRAME_HEIGHT 30
#define TREND_MARGIN       5

#define kDiagramViewSize CGSizeMake(TREND_FRAME_WIDTH, TREND_FRAME_HEIGHT)
#define kCellsCount 9
#define kDiagramNotOnGraphic -1


typedef struct {
    NSInteger positionX;
    NSInteger positionY;
    SNCorner  corner;
} DiagramPosition;

@interface SNDiagramController () <SNNavigationControllerProtocol, NSFetchedResultsControllerDelegate, UIGestureRecognizerDelegate>
{
    __weak IBOutlet UIButton *_btnNext;
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet UITableView *_bottomTableView;
    
    __weak IBOutlet UIView *_upperViewContainer;
    __weak IBOutlet UIView *_bottomViewContainer;
    __weak IBOutlet UIView *_diagramContainer;
    
    __weak IBOutlet UILabel *_norelevantLabel;
    __weak IBOutlet UILabel *_norelevantTitleLabel;
    __weak IBOutlet UIView *_norelevantView;
    __weak IBOutlet NSLayoutConstraint *_norelevantHeight;
    
    __weak IBOutlet UILabel *_relevantTitleLabel;
    
    __weak IBOutlet UILabel *_labelOrdinat;
    __weak IBOutlet UILabel *_labelAbsis;
    
    __weak IBOutlet UILabel *_labelHigh;
    __weak IBOutlet UILabel *_labelMiddle;
    __weak IBOutlet UILabel *_labelLow;
    
    __weak IBOutlet UILabel *_labelHighX;
    __weak IBOutlet UILabel *_labelMiddleX;
    __weak IBOutlet UILabel *_labelLowX;
    
    __weak IBOutlet UIImageView *_diagramImageView;
    __weak IBOutlet NSLayoutConstraint *_bottomTableHetght;
    
    UIImageView *_movingCellImageView;
    NSIndexPath *_movingIndexPath;
    CGPoint _prevCellShift;
//    NSArray *_tableTrends;
    
    NSMutableArray *_diagramViews;
    NSMutableArray *_crossArray;
    
    id _observer;
    UIImageView *_crossView;
    
    
}
@end

@implementation SNDiagramController

- (void)configureView
{
    NSString *buttonTitle = [[LMLocalizedString(@"Analyse detaillieren", nil) uppercaseString] stringByAppendingString:@" ➤"];
    [_btnNext setTitle:buttonTitle forState:UIControlStateNormal];
    _btnNext.titleLabel.font = [SNAppearance boldAppFontWithSize:_btnNext.titleLabel.font.pointSize];
    
    _labelOrdinat.text = [LMLocalizedString(@"Chancen", nil) uppercaseString];
    _labelAbsis.text = [LMLocalizedString(@"Risiken", nil) uppercaseString];
    
    _labelHigh.text = [LMLocalizedString(@"hoch", nil) lowercaseString];
    _labelMiddle.text = [LMLocalizedString(@"mittel", nil) lowercaseString];
    _labelLow.text = [LMLocalizedString(@"gering", nil) lowercaseString];
    
    _labelHighX.text = [LMLocalizedString(@"hoch", nil) lowercaseString];
    _labelMiddleX.text = [LMLocalizedString(@"mittel", nil) lowercaseString];
    _labelLowX.text = [LMLocalizedString(@"gering", nil) lowercaseString];
    
    _norelevantLabel.text = LMLocalizedString(@"Nicht relevante Megatrends", nil);
    _norelevantTitleLabel.text = LMLocalizedString(@"[Nicht relevante Megatrends hier positionieren]", nil);
    
    _relevantTitleLabel.text = LMLocalizedString(@"[Relevante Megatrends hier positionieren]", nil);
    
    [_bottomTableView reloadData];
    _bottomTableHetght.constant = 190;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Profile-Container-Bkg"]];
    _norelevantView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Header-Bkg"]];
    _upperViewContainer.backgroundColor = [UIColor clearColor];
    _bottomViewContainer.backgroundColor = [UIColor clearColor];
    _diagramContainer.backgroundColor = [UIColor clearColor];
    
    __weak SNDiagramController *weekSelf = self;
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        SNDiagramController *strongSelf = weekSelf;
        if (strongSelf) {
            [strongSelf configureView];
            [_tableView reloadData];
        }
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:NSManagedObjectContextDidSaveNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [weekSelf updateDiagramViews];
    }];
    
    [self configureView];
    
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    recognizer.delegate = self;
    [_tableView addGestureRecognizer:recognizer];
    
    recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanDiagramGesture:)];
    [_diagramImageView addGestureRecognizer:recognizer];
    
    UILongPressGestureRecognizer *recognizer2 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    [_diagramImageView addGestureRecognizer:recognizer2];
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateDiagramViews];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[DMManager sharedManager] saveContext];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (void)updateDiagramViews
{
    [_diagramViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    
    _diagramViews = [NSMutableArray array];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMDiagram class])];
    fetchRequest.sortDescriptors = [DMDiagram sortDescriptorsArray];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"analyse == %@ && (positionX >= 0 && positionY >= 0)", self.currentAnalyse];
    NSArray *results = [[[DMManager sharedManager] managedObjectContext] executeFetchRequest:fetchRequest error:NULL];
    
    
    for (NSInteger idx = [results count] - 1; idx >= 0; idx--) {
        DMDiagram *diagram = results[idx];
        
        SNDiagramView *view = [self diagramViewForDiagram:diagram];
        if (view != nil) {
            view.layer.borderColor = [UIColor whiteColor].CGColor;
            view.layer.borderWidth = 1.0f;
            [_diagramViews addObject:view];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"positionX == %lld && positionY == %lld && corner = %d && trend.tid < %d", diagram.positionX, diagram.positionY, diagram.corner, diagram.trend.tid];
                                      
            
            NSArray *arr = [results filteredArrayUsingPredicate:predicate];
            NSInteger count = [arr count];
            
            if (count > 0) {
                CGPoint diffPoint;
                CGFloat diff = 3;
                switch (diagram.corner) {
                    case SNCornerLeftTop:
                        diffPoint = CGPointMake(- diff * count, -diff * count);
                        break;
                    case SNCornerLeftBottom:
                        diffPoint = CGPointMake(- diff * count, diff * count);
                        break;
                    case SNCornerRightTop:
                        diffPoint = CGPointMake(diff * count, -diff * count);
                        break;
                    case SNCornerRightBottom:
                        diffPoint = CGPointMake(diff * count, diff * count);
                        break;
                }
                
                CGRect frame = view.frame;
                frame = CGRectMake(CGRectGetMinX(frame) + diffPoint.x, CGRectGetMinY(frame) + diffPoint.y, CGRectGetWidth(frame), CGRectGetHeight(frame));
                view.frame = frame;
//                view.backgroundColor = [UIColor darkGrayColor];
            }
            
            
            
            [_diagramImageView addSubview:view];
            
            
        }
    }
    
    [self updateCrossViews];
    [self updateRelevantTitleView];
}

- (void)updateCrossViews
{
    for (UIView *view in _crossArray) {
        [view removeFromSuperview];
    }
    
    [_crossArray makeObjectsPerformSelector:@selector(removeFromSuperview)];
    _crossArray = [NSMutableArray array];
    
    for (SNDiagramView *diagramView in _diagramViews) {
        DMDiagram *diagram = diagramView.diagram;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Cross-Mark"]];
        imageView.center = CGPointMake(CGRectGetWidth(_diagramImageView.bounds) / kCellsCount * diagram.positionX , CGRectGetHeight(_diagramImageView.bounds) / kCellsCount * (9 - diagram.positionY));
        
        [_crossArray addObject:imageView];
        [_diagramImageView insertSubview:imageView belowSubview:_diagramImageView];
    }
}

- (SNDiagramView *)diagramViewForDiagram:(DMDiagram *)diagram
{
    if (diagram.positionX >= DMDiagramPositionMinValue && diagram.positionY >= DMDiagramPositionMinValue) {
        SNDiagramView *view = [[SNDiagramView alloc] initWithFrame:[self frameForDiagram:diagram]];
        view.diagram = diagram;
        return view;
    } else {
        return nil;
    }

}

- (CGRect)frameForDiagram:(DMDiagram *)diagram
{
    CGFloat height = CGRectGetHeight(_diagramImageView.frame) / 9;
    CGFloat width = CGRectGetWidth(_diagramImageView.frame)  / 9;
    CGRect frame = CGRectMake(0, 0, TREND_FRAME_WIDTH, TREND_FRAME_HEIGHT);
    
    NSInteger corner = diagram.corner;
    
    UIImage *imageCrossMark = [UIImage imageNamed:@"Cross-Mark"];
    CGRect imageRect = CGRectMake(0, 0, imageCrossMark.size.width, imageCrossMark.size.height);
    
    frame.origin.x = width * diagram.positionX;
    if (corner == SNCornerLeftTop || corner == SNCornerLeftBottom) {
        frame.origin.x = frame.origin.x - TREND_FRAME_WIDTH - TREND_MARGIN;
        imageRect.origin.x =  width * diagram.positionX - CGRectGetWidth(imageRect) / 2;
    } else {
        frame.origin.x = frame.origin.x + TREND_MARGIN;
        imageRect.origin.x = width * (diagram.positionX - 1) - CGRectGetWidth(imageRect) / 2;
    }
    
    frame.origin.y = CGRectGetHeight(_diagramImageView.frame) - height * diagram.positionY;
    
    if (corner == SNCornerRightBottom || corner == SNCornerLeftBottom) {
        frame.origin.y = frame.origin.y + TREND_MARGIN;
        imageRect.origin.y = CGRectGetHeight(_diagramImageView.frame) - height * (diagram.positionY) - CGRectGetHeight(imageRect) / 2;
        
    } else {
        frame.origin.y = frame.origin.y - TREND_FRAME_HEIGHT - TREND_MARGIN;
        imageRect.origin.y = CGRectGetHeight(_diagramImageView.frame) - height * (diagram.positionY - 1) - CGRectGetHeight(imageRect) / 2;
    }
    
    return frame;
}



- (void)updateRelevantTitleView
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMDiagram class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(positionX > %d || positionY > %d) && analyse = %@", DMDiagramPositionMinValue, DMDiagramPositionMinValue, self.currentAnalyse];
    NSUInteger count = [[[DMManager sharedManager] managedObjectContext] countForFetchRequest:fetchRequest error:NULL];
    _relevantTitleLabel.hidden = count > 0;
}


#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Segueways
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"Show Detail Controller"]) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMDiagram class])];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"analyse = %@ && (positionX > %d || positionY > %d)", self.currentAnalyse, DMDiagramPositionMinValue, DMDiagramPositionMinValue];
        NSUInteger count = [self.managedObjectContext countForFetchRequest:fetchRequest error:NULL];
        if (count > 0) {
            return YES;
        } else {
            NSString *title = [LMLocalizedString(@"Achtung!", nil) uppercaseString];
            NSString *msg = LMLocalizedString(@"Bitte mindestens einen Megatrend auswählen.", nil);
            
            NSString *sYES = [LMLocalizedString(@"OK", nil) uppercaseString];
            
            [SNAlertController showAlertWithTitle:title message:msg okButtonTitle:sYES cancelButtonTitle:nil completionHandler:NULL];
            
            return NO;
        }
    }
    
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[SNDetailController class]]) {
        [[segue destinationViewController] setManagedObjectContext:self.managedObjectContext];
        [[segue destinationViewController] setCurrentAnalyse:self.currentAnalyse];
    }
}

//- (void)updateTableTrends
//{
//    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([DMTrend class])];
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:DMTrendSortOrderKey ascending:DMTrendSortOrderAscendingValue];
//    request.sortDescriptors = @[sortDescriptor];
//    // @TODO: WHY filteredArrayUsingPredicate works fine when request.predicate does not work?? BUG in CoreData?
//    //request.predicate = [NSPredicate predicateWithFormat:@"NONE diagrams IN %@", self.currentAnalyse.diagrams];
//    _tableTrends = [self.managedObjectContext executeFetchRequest:request error:nil];
//    _tableTrends = [_tableTrends filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NONE diagrams IN %@", self.currentAnalyse.diagrams]];
//}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonExpand:(id)sender
{
    _norelevantHeight.constant = _norelevantHeight.constant == 190 ? 400 : 190;
    [UIView animateWithDuration:.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tableView) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        return [sectionInfo numberOfObjects];
        
    } else {
//        [self updateTableTrends];
//        
//        _norelevantTitleLabel.hidden = [_tableTrends count] > 0;
//        
//        return [_tableTrends count];
    }
    
    return 0;
}

- (void)tableView:(UITableView *)tableView configureCell:(SNSelectAndEvaluateCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.contentView.hidden = NO;
    
    if (tableView == _tableView) {
        DMTrend *trend = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMDiagram class])];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"analyse = %@ && trend = %@", self.currentAnalyse, trend];
        fetchRequest.fetchLimit = 1;
        NSArray *results = [self.managedObjectContext executeFetchRequest:fetchRequest error:NULL];
        cell.trend = trend;
        cell.diagram = [results lastObject];
        
        
    } else {
//        cell.trend = [_tableTrends objectAtIndex:indexPath.row];
//        cell.diagram = nil;
    }
}

- (SNSelectAndEvaluateCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SNSelectAndEvaluateCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [self tableView:tableView configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[self.delegate trendsController:self didSelecteTrend:[self.fetchedResultsController objectAtIndexPath:indexPath]];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

#pragma mark - FetchedReults Controller
- (NSManagedObjectContext *)managedObjectContext
{
    
    if (_managedObjectContext) {
        return _managedObjectContext;
    }
    
    _managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    return _managedObjectContext;
     
}

- (NSPredicate *)predicate
{
    return [NSPredicate predicateWithFormat:@"ANY diagrams IN %@", self.currentAnalyse.diagrams];
}

- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMTrend class])];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:DMTrendSortOrderKey ascending:DMTrendSortOrderAscendingValue];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
//    fetchRequest.predicate = [self predicate];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
    
}


#pragma mark - Fetched Results Controller Delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    
    UITableView *tableView = _tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self tableView:tableView  configureCell:(SNSelectAndEvaluateCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
     
    }

}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView endUpdates];
    [self updateDiagramViews];
}

#pragma mark - Container Protocol
- (NSAttributedString *)titleForNavigationBar
{
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] init];
    
    UIFont *boldFont = [SNAppearance boldAppFontWithSize:17];
    
    NSString *sTitle = [[LMLocalizedString(@"Meine Zukunftsanalyse", nil) stringByAppendingString:@": "] uppercaseString];
    NSRange range = NSMakeRange(0, sTitle.length);
    [attrString.mutableString appendString:sTitle];
    [attrString addAttribute:NSFontAttributeName value:boldFont range:range];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
    
    NSString *s1Title = [self.currentAnalyse.title stringByAppendingString:@": "];
    [attrString.mutableString appendString:s1Title];
    NSRange range1 = NSMakeRange(sTitle.length, s1Title.length);
    [attrString addAttribute:NSForegroundColorAttributeName value:[SNAppearance colorVinous] range:range1];
    
    NSString *s2Title = LMLocalizedString(@"Megatrends auswählen und bewerten", nil);
    [attrString.mutableString appendString:s2Title];
    NSRange range2 = NSMakeRange(sTitle.length+s1Title.length, s2Title.length);
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:range2];
    
    return attrString;
    
}


#pragma mark - UIGestureRecognizer Delegate

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer
{
    UITableView *tableView = (UITableView *)[gestureRecognizer view];
    CGPoint translation = [gestureRecognizer translationInView:tableView];
    
    if (fabsf(translation.x) < fabsf(translation.y)) {
        return NO;
    }
    
    CGPoint location = [gestureRecognizer locationInView:tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:location];
    
    if (indexPath == nil) {
        return NO;
    }
    
    DMTrend *trend = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMDiagram class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"analyse = %@ && trend = %@", self.currentAnalyse, trend];
    fetchRequest.fetchLimit = 1;
    NSArray *results = [self.managedObjectContext executeFetchRequest:fetchRequest error:NULL];
    if ([results count] > 0) {
        return NO;
    }
    
    return YES;
}

- (DMDiagram *)diagramWithTrend:(DMTrend *)trend
{
    NSMutableSet *set = [self.currentAnalyse.diagrams mutableCopy];
    [set intersectSet:trend.diagrams];
    DMDiagram *diagram = [set anyObject];
    
    if (diagram == nil) {
        diagram = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMDiagram class]) inManagedObjectContext:self.managedObjectContext];
        diagram.analyse = self.currentAnalyse;
        diagram.trend = trend;
    }
    
    return diagram;
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:recognizer.view];
    UITableView *tableView = (UITableView *)recognizer.view;
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        _crossView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Cross-Mark@2x"]];
        _crossView.hidden = YES;
        [self.view addSubview:_crossView];
        
        _movingIndexPath = [tableView indexPathForRowAtPoint:location];
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:_movingIndexPath];
        _prevCellShift = [recognizer locationInView:cell];
        
        
        UIGraphicsBeginImageContextWithOptions(cell.frame.size, NO, 0);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [cell.layer renderInContext:context];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        CGRect frame = [self.view convertRect:cell.frame fromView:tableView];
        _movingCellImageView = [[UIImageView alloc] initWithFrame:frame];
        _movingCellImageView.image = image;
        [self.view addSubview:_movingCellImageView];
        
        cell.contentView.hidden = YES;
        
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        
        
        [_crossView removeFromSuperview];
        _crossView = nil;
        
        NSIndexPath *indexPath = _movingIndexPath;
        _movingIndexPath = nil;
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        if (tableView == _tableView) {
            // Top table view
            
            // Diagram image
            CGPoint point = [recognizer locationInView:_diagramImageView];
            
            if (CGRectContainsPoint(_diagramImageView.bounds, point)) {
                
                if ([self.currentAnalyse.diagrams count] == 0) {
                    
                    
                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 350, 50)];
                    label.font = [SNAppearance boldAppFontWithSize:15];
                    label.textAlignment = NSTextAlignmentCenter;
                    label.backgroundColor = [UIColor clearColor];
                    label.textColor = [UIColor darkGrayColor];
                    label.numberOfLines = 2;
                    label.text = LMLocalizedString(@"Halten Sie den Finger auf den Megatrend, um den „Handlungsdruck“ zu definieren.", nil);
                    
                    CGSize size = [label.text sizeWithFont:label.font constrainedToSize:label.frame.size lineBreakMode:label.lineBreakMode];
                    CGRect frame = label.frame;
                    frame.size = size;
                    label.frame = frame;
                    
                    label.center = CGPointMake(point.x, point.y - CGRectGetHeight(label.frame));
                    
                    
                    CGFloat diff = CGRectGetMaxX(_diagramImageView.frame) - CGRectGetMaxX(label.frame);
                    
                    if (diff < 0) {
                        CGRect frame = label.frame;
                        frame.origin.x = frame.origin.x + diff;
                        label.frame = frame;
                    }
                    
                    diff = CGRectGetMinX(_diagramImageView.frame) - CGRectGetMinX(label.frame);
                    
                    
                    if (diff > 0) {
                        CGRect frame = label.frame;
                        frame.origin.x = frame.origin.x + diff;
                        label.frame = frame;
                    }
                    
                    label.alpha = 0;
                    
                    [_diagramImageView addSubview:label];
                    [UIView animateWithDuration:.2 animations:^{
                        label.alpha = 1;
                    } completion:^(BOOL finished) {
                        double delayInSeconds = 4.0;
                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                            [UIView animateWithDuration:.2 animations:^{
                                label.alpha = 0;
                                
                            } completion:^(BOOL finished) {
                                [label removeFromSuperview];
                            }];
                        });
                    }];
                    
                    
                }
                
                
                [_movingCellImageView removeFromSuperview];
                _movingCellImageView = nil;
                
                DMTrend *trend = [self.fetchedResultsController objectAtIndexPath:indexPath];
                DMDiagram *diagram = [self diagramWithTrend:trend];
                
                [self setDiagramPositionForDiagram:diagram location:point];
                
                [diagram.managedObjectContext save:nil];
                
                [self updateDiagramViews];
                
                cell.contentView.hidden = NO;
                
                return;
            }
            
            // Bottom table
            point = [recognizer locationInView:_bottomViewContainer];
            
            if (CGRectContainsPoint(_bottomViewContainer.bounds, point)) {
                
                [_movingCellImageView removeFromSuperview];
                _movingCellImageView = nil;
//
//                DMTrend *trend = [self.fetchedResultsController objectAtIndexPath:indexPath];
//                DMDiagram *diagram = [self diagramWithTrend:trend];
//                
//                if (diagram != nil) {
//                    [self.managedObjectContext deleteObject:diagram];
//                }
//                
////                [self updateTableTrends];
//                
//                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[_tableTrends indexOfObject:trend] inSection:0];
//                [_bottomTableView beginUpdates];
//                [_bottomTableView insertRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationFade];
//                [_bottomTableView endUpdates];
//                
//                [self updateDiagramViews];
                
                return;
            }
        } else {
            // Bottom table view
            
            // Diagram image
            CGPoint point = [recognizer locationInView:_diagramImageView];
            
            if (CGRectContainsPoint(_diagramImageView.bounds, point)) {
                [_movingCellImageView removeFromSuperview];

                
                return;
            }
            
            // Top table
            point = [recognizer locationInView:_upperViewContainer];
            
            if (CGRectContainsPoint(_upperViewContainer.bounds, point)) {
                
                [_movingCellImageView removeFromSuperview];
                _movingCellImageView = nil;

                [_bottomTableView beginUpdates];
                [_bottomTableView deleteRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationFade];
                [_bottomTableView endUpdates];
                
                return;
            }
        }
        
        
        [UIView animateWithDuration:0.25 animations:^{
            _movingCellImageView.transform = CGAffineTransformIdentity;
            CGRect frame = [tableView rectForRowAtIndexPath:indexPath];
            frame = [self.view convertRect:frame fromView:tableView];
            _movingCellImageView.frame = frame;
            
        } completion:^(BOOL finished) {
            cell.contentView.hidden = NO;
            [_movingCellImageView removeFromSuperview];
            _movingCellImageView = nil;
        }];
        
    } else {
        CGPoint point = [recognizer locationInView:_movingCellImageView];
        CGRect frame = _movingCellImageView.frame;
        frame.origin = CGPointMake(frame.origin.x + point.x - _prevCellShift.x, frame.origin.y + point.y - _prevCellShift.y);
        _movingCellImageView.frame = frame;
        
        point = [recognizer locationInView:_diagramImageView];
        [self updateCornerView:point];
    }
    
    
}

- (void)updateCornerView:(CGPoint)point
{
    _crossView.hidden = !CGRectContainsPoint(_diagramImageView.bounds, point);
    
    float w = CGRectGetWidth(_diagramImageView.frame) / kCellsCount;
    float h = CGRectGetHeight(_diagramImageView.frame) / kCellsCount;
    point.x = roundf(point.x / w) * w;
    point.y = roundf(point.y / h) * h;
    point = [self.view convertPoint:point fromView:_diagramImageView];
    _crossView.center = point;
    [self.view bringSubviewToFront:_crossView];
}

- (void)setDiagramPositionForDiagramView:(SNDiagramView *)diagramView
{
    
}


- (DiagramPosition)diagramPositionForLocation:(CGPoint)location
{
    DiagramPosition diagramPosition;
    diagramPosition.positionX = kDiagramNotOnGraphic;
    diagramPosition.positionY = kDiagramNotOnGraphic;
    
    if (CGRectContainsPoint(_diagramImageView.bounds, location)) {
        CGFloat width = CGRectGetWidth(_diagramImageView.bounds) / kCellsCount;
        CGFloat height = CGRectGetHeight(_diagramImageView.bounds) / kCellsCount;
        CGFloat y = CGRectGetHeight(_diagramImageView.bounds) - location.y;
        
        SNCorner corner;
        
        
        float deltaX = location.x - roundf(location.x / width) * width;
        float deltaY = location.y - roundf(location.y / height) * height;
        
        if (deltaX > 0 && deltaY > 0) {
            corner = SNCornerRightBottom;
        } else if (deltaX > 0 && deltaY < 0) {
            corner = SNCornerRightTop;
        } else if (deltaX < 0 && deltaY < 0) {
            corner = SNCornerLeftTop;
        } else {
            corner = SNCornerLeftBottom;
        }
        
        diagramPosition.corner = corner;
        
        NSInteger positionX = round(location.x / width);
        if (positionX > 0 && positionX <= kCellsCount) {
            diagramPosition.positionX = positionX;
        } else {
            diagramPosition.positionX = 0;
        }
        
        NSInteger positionY = round(y / height);
        if (positionY > 0 && positionY <= kCellsCount) {
            diagramPosition.positionY = positionY;
        } else {
            diagramPosition.positionY = 0;
        }
        
    }
    
    
    return diagramPosition;
}


- (void)setDiagramPositionForDiagram:(DMDiagram *)diagram location:(CGPoint)location
{
    if (CGRectContainsPoint(_diagramImageView.bounds, location)) {
        CGFloat width = CGRectGetWidth(_diagramImageView.bounds) / kCellsCount;
        CGFloat height = CGRectGetHeight(_diagramImageView.bounds) / kCellsCount;
        CGFloat y = CGRectGetHeight(_diagramImageView.bounds) - location.y;
        
        SNCorner corner;
        

        float deltaX = location.x - roundf(location.x / width) * width;
        float deltaY = location.y - roundf(location.y / height) * height;
        
        if (deltaX > 0 && deltaY > 0) {
            corner = SNCornerRightBottom;
        } else if (deltaX > 0 && deltaY < 0) {
            corner = SNCornerRightTop;
        } else if (deltaX < 0 && deltaY < 0) {
            corner = SNCornerLeftTop;
        } else {
            corner = SNCornerLeftBottom;
        }
        
        diagram.corner = corner;
        
        NSInteger positionX = round(location.x / width);
        if (positionX > 0 && positionX <= kCellsCount) {
            diagram.positionX = positionX;
        } else {
            diagram.positionX = 0;
        }
        
        NSInteger positionY = round(y / height);
        if (positionY > 0 && positionY <= kCellsCount) {
            diagram.positionY = positionY;
        } else {
            diagram.positionY = 0;
        }
        
    } else {
        diagram.positionX = kDiagramNotOnGraphic;
        diagram.positionY = kDiagramNotOnGraphic;
    }
    
    
    
    [self updateCrossViews];
    NSIndexPath *indexPath = [self.fetchedResultsController indexPathForObject:diagram.trend];
    [self tableView:_tableView configureCell:(SNSelectAndEvaluateCell *)[_tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
    
    
}

- (void)handlePanDiagramGesture:(UIPanGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:_diagramImageView];
    static SNDiagramView *diagramView = nil;
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        
        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(UIView *evaluatedObject, NSDictionary *bindings) {
            return CGRectContainsPoint(evaluatedObject.frame, point);
        }];
        
        diagramView = [[_diagramViews filteredArrayUsingPredicate:predicate] lastObject];
        
        if (diagramView) {
            _crossView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Cross-Mark@2x"]];
            _crossView.hidden = YES;
            [self.view addSubview:_crossView];
        }
        
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
        [_crossView removeFromSuperview];
        _crossView = nil;
        
        CGPoint point = [recognizer locationInView:_diagramImageView];
        
        if (CGRectContainsPoint(_diagramImageView.bounds, point)) {
            
            DiagramPosition newDiagramPosition = [self diagramPositionForLocation:point];
            
            if ([diagramView.diagram.keyDescs count] > 0) {
                
                DMDiagram *diagram = diagramView.diagram;
                
                if (diagram.positionX != newDiagramPosition.positionX || diagram.positionY != newDiagramPosition.positionY) {
                    NSString *title = [LMLocalizedString(@"Achtung!" , nil) uppercaseString];
                    NSString *msg = LMLocalizedString(@"Keine Änderung möglich. Bewerten Sie bitte die aktivierten Schlüsselaspekte.", nil);
                    NSString *sYES = [LMLocalizedString(@"OK", nil) uppercaseString];
                    
                    [SNAlertController showAlertWithTitle:title message:msg okButtonTitle:sYES cancelButtonTitle:nil completionHandler:NULL];
                } else {
                    [self setDiagramPositionForDiagram:diagramView.diagram location:point];
                    [UIView animateWithDuration:0.25 animations:^{
                        diagramView.frame = [self frameForDiagram:diagramView.diagram];
                    } completion:^(BOOL finished) {
                        [self updateDiagramViews];
                    }];
                }
                
                
            } else {
                [self setDiagramPositionForDiagram:diagramView.diagram location:point];
                
                [UIView animateWithDuration:0.25 animations:^{
                    diagramView.frame = [self frameForDiagram:diagramView.diagram];
                } completion:^(BOOL finished) {
                    [self updateDiagramViews];
                }];
            }
            
            return;
        }
        
        point = [recognizer locationInView:_tableView];

        if (CGRectContainsPoint(_tableView.bounds, point)) {
            
            DMDiagram *diagram = diagramView.diagram;
            DMTrend *trend = diagram.trend;
            if (diagram) {
                
                NSString *title = [LMLocalizedString(@"Achtung!", nil) uppercaseString];
                NSString *msg = LMLocalizedString(@"Megatrend mit allen Bewertungen und Kommentaren wirklich entfernen?", nil);
                
                NSString *sYES = [LMLocalizedString(@"ja", nil) uppercaseString];
                NSString *sNO = [LMLocalizedString(@"Abbrechen", nil) uppercaseString];
                
                __weak SNDiagramController *weakSelf = self;
                
                [SNAlertController showAlertWithTitle:title message:msg okButtonTitle:sYES cancelButtonTitle:sNO completionHandler:^(BOOL completed) {
                    if (completed) {
                        SNDiagramController *strongSelf = weakSelf;
                        if (strongSelf) {
                            
                            NSIndexPath *indexPath = [strongSelf.fetchedResultsController indexPathForObject:trend];
                            [strongSelf.managedObjectContext deleteObject:diagram];
                            [strongSelf tableView:strongSelf->_tableView configureCell:(SNSelectAndEvaluateCell *)[strongSelf->_tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
                            [strongSelf.managedObjectContext save:NULL];
//                            [strongSelf updateTableTrends];
                            [strongSelf updateDiagramViews];
                            
                        }
                    }
                }];
    
            }
       
            
            return;
        }
        
        point = [recognizer locationInView:_bottomTableView];

        if (CGRectContainsPoint(_bottomTableView.bounds, point)) {
            
            DMDiagram *diagram = diagramView.diagram;
//            DMTrend *trend = diagram.trend;
            if (diagram != nil) {
                [self.managedObjectContext deleteObject:diagram];
//                [self updateTableTrends];
                
//                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[_tableTrends indexOfObject:trend] inSection:0];
//                [_bottomTableView beginUpdates];
//                [_bottomTableView insertRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationFade];
//                [_bottomTableView endUpdates];
                
                [self updateDiagramViews];
            }
            

            return;
        }

        [UIView animateWithDuration:0.25 animations:^{
            diagramView.frame = [self frameForDiagram:diagramView.diagram];
        }];
        

    } else {
        CGPoint translation = [recognizer translationInView:diagramView];
        [recognizer setTranslation:CGPointZero inView:diagramView];
        
        CGRect frame = diagramView.frame;
        frame.origin = CGPointMake(frame.origin.x + translation.x, frame.origin.y + translation.y);
        diagramView.frame = frame;
        
        point = [recognizer locationInView:_diagramImageView];
        
        [self updateCornerView:point];
    }
    

}

- (void)handleLongPressGesture:(UILongPressGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:_diagramImageView];
    static SNDiagramView *diagramView = nil;
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        
        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(UIView *evaluatedObject, NSDictionary *bindings) {
            return CGRectContainsPoint(evaluatedObject.frame, point);
        }];
        
        NSArray *viewArray = [_diagramViews filteredArrayUsingPredicate:predicate];
        NSMutableArray *positionArray = [NSMutableArray array];
        NSMutableArray *diagramArray = [NSMutableArray array];
        
        diagramView = [[_diagramViews filteredArrayUsingPredicate:predicate] lastObject];
        
        if ([viewArray count] > 0) {
            for (SNDiagramView *diagramView in viewArray) {
                CGPoint position = CGPointMake(diagramView.frame.origin.x, diagramView.frame.origin.y + kDiagramViewSize.height);
                position = [self.view convertPoint:position fromView:_diagramImageView];
                [positionArray addObject:[NSValue valueWithCGPoint:position]];
                [diagramArray addObject:diagramView.diagram];
            }
            
            SNDiagramPropertiesView *view = [[SNDiagramPropertiesView alloc] initWithFrame:self.view.bounds];
            
            view.positionArray = positionArray;
            view.diagramArray = diagramArray;
            [self.view addSubview:view];
        }
    }
    
}

@end
