//
//  SNHomeController.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 26.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNHomeController : UIViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic) IBOutlet UILabel *lblUnderstanding;
@property (weak, nonatomic) IBOutlet UITextView *textUnderstanding;
@property (weak, nonatomic) IBOutlet UILabel *lblAnalysing;
@property (weak, nonatomic) IBOutlet UITextView *textAnalysing;
-(void) setHomeTextLabels;

@end
