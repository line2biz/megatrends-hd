//
//  SNAnalyseDetailController.m
//  MegatrendsHD
//
//  Created by ios on 5/14/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNDetailController.h"
#import "SNNavigationControllerProtocol.h"
#import "SNKeyDetailCell.h"
#import "SNTrendDetailCell.h"
#import "SNAppearance.h"
#import "DMTrend+Category.h"
#import "SNDescriptionController.h"
#import "SNViewerController.h"

#import <QuartzCore/QuartzCore.h>


#define SECTION_HEIGHT  30.f

@interface SNDetailController () <SNNavigationControllerProtocol, NSFetchedResultsControllerDelegate>
{
    __weak IBOutlet UIButton *_btnNext;
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet UIView *_tableHolderView;
    
    __weak IBOutlet NSLayoutConstraint *_horizontalConstraint;
    __weak IBOutlet UITextView *_descriptionTextView;
    __weak IBOutlet UIView *_descriptionHolderView;
    
    DMDiagram *_selectedDiagram;
    SNDescriptionController *_descriptionController;
    BOOL _secondAppear;
    id _observer;
}

//@property (strong, nonatomic) NSArray *diagramArray;

@end

@implementation SNDetailController

- (void)configureView
{
    NSString *sTitle = [[LMLocalizedString(@"Analyse abschließen", nil) uppercaseString] stringByAppendingString:@"  ➤"];
    [_btnNext setTitle:sTitle forState:UIControlStateNormal];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setDescriptionViewHidden:YES animatied:NO];
    
    
    
    _descriptionHolderView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Right-Container-Bkg"]];
    
    [self configureView];
	self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Profile-Container-Bkg"]];
    _tableHolderView.backgroundColor = [UIColor clearColor];
    
    __weak SNDetailController *weekSelf = self;
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        SNDetailController *strongSelf = weekSelf;
        if (strongSelf) {
            [strongSelf configureView];
            [_tableView reloadData];
        }
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [_tableView addGestureRecognizer:tap];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([[self.fetchedResultsController fetchedObjects] count] && !_secondAppear) {
        [_tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
        [self tapOnIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        _secondAppear = YES;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - View Life Methods
- (void)setDescriptionViewHidden:(BOOL)hidden animatied:(BOOL)animated
{
    _horizontalConstraint.constant = hidden ? 1024 : 454;
    
    if (hidden) {
        _descriptionHolderView.layer.masksToBounds = NO;
        _descriptionHolderView.layer.shadowOffset = CGSizeMake(0, 0);
        _descriptionHolderView.layer.shadowRadius = 0;
        _descriptionHolderView.layer.shadowOpacity = 0;
    } else {
        _descriptionHolderView.hidden = NO;
        
        _descriptionHolderView.layer.masksToBounds = NO;
        _descriptionHolderView.layer.shadowOffset = CGSizeMake(0, 0);
        _descriptionHolderView.layer.shadowRadius = 10;
        _descriptionHolderView.layer.shadowOpacity = 0.5;
    }
    
    if (animated) {
        [UIView animateWithDuration:.2f animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            if (hidden) {
                _descriptionHolderView.hidden = YES;
            }
        }];
    }

}

- (void)showDescriptionOfObject:(NSManagedObject *)managedObject
{
    
    NSString *sNumber = nil;
    NSString *sTitle = nil;
    NSString *sText = nil;
    
    if ([managedObject isKindOfClass:[DMDiagram class]]) {
        
        DMDiagram  *diagram = (DMDiagram *)managedObject;
        sNumber = [NSString stringWithFormat:@"%02d: ", diagram.trend.tid + 1];
        sTitle  = [diagram.trend.title uppercaseString];
        sText = diagram.trend.desc;
        
        
    } else {
        
        DMKeyDesc *keyDesc = (DMKeyDesc *)managedObject;
        sNumber = [NSString stringWithFormat:@"%02d.%d ", keyDesc.keyFactor.trend.tid + 1, keyDesc.keyFactor.kid + 1];
        sTitle = keyDesc.keyFactor.title;
        sText = keyDesc.keyFactor.teaser;
        
    }
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] init];
    
    UIFont *boldFont = [SNAppearance boldAppFontWithSize:17];
    
    NSRange range = NSMakeRange(0, sNumber.length);
    [attrString.mutableString appendString:sNumber];
    [attrString addAttribute:NSFontAttributeName value:boldFont range:range];
    [attrString addAttribute:NSForegroundColorAttributeName value:[SNAppearance colorVinous] range:range];
    
    [attrString.mutableString appendString:sTitle];
    range = NSMakeRange(NSMaxRange(range), sTitle.length);
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
    
    sText = [NSString stringWithFormat:@"\n\n%@", sText];
    [attrString.mutableString appendString:sText];
    range = NSMakeRange(NSMaxRange(range), sText.length);
    [attrString addAttribute:NSFontAttributeName value:[SNAppearance appFontWithSize:17] range:range];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
    
    
    _descriptionTextView.attributedText = attrString;
    
    [self setDescriptionViewHidden:NO animatied:YES];
}



#pragma mark - Property Accessor
//- (NSArray *)diagramArray
//{
//    NSArray *__diagramArray;
////    static dispatch_once_t onceToken;
////    dispatch_once(&onceToken, ^{
//        NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:DMDiagramSortOrderKey ascending:DMDiagramSortOrderAscendingValue];
//        __diagramArray = [self.currentAnalyse.diagrams sortedArrayUsingDescriptors:@[sort]];
////    });
//    return __diagramArray;
//    
//}

#pragma mark - Segueways
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[SNDescriptionController class]]) {
        _descriptionController = (SNDescriptionController *)[segue destinationViewController];
        _descriptionController.managedObjectContext = self.managedObjectContext;
        
    } else if ([segue.destinationViewController isKindOfClass:[SNViewerController class]]) {
        [[segue destinationViewController] setCurrentAnalyse:self.currentAnalyse];

    }
}

#pragma mark - Outlet Methods
- (void)tapOnIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        DMDiagram *diagram = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.section inSection:0]];
        
//        DMDiagram *diagram = self.diagramArray[indexPath.section];
        NSInteger countOfRowsToInsert = [diagram.trend.keyFactors count];
        NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
        NSUInteger sectionOpened = indexPath.section;
        
        if (_selectedDiagram != diagram) {
            for (NSInteger i = 0; i < countOfRowsToInsert + 1; i++) {
                [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i + 1 inSection:sectionOpened]];
            }
        }
        
        
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        
        NSInteger previousOpenSectionIndex = [[self.fetchedResultsController  indexPathForObject:_selectedDiagram] row];
        if (_selectedDiagram != nil) {
            NSInteger countOfRowsToDelete = [_selectedDiagram.trend.keyFactors count];
            for (NSInteger i = 0; i < countOfRowsToDelete + 1; i++) {
                [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i + 1 inSection:previousOpenSectionIndex]];
            }
        }
        
        
        // Style the animation so that there's a smooth flow in either direction.
        UITableViewRowAnimation insertAnimation;
        UITableViewRowAnimation deleteAnimation;
        if (_selectedDiagram == nil || _selectedDiagram.trend.tid < diagram.trend.tid) {
            insertAnimation = UITableViewRowAnimationTop;
            deleteAnimation = UITableViewRowAnimationBottom;
        }
        else {
            insertAnimation = UITableViewRowAnimationBottom;
            deleteAnimation = UITableViewRowAnimationTop;
        }
        
        //        insertAnimation = UITableViewRowAnimationNone;
        //        deleteAnimation  = UITableViewRowAnimationNone;
        
        if (_selectedDiagram == diagram) {
            _selectedDiagram = nil;
        } else {
            _selectedDiagram = diagram;
        }
        
        
        // Apply the updates.
        [_tableView beginUpdates];
        [_tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
        if ([indexPathsToInsert count]) {
            NSIndexPath *updateIndexPath = [NSIndexPath indexPathForRow:0 inSection:sectionOpened];
            [_tableView reloadRowsAtIndexPaths:@[updateIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        [_tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
        if ([indexPathsToDelete count]) {
            NSIndexPath *deleteIndexPath = [NSIndexPath indexPathForRow:0 inSection:previousOpenSectionIndex];
            [_tableView reloadRowsAtIndexPaths:@[deleteIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        [_tableView endUpdates];
        
        _descriptionController.diagram = _selectedDiagram;
    }
}

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer
{
    CGPoint location = [gestureRecognizer locationInView:_tableView];
    NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:location];
    if (indexPath == nil) {
        return;
    }
    [self tapOnIndexPath:indexPath];
    
}

- (IBAction)didTapButtonHide:(id)sender
{
    [self setDescriptionViewHidden:YES animatied:YES];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController fetchedObjects] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:section inSection:0];
    
    DMDiagram *diagram = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if (_selectedDiagram == diagram && _selectedDiagram != nil) {
        return [diagram.trend.keyFactors count] + 2;
    }
    
    return 1;

}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    DMDiagram *diagram = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.section inSection:0]];
    if (indexPath.row == 0) {
        [(SNTrendDetailCell *)cell setTrend:diagram.trend];
        SNTrendDetailCell *trendCell = (SNTrendDetailCell *)cell;
        if (_selectedDiagram == diagram) {
            [trendCell.titleLabel setTextColor:[SNAppearance colorVinous]];
            trendCell.dottedLine.backgroundColor = [UIColor clearColor];
        } else {
            [trendCell.titleLabel setTextColor:[UIColor blackColor]];
            trendCell.dottedLine.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Dotted-Line"]];
        }
    } else {
        NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:DMKeyFactorOrderKey ascending:DMKeyFactorAscendingValue];
        NSArray *array = [diagram.trend.keyFactors sortedArrayUsingDescriptors:@[sort]];
        
        if (indexPath.row - 1 < [array count]) {
            SNKeyDetailCell *keyCell = (SNKeyDetailCell *)cell;
            [keyCell setDiagram:diagram];
            [keyCell setKeyFactor:[array objectAtIndex:indexPath.row - 1]];
        }
        
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DMDiagram *diagram = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.section inSection:0]];
    
    NSString *CellIdentifier = indexPath.row == 0 ? @"Cell" : indexPath.row - 1 < [diagram.trend.keyFactors count] ? @"CellKeyFactor" : @"CellEmpty";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - Talbe View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
//    DMDiagram *diagram = self.diagramArray[indexPath.section];
    
    DMDiagram *diagram = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.section inSection:0]];
    
    if (indexPath.row == 0) {
        return SECTION_HEIGHT;
    } else {

        NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:DMKeyFactorOrderKey ascending:DMKeyFactorAscendingValue];
        NSArray *array = [diagram.trend.keyFactors sortedArrayUsingDescriptors:@[sort]];
        
        if (indexPath.row - 1 >= [array count]) {
            return 6;
        }
        
        DMKeyFactor *keyFactor = [array objectAtIndex:indexPath.row - 1];
        NSString *str = keyFactor.title;
        
        CGSize size = [str
                       sizeWithFont:[UIFont fontWithName:@"Avenir-Medium" size:14.0f]
                       constrainedToSize:CGSizeMake(214-14-2, 500)
                       lineBreakMode:NSLineBreakByWordWrapping];
        return MAX(33, size.height + 3);
    }
    return 30.f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
}


#pragma mark - FetchedReults Controller
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext) {
        return _managedObjectContext;
    }
    
    _managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    return _managedObjectContext;
}

- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMDiagram class])];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(positionX >= 0 && positionY >= 0) && analyse = %@", self.currentAnalyse];
    
    fetchRequest.predicate = predicate;
    
    [fetchRequest setSortDescriptors:[DMDiagram sortDescriptorsArray]];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}



#pragma mark - Fetched Results Controller Delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = _tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
//            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.row] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [_descriptionController reloadDiagramCell];
            
//            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            indexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:1];
            newIndexPath = [NSIndexPath indexPathForRow:newIndexPath.row inSection:1];
            
            [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.row] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertSections:[NSIndexSet indexSetWithIndex:newIndexPath.row] withRowAnimation:UITableViewRowAnimationFade];
            [_descriptionController reloadDiagramCell];
            
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView endUpdates];
}


#pragma mark - Container Protocol
- (NSAttributedString *)titleForNavigationBar
{
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] init];
    
    UIFont *boldFont = [SNAppearance boldAppFontWithSize:17];
    
    NSString *sTitle = [[LMLocalizedString(@"Meine Zukunftsanalyse", nil) stringByAppendingString:@": "] uppercaseString];
    NSRange range = NSMakeRange(0, sTitle.length);
    [attrString.mutableString appendString:sTitle];
    [attrString addAttribute:NSFontAttributeName value:boldFont range:range];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
    
    NSString *s1Title = [self.currentAnalyse.title stringByAppendingString:@": "];
    [attrString.mutableString appendString:s1Title];
    NSRange range1 = NSMakeRange(sTitle.length, s1Title.length);
    [attrString addAttribute:NSForegroundColorAttributeName value:[SNAppearance colorVinous] range:range1];
    
    NSString *s2Title = LMLocalizedString(@"Analyse detaillieren", nil);
    [attrString.mutableString appendString:s2Title];
    NSRange range2 = NSMakeRange(sTitle.length+s1Title.length, s2Title.length);
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:range2];

    return attrString;
}

@end
