//
//  SNChartPreviewController.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 10.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMTrend, SNChartController, DMKeyFactor;

@interface SNChartPreviewController : UIViewController

@property (strong, nonatomic) DMTrend *trend;
@property (strong, nonatomic) DMKeyFactor *keyFactor;

@property (weak, nonatomic) SNChartController *chartController;

- (void)dismissChartController;

@end
