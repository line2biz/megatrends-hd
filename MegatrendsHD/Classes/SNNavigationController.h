//
//  SNNavigationController.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 27.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SNNavigationControllerProtocol.h"
#import "SNNavigationBar.h"

@class DMTrend;

@interface SNNavigationController : UIViewController

+ (SNNavigationController *)navigationControllerWithRootController:(UIViewController *)viewController;

@property (strong, readonly) UIViewController *rootViewController;
@property (strong, readonly) NSArray *viewControllers;

@property (weak, nonatomic) IBOutlet SNNavigationBar *navigationBar;


- (void)popToRootViewControllerAnimated:(BOOL)animated;

- (void)configureView;

@end
