//
//  SNKeyCell.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 16.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNKeyDetailCell.h"
#import "SNAlertController.h"

@interface SNKeyDetailCell()
{
    __weak IBOutlet UITextView *_numberLabel;
    __weak IBOutlet UITextView *_titleLabel;
    __weak IBOutlet UISwitch *_switch;
    __weak IBOutlet NSLayoutConstraint *_stringMargin;
    __weak IBOutlet NSLayoutConstraint *_numberMargin;
    
}
@end

@implementation SNKeyDetailCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [_switch setOnTintColor:[UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1]];
}

- (void)configureView
{
    _numberLabel.text = [NSString stringWithFormat:@"%02d.%d", self.keyFactor.trend.tid + 1, self.keyFactor.kid + 1];
    _titleLabel.text = self.keyFactor.title;
    _switch.on = [DMKeyDesc keyDescForDiagram:self.diagram keyFactor:self.keyFactor] != nil;
    CGSize size = [self.keyFactor.title
                   sizeWithFont:[UIFont fontWithName:@"Avenir-Medium" size:14.0f]
                   constrainedToSize:CGSizeMake(214-14-2, 500)
                   lineBreakMode:NSLineBreakByWordWrapping];
    if (size.height == 20)
        {
            _stringMargin.constant = 0;
            _numberMargin.constant = 0;
        } else {
            _stringMargin.constant = -6;
            _numberMargin.constant = -6;
        }
}

#pragma mark - Property Accessory
- (void)setKeyFactor:(DMKeyFactor *)keyFactor
{
    _keyFactor = keyFactor;
    [self configureView];
}

- (IBAction)switchValueChanged:(UISwitch *)sender
{
    
    if (sender.on) {
        NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
        DMKeyDesc *keyDesc = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMKeyDesc class]) inManagedObjectContext:context];
        keyDesc.diagram = self.diagram;
        keyDesc.keyFactor = self.keyFactor;
        [context save:NULL];
        
    } else {
        
        __weak SNKeyDetailCell *weakSelf = self;
        
        NSString *title = [LMLocalizedString(@"Achtung!", nil) uppercaseString];
        NSString *msg = LMLocalizedString(@"Ihre Einstellungen werden nicht gespeichert. Fortfahren?", nil);
        NSString *sYES = [LMLocalizedString(@"ja", nil) uppercaseString];
        NSString *sNO = [LMLocalizedString(@"nein", nil) uppercaseString];
        
        [SNAlertController showAlertWithTitle:title message:msg okButtonTitle:sYES cancelButtonTitle:sNO completionHandler:^(BOOL completed) {
            SNKeyDetailCell *strongSelf = weakSelf;
            if (strongSelf) {
                if (completed) {
                    DMKeyDesc *keyDesc = [DMKeyDesc keyDescForDiagram:strongSelf.diagram keyFactor:self.keyFactor];
                    if (!keyDesc.isDeleted) {
                        [[[DMManager sharedManager] managedObjectContext] deleteObject:keyDesc];
                        [[[DMManager sharedManager] managedObjectContext] save:NULL];
                        [strongSelf.diagram updatePositionValues];
                    }
                    
                } else {
                    strongSelf->_switch.on = YES;
                    
                }
            }
        }];
    }
    
    

}



@end
