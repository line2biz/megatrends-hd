//
//  DMAnalyse+Category.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 13.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMAnalyse+Category.h"

NSString  *const DMAnalyseSortOrderKey               = @"date";
BOOL       const DMAnalyseSortOrderAscendingValue    = YES;

@implementation DMAnalyse (Category)

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    self.date = [NSDate date];
}


- (DMDiagram *)addTrend:(DMTrend *)trend
{
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@", trend];
    
    NSManagedObjectContext *context  = self.managedObjectContext;
    if (context == nil) {
        context = [[DMManager sharedManager] managedObjectContext];
    }
    DMDiagram *diagram = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMDiagram class]) inManagedObjectContext:context];
    diagram.trend = trend;
    diagram.positionY = -1;
    diagram.positionX = -1;
    
    [self addDiagramsObject:diagram];
    return diagram;
}

@end
