//
//  DMKeyFactor.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 18.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "DMKeyFactor.h"
#import "DMKeyDesc.h"
#import "DMTrend.h"


@implementation DMKeyFactor

@dynamic kid;
@dynamic teaser_de;
@dynamic teaser_en;
@dynamic title_de;
@dynamic title_en;
@dynamic keyDesces;
@dynamic trend;

@dynamic chartSource_de;
@dynamic chartSource_en;

@dynamic chartDesc_de;
@dynamic chartDesc_en;

@end
