//
//  SNAlertController.h
//  MegatrendsHD
//
//  Created by Александр Жовтый on 22.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^CompletionHandler)(BOOL completed);

@interface SNAlertController : UIViewController

@property (strong, nonatomic) NSString *alertTitle;
@property (strong, nonatomic) NSString *messageText;
@property (strong, nonatomic) NSString *okButtonTitle;
@property (strong, nonatomic) NSString *cancelButtonTitle;

@property Boolean oneButton;

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle completionHandler:(CompletionHandler)completionHandler;

@end
