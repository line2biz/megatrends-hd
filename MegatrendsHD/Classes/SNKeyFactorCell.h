//
//  SNTrendProfileCell.h
//  MegatrendsHD
//
//  Created by ios on 5/8/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNKeyFactorCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewChevron;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *_numberKeyItemsContstraint;
@end
