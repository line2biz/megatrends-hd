//
//  SNViewerController.m
//  MegatrendsHD
//
//  Created by ios on 5/14/13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNViewerController.h"
#import "SNNavigationControllerProtocol.h"
#import "SNAppearance.h"
#import "PDFGenerator.h"
#import "SNViewerCell.h"

#import <MessageUI/MFMailComposeViewController.h>

static UIImage *UIImageWithPDFPage(CGPDFPageRef pdfPage);

@interface SNViewerController () <SNNavigationControllerProtocol, NSFetchedResultsControllerDelegate, MFMailComposeViewControllerDelegate>
{
    __weak IBOutlet UIButton *_btnNext;
    __weak IBOutlet UIWebView *_webView;
    BOOL _generated;
    PDFGenerator *_pdfGenerator;
    
    __weak IBOutlet UICollectionView *_collectionView;
    
    CGPDFDocumentRef        _PDFDocument;
    id _observer;
    
    NSString *_fileName;
    
    NSCache *_imageCache;
    
}
@end

@implementation SNViewerController

- (void)configureView
{
    NSString *buttonTitle = [LMLocalizedString(@"AUSWERTUNG PER E-MAIL VERSENDEN", nil) uppercaseString];
    [_btnNext setTitle:buttonTitle forState:UIControlStateNormal];
    _btnNext.titleLabel.font = [SNAppearance boldAppFontWithSize:_btnNext.titleLabel.font.pointSize];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _imageCache = [[NSCache alloc] init];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Big-Bkg"]];
    _collectionView.backgroundColor = [UIColor clearColor];
    
     __weak SNViewerController *weekSelf = self;
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        SNViewerController *strongSelf = weekSelf;
        if (strongSelf) {
            [strongSelf configureView];
            
            _pdfGenerator = [[PDFGenerator alloc] init];
            _fileName = [_pdfGenerator generatePDFForAnalyze:self.currentAnalyse];
            
            NSURL *url = [NSURL fileURLWithPath:_fileName];
            
            CGPDFDocumentRef doc = CGPDFDocumentCreateWithURL((__bridge CFURLRef)url);
            [strongSelf setPDFDocument:doc];
            CGPDFDocumentRelease(doc);
            [strongSelf->_imageCache removeAllObjects];
            [strongSelf->_collectionView reloadData];
            
        }
    }];
    [self configureView];
}

- (void)dealloc
{
    CGPDFDocumentRelease(_PDFDocument);
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!_generated) {
        _pdfGenerator = [[PDFGenerator alloc] init];
        _fileName = [_pdfGenerator generatePDFForAnalyze:self.currentAnalyse];
        self.currentAnalyse.generated = YES;
        [[DMManager sharedManager] saveContext];
        
        NSURL *url = [NSURL fileURLWithPath:_fileName];
        
        CGPDFDocumentRef doc = CGPDFDocumentCreateWithURL((__bridge CFURLRef)url);
        [self setPDFDocument:doc];
        CGPDFDocumentRelease(doc);
        [_collectionView reloadData];

    }
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonSend:(id)sender
{
    
    MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
    
    mailViewController.mailComposeDelegate = self;
    mailViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:mailViewController animated:YES completion:nil];
    
    NSData *data = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:_fileName]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    
    NSString *fileName = [@"MegatrendsHD-" stringByAppendingString:LMLocalizedString(@"Analyse", nil)];
    fileName = [fileName stringByAppendingFormat:@"_%@.pdf", [formatter stringFromDate:[NSDate date]]];
    [mailViewController addAttachmentData:data mimeType:@"pdf" fileName:fileName];
    
    
    

}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [[self presentedViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PDF Methods
- (void)setPDFDocument:(CGPDFDocumentRef)PDFDocument
{
    CGPDFDocumentRetain(PDFDocument);
    CGPDFDocumentRelease(_PDFDocument);
    _PDFDocument = PDFDocument;
    //[self scrollToPageNumber:1 animated:NO];
}

- (CGPDFDocumentRef)PDFDocument
{
    return _PDFDocument;
}

- (NSInteger)numberOfPDFPages
{
    return CGPDFDocumentGetNumberOfPages(_PDFDocument);
}

#pragma mark - Collection View Datasource
- (NSUInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self numberOfPDFPages];
}

- (void)configureCollectionCell:(SNViewerCell *)cell forIndexPath:(NSIndexPath *)indexPath
{
    UIImage *image = [_imageCache objectForKey:indexPath];
    if (image == nil) {
        image = UIImageWithPDFPage(CGPDFDocumentGetPage(_PDFDocument, indexPath.row + 1));
        [_imageCache setObject:image forKey:indexPath];
    }
    
    cell.imageView.image = image;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SNViewerCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    [self configureCollectionCell:cell forIndexPath:indexPath];
    return cell;
}



#pragma mark - Container Protocol
- (NSAttributedString *)titleForNavigationBar
{
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] init];
    UIFont *boldFont = [SNAppearance boldAppFontWithSize:17];
    
    NSString *sTitle = [[LMLocalizedString(@"Meine Zukunftsanalyse", nil) stringByAppendingString:@": "] uppercaseString];
    NSRange range = NSMakeRange(0, sTitle.length);
    [attrString.mutableString appendString:sTitle];
    [attrString addAttribute:NSFontAttributeName value:boldFont range:range];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
    
    NSString *s1Title = [self.currentAnalyse.title stringByAppendingString:@": "];
    [attrString.mutableString appendString:s1Title];
    NSRange range1 = NSMakeRange(sTitle.length, s1Title.length);
    [attrString addAttribute:NSForegroundColorAttributeName value:[SNAppearance colorVinous] range:range1];
    
    NSString *s2Title = LMLocalizedString(@"Analyse abschließen", nil);
    [attrString.mutableString appendString:s2Title];
    NSRange range2 = NSMakeRange(sTitle.length+s1Title.length, s2Title.length);
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:range2];
    
    return attrString;
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}


@end

static UIImage *UIImageWithPDFPage(CGPDFPageRef pdfPage)
{
    CGRect pageRect = CGPDFPageGetBoxRect(pdfPage, kCGPDFTrimBox);
    
    UIGraphicsBeginImageContextWithOptions(pageRect.size, NO, .7f);
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
    CGContextSetRGBFillColor(context, 1.0f, 1.0f, 1.0f, 1.0f); // White
    CGContextFillRect(context, CGContextGetClipBoundingBox(context)); // Fill
    
	CGContextTranslateCTM(context,0,pageRect.size.height);
	
	// scale 1:1 100%
	CGContextScaleCTM(context, 1, -1);
    
    
	
	CGContextSaveGState(context);
	CGAffineTransform pdfTransform = CGPDFPageGetDrawingTransform(pdfPage, kCGPDFTrimBox, CGRectMake(0, 0, pageRect.size.width, pageRect.size.height), 0, true);
	CGContextConcatCTM(context, pdfTransform);
	CGContextDrawPDFPage(context, pdfPage);
	CGContextRestoreGState(context);
	
	UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
	return resultingImage;
}
