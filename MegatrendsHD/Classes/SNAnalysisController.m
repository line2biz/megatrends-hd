//
//  SNAnalysisController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 13.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNAnalysisController.h"
#import "SNNavigationControllerProtocol.h"
#import "SNAppearance.h"
#import "SNAnalyseCell.h"
#import "SNDiagramController.h"
#import "SNViewerController.h"
#import "SNAlertController.h"

#define SEGUE_SHOW_DIAGRAM @"Show Diagram Controller"

@interface SNAnalysisController () <SNNavigationControllerProtocol, NSFetchedResultsControllerDelegate, SNAnalyseCellDelegate>
{
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet UILabel *_descLabel;
    __weak IBOutlet UIButton *_btnNext;
    __weak IBOutlet UILabel *_dateLabel;
    __weak IBOutlet UITextField *_titleTextField;
    __weak IBOutlet UITextView *_textView;
    __weak IBOutlet NSLayoutConstraint *_bottomConstraint;
    __weak IBOutlet UILabel *_placeHolderLabel;
    
    NSArray *_observers;
    id _observer;
}

@end

@implementation SNAnalysisController

#pragma mark - View Life Cycle

- (void)configureView
{
    _descLabel.font = [SNAppearance boldAppFontWithSize:_descLabel.font.pointSize];
    _descLabel.text = [LMLocalizedString(@"beschreibung", nil) uppercaseString];
    
    NSString *buttonTitle = [[LMLocalizedString(@"Megatrends auswählen und bewerten", nil) uppercaseString] stringByAppendingString:@" ➤"];
    [_btnNext setTitle:buttonTitle forState:UIControlStateNormal];
    _btnNext.titleLabel.font = [SNAppearance boldAppFontWithSize:_btnNext.titleLabel.font.pointSize];
    _titleTextField.placeholder = LMLocalizedString(@"Titel der Analyse", nil);
    _titleTextField.textColor = [SNAppearance colorVinous];
    _textView.font = [SNAppearance appFontWithSize:_textView.font.pointSize];
    _placeHolderLabel.text = LMLocalizedString(@"Fragestellung", nil);
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[[SNLocalizationManager sharedManager] getLanguage]];
    NSString *format = [NSDateFormatter dateFormatFromTemplate:@"d MMM yyyy" options:0 locale:locale];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:format];
    
    _dateLabel.text = [formatter stringFromDate:_currentAnalyse.date];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Profile-Container-Bkg"]];
    
    [self configureView];
    self.currentAnalyse = nil;
    
    __weak SNAnalysisController *weekSelf = self;
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        SNAnalysisController *strongSelf = weekSelf;
        if (strongSelf) {
            [strongSelf configureView];
            [_tableView reloadData];
        }
    }];
    

    
	// Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.currentAnalyse == nil) {
        if ([[self.fetchedResultsController fetchedObjects] count]) {
            [_tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
            [self tableView:_tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self unregisterObserver];
    [[[DMManager sharedManager] managedObjectContext] save:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerObserver];
}

#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Observers
- (void)registerObserver
{
    id observer1 = [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        
        _bottomConstraint.constant = 320;
        
        [UIView animateWithDuration:.3 animations:^{
           [self.view layoutIfNeeded];
        }];
        
    }];
    
    id observer2 = [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        
        _bottomConstraint.constant = 20;
        
        [UIView animateWithDuration:.3 animations:^{
            [self.view layoutIfNeeded];
        }];
        
    }];
    
    _observers = @[observer1, observer2];
}

- (void)unregisterObserver
{
    for (id observer in _observers) {
        [[NSNotificationCenter defaultCenter] removeObserver:observer];
    }
}

#pragma mark - Property Accessors
- (void)setCurrentAnalyse:(DMAnalyse *)currentAnalyse
{
    
    [[[DMManager sharedManager] managedObjectContext] save:nil];
    
    if (_currentAnalyse != currentAnalyse) {
        _currentAnalyse = currentAnalyse;
        
        _titleTextField.text = _currentAnalyse.title;
        _dateLabel.text = [NSString stringWithFormat:@"%@", _currentAnalyse.date];
        _textView.text = _currentAnalyse.desc;
        
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[[SNLocalizationManager sharedManager] getLanguage]];
        NSString *format = [NSDateFormatter dateFormatFromTemplate:@"d MMM yyyy" options:0 locale:locale];
        NSDateFormatter *formatter = [NSDateFormatter new];
        [formatter setDateFormat:format];
        
        _dateLabel.text = [formatter stringFromDate:_currentAnalyse.date];
        
        
    }
    
    if (_currentAnalyse == nil) {
        _titleTextField.text = @"";
        _dateLabel.text = @" ";
        _textView.text = @"";
        
        if ([_textView isFirstResponder]) {
            [_textView resignFirstResponder];
        }
        
        if ([_titleTextField isFirstResponder]) {
            [_titleTextField resignFirstResponder];
        }
    }
    
    _placeHolderLabel.hidden = [_currentAnalyse.desc length] > 0;
    _titleTextField.enabled = _currentAnalyse != nil;
    _textView.editable = _currentAnalyse != nil;
    _btnNext.enabled = _currentAnalyse != nil;
    
}

#pragma mark - Segueways
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:SEGUE_SHOW_DIAGRAM]) {
        
        NSString *msg = nil;
        NSString *msgTitle = nil;
        NSString *sYES = [LMLocalizedString(@"ok", nil) uppercaseString];
        
        if (self.currentAnalyse == nil) {
            msg = LMLocalizedString(@"Analyse is selected.", nil);
            
        } else if (self.currentAnalyse.title == nil || [self.currentAnalyse.title length] < 1) {
            msg = LMLocalizedString(@"Bitte Titel der Analyse eingeben", nil);
            msgTitle = [NSString stringWithFormat:@"%@!", [LMLocalizedString(@"Achtung", nil) uppercaseString]];
            [_titleTextField becomeFirstResponder];
        }
        
        if (msg != nil) {
            [SNAlertController showAlertWithTitle:msgTitle message:msg okButtonTitle:sYES cancelButtonTitle:nil completionHandler:NULL];
            return NO;
            
        }
        
    }
    
    return YES;
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[SNDiagramController class]]) {
        [[[DMManager sharedManager] managedObjectContext] save:nil];
        [[segue destinationViewController] setCurrentAnalyse:self.currentAnalyse];
        
    }
}



#pragma mark - Outlet Methods
- (IBAction)didTapButtonAdd:(id)sender
{
    DMAnalyse *future = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMAnalyse class]) inManagedObjectContext:self.managedObjectContext];
    [self.managedObjectContext save:nil];
    NSIndexPath *indexPath = [self.fetchedResultsController indexPathForObject:future];
    [_tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    
    self.currentAnalyse = future;
    [_titleTextField becomeFirstResponder];
}

- (IBAction)editingChanged:(UITextField *)sender
{
    self.currentAnalyse.title = sender.text;
    
}

#pragma mark - UITextView Delegate
- (void)textViewDidChange:(UITextView *)textView
{
    self.currentAnalyse.desc = textView.text;
    _placeHolderLabel.hidden = [_currentAnalyse.desc length] > 0;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    [(SNAnalyseCell *)cell setAnalyse:[self.fetchedResultsController objectAtIndexPath:indexPath]];
//    [(SNAnalyseCell *)cell setDelegate:self];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.currentAnalyse = [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize maxSize = CGSizeMake(259, 500);
    DMAnalyse *analyse  = [self.fetchedResultsController objectAtIndexPath:indexPath];
    maxSize = [analyse.title sizeWithFont:[SNAppearance appFontWithSize:17] constrainedToSize:maxSize lineBreakMode:NSLineBreakByCharWrapping];
    
    return MAX(41, maxSize.height);
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
}

#pragma mark - FetchedReults Controller
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext) {
        return _managedObjectContext;
    }
    
    _managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    return _managedObjectContext;
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMAnalyse class])];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:DMAnalyseSortOrderKey ascending:DMAnalyseSortOrderAscendingValue];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Futures"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
    
}


#pragma mark - Fetched Results Controller Delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = _tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            if ([self.currentAnalyse isDeleted]) {
                self.currentAnalyse = nil;
            }
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView endUpdates];
}

#pragma mark - Analyse Cell Delegate
- (void)analyseCell:(SNAnalyseCell *)analyseCell didShowAnalyse:(DMAnalyse *)analyse
{
    SNViewerController *viewerController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNViewerController"];
    viewerController.currentAnalyse = analyse;
    [self.navigationController pushViewController:viewerController animated:YES];
}

#pragma mark - Container Protocol
- (NSAttributedString *)titleForNavigationBar
{
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] init];
    
    UIFont *boldFont = [SNAppearance boldAppFontWithSize:17];
    
    NSString *sTitle = [[LMLocalizedString(@"Meine Zukunftsanalyse", nil) stringByAppendingString:@": "] uppercaseString];
    NSRange range = NSMakeRange(0, sTitle.length);
    [attrString.mutableString appendString:sTitle];
    
    [attrString addAttribute:NSFontAttributeName value:boldFont range:range];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
    
//    NSString *sSubTitle = nil;
//    
//    switch (_selectedButton.tag) {
//        case SNZpunktTypeNews:
//            sSubTitle = LMLocalizedString(@"Future\nNews", @"Button info name");
//            break;
//        case SNZpunktTypeAbout:
//            sSubTitle = LMLocalizedString(@"Über\nz_punkt", @"Button info name");
//            break;
//        case SNZpunktTypeContact:
//            sSubTitle = LMLocalizedString(@"Kontakt &\nSupport", @"Button info name");
//            break;
//    }
    
//    sSubTitle = [sSubTitle stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
//    range = NSMakeRange(NSMaxRange(range), sSubTitle.length);
//    [attrString.mutableString appendString:sSubTitle];
//    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:range];
    
    return attrString;
    
}

@end
