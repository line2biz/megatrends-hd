//
//  SNTestCell.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 31.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNTestCell.h"
@interface SNTestCell()
{
    __weak IBOutlet UITextView *_textView;
}
@end


@implementation SNTestCell

- (void)setHighlighted:(BOOL)highlighted
{
    [self setSelected:highlighted];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [self setHighlighted:highlighted];
}

- (void)setSelected:(BOOL)selected
{
    if (selected) {
        self.backgroundColor = [UIColor grayColor];
    } else {
        self.backgroundColor = [UIColor whiteColor];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [self setSelected:selected];
    
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    _textView.userInteractionEnabled = NO;
}

@end
