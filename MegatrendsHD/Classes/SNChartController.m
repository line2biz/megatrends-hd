//
//  SNChartController.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 10.05.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import "SNChartController.h"
#import "DMManager.h"
//#import "SNKeyFactorController.h"
#import "SNChartPreviewController.h"
#import "SNNavigationController.h"
#import "SNAppearance.h"
#import "SNKeyController.h"


@interface SNChartController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate, SNNavigationControllerProtocol>
{
    __weak IBOutlet UIImageView *_imageView;
    __weak IBOutlet UITextView *_textView;
    __weak IBOutlet UIView *_containerView;
    __weak IBOutlet UIView *_chartView;
    __weak IBOutlet UIPageControl *_pageControl;
    
    UIPageViewController *_pageViewController;
    
    id _observer;
}

@end

@implementation SNChartController

#pragma mark - View Life Cycle
- (void)configureView
{
//    Trend Chart
    NSString *imageName = [NSString stringWithFormat:@"%02dsvg@2x", self.trend.tid + 1];
    
    NSBundle *bundle = [[SNLocalizationManager sharedManager] bundle];
    NSString *pictPath = [bundle pathForResource:imageName ofType:@"png"];
    _imageView.image = [UIImage imageWithContentsOfFile:pictPath];
    
//    Trend Chart Description
    UIFont *headerFont = [SNAppearance boldAppFontWithSize:17 - 3];
    UIFont *paraFont  = [SNAppearance appFontWithSize:17];
    UIFont *sourceFont  = [SNAppearance appFontWithSize:17 - 3];
    
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] init];
    NSString *paragraph = [NSString stringWithFormat:@"%@\n\n", self.trend.teaser];
    [attrString.mutableString appendString:paragraph];
    
    NSRange paraRange = NSMakeRange(0, [paragraph length]);
    [attrString addAttribute:NSFontAttributeName value:paraFont range:paraRange];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:paraRange];
    
    //    _textView.text = [SNAppDelegate chartDescriptionForIndex:self.index];
    //    _textView.font = [UIFont fontWithName:@"AvenirLTPro-Medium" size:_textView.font.pointSize];
    
    NSString *str = LMLocalizedString(@"Chart Quelle", nil);
    str = [str stringByAppendingString:@": "];
    
    paraRange = NSMakeRange([attrString length], [str length]);
    [attrString.mutableString appendString:str];
    [attrString addAttribute:NSFontAttributeName value:headerFont range:paraRange];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:paraRange];
    
    
    str = self.trend.chartSource;
    
    paraRange = NSMakeRange([attrString length], [str length]);
    [attrString.mutableString appendString:str];
    [attrString addAttribute:NSFontAttributeName value:sourceFont range:paraRange];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:paraRange];
    
    
    _textView.attributedText = attrString;
    _textView.dataDetectorTypes = UIDataDetectorTypeLink | UIDataDetectorTypePhoneNumber;
    
    _pageControl.currentPage = self.keyFactor.kid;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _chartView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Profile-Container-Bkg"]];
    _textView.editable = NO;
    
    UITapGestureRecognizer *tapRecognizer  = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapRecognizer];
    
    __weak SNChartController *weekSelf = self;
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        SNChartController *strongSelf = weekSelf;
        if (strongSelf) {
            [strongSelf configureView];
        }
    }];
    
    /*
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SNLocalizationManagerDidChangeLanguage object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self configureView];
    }];
    */
    
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}


#pragma mark - Status Bar iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Property Accessor
- (void)setKeyFactor:(DMKeyFactor *)keyFactor animated:(BOOL)animated
{
    self.keyFactor = keyFactor;
    
    UIViewController *viewController = [self viewControllerForPageIndex:self.keyFactor.kid];
    [_pageViewController setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionForward animated:animated completion:NULL];
    _pageControl.currentPage = self.keyFactor.kid;
    
    SNNavigationController *navController = (SNNavigationController *)self.parentViewController.parentViewController;
    [navController configureView];
    
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[UIPageViewController class]]) {
        _pageViewController = [segue destinationViewController];
        _pageViewController.dataSource  = self;
        _pageViewController.delegate = self;
        _pageViewController.view.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
        
        UIViewController *viewController = [self viewControllerForPageIndex:self.keyFactor.kid];
        [_pageViewController setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:NULL];
        
        _pageControl.numberOfPages = [self.trend.keyFactors count];
        _pageControl.currentPage = self.keyFactor.kid;
        
    } else
        if ([segue.destinationViewController isKindOfClass:[SNChartPreviewController class]]) {
            UIViewController *rootController = [self.storyboard instantiateInitialViewController];
            rootController.modalPresentationStyle = UIModalPresentationCurrentContext;
        }
}


#pragma mark - Outlet Methods
- (IBAction)pageControlValueChanged:(UIPageControl *)sender
{
    SNKeyController *trendController = (SNKeyController *)[self viewControllerForPageIndex:sender.currentPage];
    SNKeyController *currentController = [_pageViewController.viewControllers lastObject];
    
    UIPageViewControllerNavigationDirection direction = (currentController.keyFactor.kid < trendController.keyFactor.kid) ? UIPageViewControllerNavigationDirectionForward : UIPageViewControllerNavigationDirectionReverse;
    [_pageViewController setViewControllers:@[trendController] direction:direction animated:YES completion:NULL];
    
}

- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    CGPoint location = [gestureRecognizer locationInView:self.view];
    if (CGRectContainsPoint(_imageView.frame, location)) {
        
        SNChartPreviewController *chartPreviewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNChartPreviewController"];
        chartPreviewController.trend = self.trend;
        chartPreviewController.keyFactor = self.keyFactor;
        
        UIViewController *pvc = self.parentViewController.parentViewController.parentViewController;
        pvc.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        
        //NSLog(@"%@", NSStringFromClass([pvc class]));
        
        [pvc presentViewController:chartPreviewController animated:NO completion:NULL];
//      [self performSegueWithIdentifier:@"Show Chart" sender:self];
    }
    
}

#pragma mark - PageViewController DataSource
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    int16_t idx = [[(SNKeyController *)viewController keyFactor] kid] - 1;
    return [self viewControllerForPageIndex:idx];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    int16_t idx = [[(SNKeyController *)viewController keyFactor] kid] + 1;
    return [self viewControllerForPageIndex:idx];
}

- (UIViewController *)viewControllerForPageIndex:(NSInteger)idx
{
    DMKeyFactor *keyFactor = [DMKeyFactor keyFactorByID:idx forTrend:self.trend];;
    if (keyFactor) {
        SNKeyController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SNKeyController"];
        viewController.keyFactor  = keyFactor;
        return viewController;
    }
    
    return nil;
    
}

#pragma mark - PageController Delegate
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    SNKeyController *keyController = (SNKeyController *)[pageViewController.viewControllers lastObject];
    _pageControl.currentPage = keyController.keyFactor.kid;
    self.keyFactor = keyController.keyFactor;
    
    SNNavigationController *navController = (SNNavigationController *)self.parentViewController.parentViewController;
    [navController configureView];
    
}

#pragma mark - Container Protocol
- (NSAttributedString *)titleForNavigationBar
{
    NSString *sNumber = [NSString stringWithFormat:@"%02d: ", self.trend.tid + 1];
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:sNumber];
    
    UIFont *boldFont = [SNAppearance boldAppFontWithSize:17];
    
    NSRange range = NSMakeRange(0, attrString.length);
    [attrString addAttribute:NSFontAttributeName value:boldFont range:range];
    [attrString addAttribute:NSForegroundColorAttributeName value:[SNAppearance colorVinous] range:range];
    
    NSString *sTitle = [[self.trend.title uppercaseString] stringByAppendingString:@": "];
    range = NSMakeRange(NSMaxRange(range), sTitle.length);
    [attrString.mutableString appendString:sTitle];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
    
    NSString *sSubTitle = [self.keyFactor title];
    range = NSMakeRange(NSMaxRange(range), sSubTitle.length);
    [attrString.mutableString appendString:sSubTitle];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:range];
    
    return attrString;
    
}

@end
