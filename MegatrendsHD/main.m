 //
//  main.m
//  MegatrendsHD
//
//  Created by Александр Жовтый on 24.04.13.
//  Copyright (c) 2013 Lindenvalley Gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SNAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SNAppDelegate class]));
    }
}
